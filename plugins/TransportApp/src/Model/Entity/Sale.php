<?php

declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sale Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $salesman
 * @property string|null $vehicleno
 * @property \Cake\I18n\FrozenDate|null $date
 * @property int|null $customer_id
 * @property float|null $payed
 * @property float|null $pending
 * @property string|null $itemname
 * @property string|null $location
 * @property float|null $itemrate
 * @property float|null $itemquantity
 * @property float|null $total
 *
 * @property \TransportApp\Model\Entity\Customer $customer
 * @property \TransportApp\Model\Entity\Salesitem[] $salesitems
 */
class Sale extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'salesman' => true,
        'vehicleno' => true,
        'date' => true,
        'customer_id' => true,
        'payed' => true,
        'pending' => true,
        'itemname' => true,
        'location' => true,
        'itemrate' => true,
        'itemquantity' => true,
        'total' => true,
        'customer' => true,
        'salesitems' => true,
    ];

   public function initialize(array $config): void
    {
        $this->setDisplayField('label');
    }

    protected function _getLabel() {
                return 'Sales Date '.$this->_fields['date'] . ' Amount::'. $this->_fields['total'].'-->'.$this->_fields['name'].'__'.$this->_fields['itemname'];

    }

}
