<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Form;

use Cake\TestSuite\TestCase;
use TransportApp\Form\SalesForm;

/**
 * TransportApp\Form\SalesForm Test Case
 */
class SalesFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Form\SalesForm
     */
    protected $Sales;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->Sales = new SalesForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Sales);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
