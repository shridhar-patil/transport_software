<style>
    @media print {
        td,tr{
            line-height: 2px;
            border-style: 1px solid rgb(0,0,0);
            color: rgb(253, 249, 249);
            font-size: 10px;
            font-weight: lighter;
        }

        .top_nav{
            position: absolute;
            top: -9999px;
            left: -9999px;
        }
    }
</style>
<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="Sale index content">
    <h3><?= __('दैनिक तपशील ') ?> <?php echo date('d M Y'); ?></h3>
    <div>
        <table class= "table table-bordered table-hover" id="sales" >
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('itemname') ?></th>
                    <th><?= $this->Paginator->sort('itemrate') ?></th>
                    <th><?= $this->Paginator->sort('itemquantity') ?></th>
                    <th><?= $this->Paginator->sort('total') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sales_rows as $sales): ?>
                    <tr>
                        <td><?= h($sales['name']) ?></td>
                        <td><?= h($sales['itemname']) ?></td>
                        <td><?= h($sales['itemrate']) ?></td>
                        <td><?= h($sales['itemquantity']) ?></td>
                        <td><?= h($sales['total']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <h3><?= __('Kharch-Expenses') ?></h3>
    <div>
        <table class= "table table-bordered table-hover"   id="expenses2" >
            <tr>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('expense') ?></th>
                <th><?= $this->Paginator->sort('cost') ?></th>
            </tr>
            <tbody>
                <?php foreach ($expenses_rows as $expense): ?>
                    <tr>
                        <td><?= h($expense['date']) ?></td>
                        <td><?= h($expense['expense']) ?></td>
                        <td><?= h($expense['cost']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <h3><?= __('Jama-Payments') ?></h3>
    <div>
        <table class= "table table-bordered table-hover" id="payments"  >
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('itemname') ?></th>
                    <th><?= $this->Paginator->sort('payment_amount') ?></th>
                    <th><?= $this->Paginator->sort('date') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sales_payments as $sales): ?>
                    <tr>
                        <td><?= h($sales['id']) ?></td>
                        <td><?= h($sales['name']) ?></td>
                        <td><?= h($sales['itemname']) ?></td>
                        <td><?= h($sales['payment_amount']) ?></td>
                        <td><?= h($sales['date']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#sales').dataTable(
                {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                }
        );
    });

    $(document).ready(function () {
        $('#expenses2').dataTable(
                {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                }
        );
    });

    $(document).ready(function () {
        $('#payments').dataTable(
                {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                }
        );
    });
</script>





