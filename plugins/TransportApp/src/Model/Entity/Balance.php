<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Balance Entity
 *
 * @property int $id
 * @property int $payed
 * @property int $pending
 * @property int $bill_id
 * @property string $name
 *
 * @property \TransportApp\Model\Entity\Bill $bill
 * @property \TransportApp\Model\Entity\Salesitem[] $salesitems
 */
class Balance extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'payed' => true,
        'pending' => true,
        'bill_id' => true,
        'name' => true,
        'bill' => true,
        'salesitems' => true,
    ];
}
