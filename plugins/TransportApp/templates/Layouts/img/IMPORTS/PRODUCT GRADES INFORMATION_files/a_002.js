// name - name of the cookie
// value - value of the cookie
// [days] - number of days before cookie expires
// [path] - path for which the cookie is valid (defaults to path of calling document)
// [domain] - domain for which the cookie is valid (defaults to domain of calling document)
// [secure] - Boolean value indicating if the cookie transmission requires a secure transmission
// * an argument defaults when it is assigned null as a placeholder
// * a null placeholder is not required for trailing omitted arguments
function setCookie(name, value, days, path, domain, secure) {
	if (days) {
		expires = new Date();
		expires.setTime(expires.getTime() + days * 86400000);
	}
	var curCookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
	document.cookie = curCookie;
}

// name - name of the desired cookie
// * return string containing value of specified cookie or null if cookie does not exist
function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return unescape(dc.substring(begin + prefix.length, end));
}

function getParam(name, s) {
	var params = '';
	name = '&' + name + '=';
	if(s)
		params = '&' + s;
	else
		params = '&' + window.location.search.substr(1);
	var start = params.indexOf(name);
	if (start != -1) {
		start += name.length;
		var end = params.indexOf('&',start);
		if (end == -1) end = params.length;
		return params.substring(start,end);
	} else {
		return null;
	}
}

function dynamic_code(server) {

	var tid = getParam('tid');
	var sourceid = getParam('sourceid');
	var cj_aid = getParam('AID');
	var cj_pid = getParam('PID');
	var cj_sid = getParam('SID');
	var cj_url = getParam('URL');
	var cookie_value = '';

	if (sourceid) {
		setCookie(
			'WWW_ORIGINAL_SOURCE',
			sourceid,
			30,
			'/',
			'.liquidation.com'
		);
	}

	if (tid) {

		setCookie(
			'WWW_TID',
			tid,
			1000,
			'/',
			'.liquidation.com'
		);

		document.write('<IMG SRC=/tid?tid=' + tid + '&url='
			+ document.location.hostname
			+ document.location.pathname
			+ document.location.search + '>');
	}

	if (cj_aid) {
		cookie_value = 'AID=' + cj_aid + '&PID=' + ((cj_pid) ? cj_pid : '') + '&SID=' + ((cj_sid) ? cj_sid : '');
		
		setCookie(
			'WWW_CJ',
			cookie_value,
			30,
			'/',
			'.liquidation.com'
		);

		if (cj_url) {
			document.location = unescape(cj_url);
		}
	}

}

function write_banner(type) {

	var html    = banner_html_data[type];
	var weights = banner_weight_data[type];

	// make an array with the indexes of banner_weight_data where each value is there weight times
	// thus, if we select a random index from this matrix it has weight chance of being picked
	// even if we have 100 banner adds all with weights of 100 (the max) thats still only 10,000 at constant time O(10,000)
	// likely this # will be closer to 8 banner ads at weights < 10 each so thats constant time O(80) 
	var matrix = new Array();
	for (var i = 0; i < weights.length; i++) {
		var weight = weights[i];
		for (var j = 0; j <= weight; j++) {
			matrix[i+j] = i;
		}
	}

	var index = 0;
	var randomNumber = 0;

	randomNumber = randomvalue(0, matrix.length-1);
	if (randomNumber < 0) { randomNumber *= -1 }

	index = matrix[randomNumber];

	document.write(html[index]);

	return;
}

function randomvalue(low, high) { return Math.floor(Math.random() * (1 + high - low) + low); }
var categoryData ;
var isCategory = false;
var isAdvanced = false;
var isAdvancedOpen = false;
$(document).ready(function() {
    var mainCategoryHTML = "";
    $.getJSON( "/json/category_with_auctions.js", function(data) {
        categoryData = data;
        for(var i=0;i<categoryData.length;i++){
            mainCategoryHTML +='<li class="item" id="'+categoryData[i].id+'"><a href="' + categoryData[i].url + '">'+categoryData[i].name+'</a></li>';
        }
        if(typeof(str)!="undefined" && str == true){
            mainCategoryHTML +='<li class="item" id="all_categories"><a href="/allcategories.html">All Categories</a></li>';
        }else{
            mainCategoryHTML +='<li class="item" id="all_categories"><a href="/bu/allcategories.html">All Categories</a></li>';
        }
        $('div.categoryList').find('ul.list').html(mainCategoryHTML);
        CategoryFunctions();
        allCatsFillFunction(data);
    });
	

   
    AdvancedSearchFunctions();
	Invoker.userIdentify(function(data){
		Invoker.updateSignIn(data);
	});
});
function CategoryFunctions() {

    function appendItems(type) {
        var subItems = [];
        for(var j=0;j<categoryData.length;j++){
            subItems[categoryData[j].id] = [];
            for(var k=0;k<categoryData[j].subcategory.length;k++){
                var subitemObj =  {
                    value:categoryData[j].subcategory[k].name,
                    link:categoryData[j].subcategory[k].url
                };
                subItems[categoryData[j].id].push(subitemObj);
            }
        }
        var content = "";
        for(var i = 0 ; i < subItems[type].length;i++) {
            content += "<div class='subItem'><a href='"+ subItems[type][i].link +"'>"+ subItems[type][i].value+"</a></div>";
        }
        $('div.subCategory div.subContainer').append(content);
    }

    function appendSubCategory() {
        var currentIndex = mainCat.length - mainCat.index(currentNode);
        currentNode.addClass("selected");
        if ($('div.subCategory').length != 0) {
            closeSubCategory();
        }
        $("div.subCatMenu").remove();
        $("div.categoryList").append("<div class='subCatMenu'><div class='whiteBlock'> </div><div class='subCategory'><div class='top'></div><div class='subContainer'></div><div class='subCategoryBottom'></div></div></div>");

        appendItems(currentNode.attr("id"));
        if($('div.subCategory div.subContainer').html() == ""){
            $("div.subCatMenu").remove();
        }
        var isIE6 = (typeof document.body.style.maxHeight == "undefined");
        if(!isIE6) {
            $('div.subCatMenu').css('margin-top',-(currentIndex*25+5));
            $('div.subCategory').css({
                'height':$('div.subContainer').attr('offsetHeight')+10
            });
        }
        else {
			$('div.subCategory').css({
                'height':$('div.subContainer').attr('offsetHeight')+20
            });
            $('div.subCatMenu').css('margin-top',-(currentIndex*25 + 13));
        }
        $('div.subContainer').hover(function() {
            if (intervalId != null) {
                clearTimeout(intervalId);
                intervalId = null;
            }
        },closeSubCategory);
    }

    function closeSubCategory() {
        $("div.container ul.list li.selected").removeClass("selected");
        $('div.subCategory').remove();
        $('div.whiteBlock').remove();
    }

    $(document).bind("mouseover",function(e){ //hide the category list when mouseovered somewhere in the document except categoryListLink and categoryList container.
        var categoryContainer = $(e.target).parents('div.categoryList');
        var categoryListLink = $(e.target).parents('div.categoryListLink');
        if($(e.target).hasClass("categoryList") || $(e.target).hasClass("categoryListLink")){
            isCategory = true;
        }else if(categoryContainer.length == 0 && categoryListLink.length == 0){
            isCategory = false;
        }
    });
    
    setInterval(function(){
		
        if(isCategory){
            setTimeout(function(){
                var currentElement = $('div.categoryListLink');
                currentElement.addClass("categorySelected");
                $("div.categoryList").show();
            },300);
        }else if(isAdvancedOpen){
            setTimeout(function(){
                var currentElement = $("div.advancedSearchLink");
                if(isAdvancedOpen) {
                    showSearch(currentElement);
                }
            },300);
        }else{
            $("div.categoryListLink").removeClass("categorySelected");
            $("div.categoryList").hide();
            closeSubCategory();
            var currentElement = $("div.advancedSearchLink");
            hideSearch(currentElement);
        }
    },400);

    var currentNode =  null;
    var intervalId = null;
    var mainCat = $("div.container ul.list li.item");
    mainCat.hover(function() {
        if(intervalId == null) {
            currentNode = $(this);
            if(currentNode.attr("id")!="all_categories")
            {
                appendSubCategory();
            }
            else
            {    
                currentNode.addClass("selected");
            }
            currentNode = null;
        } else {
            currentNode = $(this);
        }
    },function(){
        if(intervalId == null) {
            intervalId = setTimeout(function() {
                closeSubCategory();
                intervalId = null;
                if(currentNode != null) {
                    if(currentNode.attr("id")!="all_categories")
                    {
                        appendSubCategory();
                    }
                    else
                    {
                        currentNode.addClass("selected");
                    }
                    currentNode = null;
                }
            },50);
        }
    });
}


function showSearch(currentElement) {
    currentElement.addClass("shown");
    $("div.advancedSearch").show();
    $('div.advancedSearch .customselect-label').each(function() {
        $(this).autoEllipsis('',{
            'width':parseInt($(this).parent().css('width').split('px')[0]) -20
        });
    });
}

function hideSearch(currentElement) {
    isAdvanced = false;
    isAdvancedOpen = false;
    currentElement.removeClass("shown");
    $("div.advancedSearch").hide();
    $(document).unbind('keydown');
}
	
function AdvancedSearchFunctions() {
    $(document).bind("mouseover",function(e){ //hide the advancedSearch container when mouseovered somewhere in the document except advancedSearchLink and advancedSearch container.
        var advancedSearchContainer = $(e.target).parents('div.advancedSearch');
        var advancedSearchLink = $(e.target).parents('div.advancedSearchLink');
        var currentElement = $("div.advancedSearchLink");
        if($(e.target).hasClass("advancedSearch") || $(e.target).hasClass("advancedSearchLink")){
            isAdvancedOpen = true;
            $("div.advancedSearch form").find("div.customselect-label, input").bind("click",function(){
                // set advance true only if the user clicks or textfiels
                isAdvanced = true;
            });
            //showSearch(currentElement);
            $("div.saveSearchContainer").hide();
        } else if(advancedSearchContainer.length == 0 && advancedSearchLink.length == 0 && isAdvanced == false ){
            isAdvancedOpen = false;
            isAdvanced = false;
            hideSearch(currentElement);
        }


    });

    $('div.advancedSearch div.container div.closeContainer > *').bind('click',function() {
        isAdvanced = false;
        isAdvancedOpen = false;
        $('.advancedSearch').find('.customselect-container').hide();
        hideSearch($("div.advancedSearchLink"));
    });
}

function allCatsFillFunction(allCategoryData)
{
    var row, allCategoriesHTML;
    for (var i=0; i<allCategoryData.length; i++) {
        allCategoriesHTML='<ul><label><a href="'+allCategoryData[i].url+'">'+allCategoryData[i].name+'</a></label>';
        for(var j=0;j<allCategoryData[i].subcategory.length;j++){
            allCategoriesHTML += '<li><a href="'+allCategoryData[i].subcategory[j].url+'">' + allCategoryData[i].subcategory[j].name+'</a></li>';
        }
        allCategoriesHTML+='</ul>';
        row = Math.floor(i/3) + 1;
        $('div#categorylist'+row).append(allCategoriesHTML);
    }
}

