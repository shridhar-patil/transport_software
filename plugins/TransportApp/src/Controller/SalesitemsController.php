<?php
declare(strict_types=1);

namespace TransportApp\Controller;

use TransportApp\Controller\AppController;

/**
 * Salesitems Controller
 *
 * @property \TransportApp\Model\Table\SalesitemsTable $Salesitems
 * @method \TransportApp\Model\Entity\Salesitem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalesitemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sales', 'Balances'],
        ];
        $salesitems = $this->paginate($this->Salesitems);

        $this->set(compact('salesitems'));
    }

    /**
     * View method
     *
     * @param string|null $id Salesitem id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesitem = $this->Salesitems->get($id, [
            'contain' => ['Sales', 'Balances'],
        ]);

        $this->set(compact('salesitem'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesitem = $this->Salesitems->newEmptyEntity();
        if ($this->request->is('post')) {
            $salesitem = $this->Salesitems->patchEntity($salesitem, $this->request->getData());
            if ($this->Salesitems->save($salesitem)) {
                $this->Flash->success(__('The salesitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The salesitem could not be saved. Please, try again.'));
        }
        $sales = $this->Salesitems->Sales->find('list', ['limit' => 200]);
        $balances = $this->Salesitems->Balances->find('list', ['limit' => 200]);
        $this->set(compact('salesitem', 'sales', 'balances'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Salesitem id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesitem = $this->Salesitems->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesitem = $this->Salesitems->patchEntity($salesitem, $this->request->getData());
            if ($this->Salesitems->save($salesitem)) {
                $this->Flash->success(__('The salesitem has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The salesitem could not be saved. Please, try again.'));
        }
        $sales = $this->Salesitems->Sales->find('list', ['limit' => 200]);
        $balances = $this->Salesitems->Balances->find('list', ['limit' => 200]);
        $this->set(compact('salesitem', 'sales', 'balances'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Salesitem id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesitem = $this->Salesitems->get($id);
        if ($this->Salesitems->delete($salesitem)) {
            $this->Flash->success(__('The salesitem has been deleted.'));
        } else {
            $this->Flash->error(__('The salesitem could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
