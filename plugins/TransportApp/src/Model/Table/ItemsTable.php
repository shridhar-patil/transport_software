<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @method \TransportApp\Model\Entity\Item newEmptyEntity()
 * @method \TransportApp\Model\Entity\Item newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Item get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Item findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Item[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Item|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Item saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('item_id');
        $this->setPrimaryKey('item_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('item_id')
            ->allowEmptyString('item_id', null, 'create');

        $validator
            ->scalar('item')
            ->maxLength('item', 200)
            ->allowEmptyString('item');

        $validator
            ->scalar('item_cd')
            ->maxLength('item_cd', 15)
            ->allowEmptyString('item_cd');

        return $validator;
    }
}
