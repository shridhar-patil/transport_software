var lsi = {
	cookieDomain: function() {
		return ".liquidation.com";
	},
	cookieName: function() {
		return "WWW_SESSION_ID_USER";
	},
	sessionCookieName: function() {
		return "WWW_SESSION_ID";
	},
	currencySymbol: function() {
		return "$";
	},
	wwwDomain: function() {
		return "www.liquidation.com";
	},
	uaAppKey: function() {
		return "1";
	},
	uaURL: function() {
		return "https://ua.lqdt.com";
	},
	uaActiveFlag: function() {
		return true;
	},
	isMobile: function() {
		if (navigator.userAgent && navigator.userAgent.match(/Mobile|Android|BlackBerry/))
			return true;
		else return false;
	},
	_i18n: {
		
	},
	i18n: function(el, params) {
		function replace_vars(str, vars) {
			str = str.replace(/%{(\w+)}/g, function(match, $1, offset, original) {
				return vars[$1];
			});
			return str;
		}
		var locale = lsi.get_locale();
		if (!(locale in lsi._i18n) || !(el in lsi._i18n[locale]))
			return "";
		var str = lsi._i18n[locale][el];
		if (typeof params == "object" && "vars" in params)
			str = replace_vars(str, params.vars);
		return str;
	},
	localeURLPrefix: function() {
		if (lsi.get_locale() != "en") // only until /en exists
			return '/' + lsi.get_locale();
		return "";
	},
	get_locale: function() {
		var locale = "en";
		var matches = window.location.pathname.match(/^\/(\w\w)\//)
		if (matches && matches.length > 0)
			locale = matches[1];
		return locale;
	}
};
