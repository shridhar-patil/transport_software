<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BalancesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BalancesTable Test Case
 */
class BalancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BalancesTable
     */
    protected $Balances;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Balances',
        'app.Bills',
        'app.Salesitems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Balances') ? [] : ['className' => BalancesTable::class];
        $this->Balances = $this->getTableLocator()->get('Balances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Balances);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
