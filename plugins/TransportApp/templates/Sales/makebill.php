<style>

    @media print {
        td,tr{
            line-height: 2px;
            border-style: 1px solid rgb(233, 225, 225);
            color: rgb(253, 249, 249);
            font-size: 10px;
            font-weight: lighter;
        }
        .top_nav{
            position: absolute;
            top: -9999px;
            left: -9999px;
        }

    }

</style>
&nbsp;

<div class="container">
        <div class="row">
    
 <div class="col-md-12">
<div class="x_panel filter-panel">
                                <div class="x_title">
                                    <h2>Invoice Filters And Payments वयक्तिक खातेदार बिलिंग  </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Settings 1</a>
                                                <a class="dropdown-item" href="#">Settings 2</a>
                                            </div>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <form class="form-label-left input_mask">

                                        <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Select Customer </label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                          <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Invoice Category </label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                          <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Invoice Types  </label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                        <div class="form-group-sm row">
                                            <div class="col-md-5 col-sm-9  offset-md-3 pull-right">
                                                <br>
                                            
                                                <button class="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" class="btn btn-success">Filter Invoice</button>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Update Payments</button> 

                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>



</div></div> </div>
<?php
if ($_POST) {
    $total_salespayment_done = 0;
    ?>

    <div class="container">
         <div class="row">

             <div class="col-md-12"><h2> वयक्तिक खातेदार बिलिंग</h2></div>
         </row>
        <div class="row">

            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Invoice Date: <?php echo date('d M Y'); ?>   <small>Pujari Transport</small></h2>

                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Settings 1</a>
                                    <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <section class="content invoice">
                            <!-- title row -->
                            <div class="row">
                                <div class="  invoice-header">
                                    <h1><i class="fa fa-globe"></i> Invoice. </h1>

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    From
                                    <address>
                                        <strong>Pujari Transport</strong>
                                        <br>Hupari City
                                        <br>District: Kolhapur
                                        <br>State : Maharashtra
                                        <br>Email: akshay.pujari89@gmail.com
                                        <br>GST#: Not Available 
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    To
                                    <address>
                                        <strong><?php echo strtoupper($Sale[0]['name']); ?> </strong>

                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>Invoice #</b>
                                    <br>
                                    <br>
                                    <b>Order ID:</b> ............
                                    <br>
                                    <b>Payment Due:</b><?php echo date('d M Y'); ?>
                                    <br>
                                    <b>Account:</b><?php echo $Sale[0]['customer_id'] . ':' . strtoupper($Sale[0]['name']); ?> 
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- Table row -->
                            <div class="row">

                                <?php
                                if ($_POST) {
                                    $total = 0;
                                    $mastertotal = 0;
                                    $masterq = 0;
                                    $datetotal = 0;
                                    $lastdate = '2004-01-01';
                                    echo"<br><table class=\"table table-striped\" id=testTabl >";
                                    echo "<thead>
                                      <tr>
                                      <th>Invoice#</th>
                                      <th>Date</th>
                                      <th>Itemname</th>
                                      <th>ItemQuantity</th>
                                      <th>ItemRate</th>
                                      <th>Total</th>
                                      <th>Same Day Total</th>
                                      <th> Paid Payments </th>
                                      </tr>
                                      </thead>";

                                    $i = count($Sale);
                                    $i = $i - 1;
                                    while ($i >= 0) {

                                        $ID = $Sale[$i]['id'];
                                        echo"<tr><td><a href=#";
                                        echo '>  ';
                                        echo $Sale[$i]['id'];
                                        echo"</a></td>";
                                        echo"<td>";

                                        $d = new DateTime($Sale[$i]['date']);
                                        echo $d->format('l d F Y');
                                        echo"</a></td>";

                                        echo"<td>";
                                        echo $Sale[$i]['itemname'];
                                        echo"</td>";
                                        echo"<td>";
                                        echo $Sale[$i]['itemquantity'];
                                        echo"</td>";
                                        $masterq = $masterq + $Sale[$i]['itemquantity'];

                                        echo"<td>";
                                        echo $Sale[$i]['itemrate'];
                                        echo"</td>";

                                        $total = $Sale[$i]['itemquantity'] * $Sale[$i]['itemrate'];
                                        $mastertotal = $total + $mastertotal;

                                        $total_salespayment_done += $Sale[$i]['total_salespayment_done'];

                                        echo"<td>";
                                        echo $total;
                                        echo"</td>";

//this is for calculation of same date totals

                                        if ($lastdate == $Sale[$i]['date']) {

                                            $datetotal = $datetotal + $total;

                                            echo"<td>";
                                            echo"Total Rs::";
                                            echo $datetotal;
                                            echo"</td>";
                                        } else {
                                            echo"<td>&nbsp;";
                                            echo"</td>";
                                            $lastdate = $Sale[$i]['date'];
                                            $datetotal = $total;
                                        }

                                        echo"<td>";
                                        echo $Sale[$i]['total_salespayment_done'];
                                        echo"</td>";
                                        echo"</tr>";
                                        $i--;
                                    }
                                    echo"
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total Qty-$masterq</td>
                                <td></td>
                                <td>Total All::$mastertotal</td></tr>
                                ";
                                    echo "</table>";
                                }
                                ?>

                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <div class="row d-print-inline ">
                                <!-- accepted payments column -->
                                <div class="col-md-4  ">
                                    <p class="lead">Payment Methods:</p>
                                    <img src="images/mastercard.png" alt="Cash"> |
                                    <i class="fas fa-cash-registers"></i> Cash 
                                    <img src="images/mastercard.png" alt="Bank Transfer"> |
                                    <img src="images/mastercard.png" alt="Google Pay"> | 
                                    <img src="images/mastercard.png" alt="Paytm">

                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        Payment should be done before due date on below A/C details  <br>
                                        Bank Name : Hupari Bank Ltd <br>
                                        Branch : Hupari <br>
                                        A/C Name :<br>
                                        A/C NO: 1234 5678 9101<br>
                                        IFSC No : HPLP0000123
                                    </p>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-7">
                                    <p class="lead">Amount Due <?php echo date('d M Y'); ?></p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th style="width:50%">Subtotal:</th>
                                                    <td><?php echo $mastertotal; ?> </td>
                                                </tr>
                                                <tr>
                                                    <th>Tax (12.5%)</th>
                                                    <td>Not Applicable</td>
                                                </tr>
                                                <tr>
                                                    <th>Paid Payments</th>

                                                    <td><?php echo $total_salespayment_done; ?> </td>
                                                </tr>
                                                <tr>
                                                    <th>Shipping:</th>
                                                    <td>Not Applicable</td>
                                                </tr>
                                                <tr>
                                                    <th><h1>Total:</h2></th>
                                                    <td><h1><?php echo $mastertotal - $total_salespayment_done; ?></h1> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- this row will not appear when printing -->
                            <div class="row no-print">
                                <div class=" ">
                                    <button class="btn btn-primary" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                    <!--<button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>-->
                                    <!--<button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>-->
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } else { ?> 
    <h2>वयक्तिक खातेदार बिलिंग</h2>
    <form method="post"><fieldset>
            Enter date as year-month-day eg:2014-01-01 for 1 Jan 2014<br><br>
            <label>Start Date</label>
            <input type="date" name="startdate" value="<?php echo date('Y-m-01') ?>" />
            <label>Last Date</label>
            <input type="date" name="enddate" value="<?php echo date('Y-m-d') ?>" />
            <div id="loadcustomer"></div>
            <script>
                $("#loadcustomer").load("<?php echo $this->Url->build(array('controller' => 'customers', 'action' => 'loadcustomer')); ?>");
            </script>
            <input type="submit"></fieldset>
    </form>

    <?php
}//ifelse ends
?>





<div class="modal fade bs-example-modal-lg  " tabindex="-1" role="dialog" style=" padding-right: 15px;" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel"> Payment History</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                         
                         <div class="x_panel filter-panel">
                                <div class="x_title">
                                    <h2>Adding Payments</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Settings 1</a>
                                                <a class="dropdown-item" href="#">Settings 2</a>
                                            </div>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <form class="form-label-left input_mask">

                                        <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Payment Deposit </label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                          <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Pending Till Now</label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                          <div class="col-md-3 col-sm-6  form-group-sm has-feedback">
                                            <label>Bill Amount  </label>
                                            <input type="text" class="form-control1 has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                           
                                        </div>

                                        <div class="form-group-sm row">
                                            <div class="col-md-5 col-sm-9  offset-md-3 pull-right">
                                                <br>
                                            
                                                <button class="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" class="btn btn-success">Add Payment</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
</div>
                        
                                <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </th>
                            <th class="column-title">Invoice </th>
                            <th class="column-title">Invoice Date </th>
                            <th class="column-title">Order </th>
                            <th class="column-title">Bill to Name </th>
                            <th class="column-title">Status </th>
                            <th class="column-title">Amount </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                            </td>
                            <td class=" ">121000040</td>
                            <td class=" ">May 23, 2014 11:47:56 PM </td>
                            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
                            <td class=" ">John Blank L</td>
                            <td class=" ">Paid</td>
                            <td class="a-right a-right ">$7.45</td>
                            <td class=" last"><a href="#">View</a>
                            </td>
                          </tr>
                        
                          
                        </tbody>
                      </table>
                         
                        <p>asfdasf</p>
                                

                        </div>


                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>

                      </div>
                    </div>
                  </div>






<script type="text/javascript">

    // hide filters 


$(document).ready(
function(){

    $(".filter-panel").click(); 
});
   
</script>