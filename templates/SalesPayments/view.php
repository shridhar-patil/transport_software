<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SalesPayment $salesPayment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sales Payment'), ['action' => 'edit', $salesPayment->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sales Payment'), ['action' => 'delete', $salesPayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesPayment->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sales Payments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sales Payment'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="salesPayments view content">
            <h3><?= h($salesPayment->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Sale') ?></th>
                    <td><?= $salesPayment->has('sale') ? $this->Html->link($salesPayment->sale->name, ['controller' => 'Sales', 'action' => 'view', $salesPayment->sale->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($salesPayment->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Payment Amount') ?></th>
                    <td><?= $this->Number->format($salesPayment->payment_amount) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date') ?></th>
                    <td><?= h($salesPayment->date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
