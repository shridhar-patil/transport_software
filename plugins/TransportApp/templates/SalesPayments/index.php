
<style>
    
  @media print {
  td,tr{
    line-height: 2px;
    border-style: 1px solid rgb(255, 225, 225);
    color: rgb(253, 249, 249);
    font-size: 10px;
    font-weight: lighter;
  }
  
  
  }
  
  
</style>
<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $salesPayments
 */
?>
<div class="salesPayments index content">
    <?= $this->Html->link(__('New Sales Payment'), ['action' => 'pendingpayments'], ['class' => 'button float-right']) ?>
    <h3><?= __('Sales Payments') ?></h3>
    <div>
        <table class= "table table-bordered table-hover" id="testTabl">
            <thead>
                <tr>
                    <th>Payment<?= $this->Paginator->sort('id') ?></th>
                    <th>
                        <?= $this->Paginator->sort('sales_id') ?> Id</th>

                    <th>Payment<?= $this->Paginator->sort('date') ?></th>


                    <th>Sales Date</th>
                    <th>Customer</th>
                    <th>Item name</th>
                    <th>Total</th>

                    <th><?= $this->Paginator->sort('payment_amount') ?></th>
                 

                   
                </tr>
            </thead>
            <tbody>
                <?php foreach ($salesPayments as $salesPayment): ?>
                    <tr>
                        <td><?= $this->Number->format($salesPayment->id) ?></td>
                        
<td><?= h($salesPayment->sale->id) ?></td>
                        <td><?= h($salesPayment->date) ?></td>

                        <td><?= h($salesPayment->sale->date) ?></td>
                        <td><?= h($salesPayment->sale->name) ?></td>
                        <td><?= h($salesPayment->sale->itemname) ?></td>
                        <td><?= h($salesPayment->sale->total) ?></td>
                        <td><?= $this->Number->format($salesPayment->payment_amount) ?></td>


                   
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#testTabl').dataTable();
} );
</script>
