<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SalesPayment[]|\Cake\Collection\CollectionInterface $salesPayments
 */
?>
<div class="salesPayments index content">
    <?= $this->Html->link(__('New Sales Payment'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Sales Payments') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('sales_id') ?></th>
                    <th><?= $this->Paginator->sort('payment_amount') ?></th>
                    <th><?= $this->Paginator->sort('date') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($salesPayments as $salesPayment): ?>
                <tr>
                    <td><?= $this->Number->format($salesPayment->id) ?></td>
                    <td><?= $salesPayment->has('sale') ? $this->Html->link($salesPayment->sale->name, ['controller' => 'Sales', 'action' => 'view', $salesPayment->sale->id]) : '' ?></td>
                    <td><?= $this->Number->format($salesPayment->payment_amount) ?></td>
                    <td><?= h($salesPayment->date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $salesPayment->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesPayment->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesPayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesPayment->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
