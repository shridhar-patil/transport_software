<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Form;

use Cake\TestSuite\TestCase;
use TransportApp\Form\AllForm;

/**
 * TransportApp\Form\AllForm Test Case
 */
class AllFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Form\AllForm
     */
    protected $All;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->All = new AllForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->All);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
