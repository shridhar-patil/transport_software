<style>
    
  @media print {
  td,tr{
    line-height: 2px;
    border-style: 1px solid rgb(0,0,0);
    color: rgb(253, 249, 249);
    font-size: 10px;
    font-weight: lighter;
    
   
      
  }
  
   .top_nav{
      position: absolute;
        top: -9999px;
         left: -9999px;
   }
  
  }
  
  
</style>
<h1>Customers List सर्व  खातेदार </h1>
<div class="customers index content">
    <h3><?= __('Customers') ?></h3>
    <div class="table-responsive">
        <table class= "table table-bordered table-hover" id="testTabl">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('phone') ?></th>
                    <th><?= $this->Paginator->sort('area') ?></th>
                    <th><?= $this->Paginator->sort('date') ?></th>
                    <th><?= $this->Paginator->sort('vehicle') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customers as $customer): ?>
                <tr>
                    <td><?= $this->Number->format($customer->id) ?></td>
                    <td><?= h($customer->name) ?></td>
                    <td><?= h($customer->phone) ?></td>
                    <td><?= h($customer->area) ?></td>
                    <td><?= h($customer->date) ?></td>
                    <td><?= h($customer->vehicle) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.table').dataTable();
} );
</script>