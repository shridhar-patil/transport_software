<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sm Entity
 *
 * @property int $id
 * @property string|null $from
 * @property string|null $to
 * @property string|null $message
 * @property \Cake\I18n\FrozenTime|null $created_at
 */
class Gst extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sale_ac' => true,
        'invoice_no' => true,
        'invoice_no_date' => true,
        'party_name' => true,
        'date' => true,
        'order_no' => true,
        'order_no_date' => true,
        'transport' => true,
        'station' => true,
        'lorry_no_date' => true,
        'from_ps' => true,
        'to_ps' => true,
        'quality' => true,
        'hs_code' => true,
        'unit' => true,
        'taga' => true,
        'quantity' => true,
        'rate' => true,
        'amount' => true,
        'addition1' => true,
        'addition_value1' => true,
        'addition2' => true,
        'addition_value2' => true,
        'addition3' => true,
        'addition_value3' => true,
        'deduction1' => true,
        'deduction_value1' => true,
        'deduction2' => true,
        'deduction_value2' => true,
        'deduction3' => true,
        'deduction_value3' => true,
        'due_date' => true,
        'gst_date' => true,

    ];
}
