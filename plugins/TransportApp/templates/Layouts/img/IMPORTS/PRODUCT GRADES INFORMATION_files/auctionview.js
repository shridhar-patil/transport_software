$.cookie = function(name, value, options) {
    if (typeof value != 'undefined') {
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString();
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = $.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};


$.extend({

	showRegBox: function() {

		// Show content block.
		$("#nr_register").show();

		// Check for search term in referring page, replace "Looking for...?" text.
		$("#engineString").text(function() {
			var ref = document.referrer;
			if (ref.indexOf('?') == -1) return;
			var qs = ref.substr(ref.indexOf('?')+1);
			var qsa = qs.split('&');
			for (var i=0;i<qsa.length;i++) {
			var qsip = qsa[i].split('=');
			if (qsip.length == 1) continue;
				if (qsip[0] == 'q' || qsip[0] == 'p') {
					var wordstring = unescape(qsip[1].replace(/\+/g,' '));
					return wordstring;
				}
			}
		});
	
	}

});


$(document).ready(function(){

	// Check session cookie.
	var searchCookie = null;
	searchCookie = $.cookie("searchReferral");
	if (searchCookie != null) {

		$.showRegBox();

	} else {

		// Check referrer domain.
		var referrerDomain = document.referrer.split('/')[2];
		if ( (referrerDomain.indexOf("google.com") != -1) || (referrerDomain.indexOf("yahoo.com") != -1) || (referrerDomain.indexOf("bing.com") != -1) || (referrerDomain.indexOf("msn.com") != -1) || (referrerDomain.indexOf("live.com") != -1) ) {
		
			// Check user cookie.
			var userCookie = null;
			var userCookieDev = null;
			userCookie = $.cookie("WWW_SESSION_ID_USER");
			userCookieDev = $.cookie("WWW_SESSION_ID_DEV_USER");
			if ( (userCookie == null) && (userCookieDev == null) ) {
	
				$.cookie("searchReferral", referrerDomain);
				$.showRegBox();
	
			}
	
		}
	
	}

});