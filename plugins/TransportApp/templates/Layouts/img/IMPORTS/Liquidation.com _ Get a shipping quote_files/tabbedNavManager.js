
        function adjusttabs() {
          $('.tab .tabNavMiddle').css({'padding-left':'0','padding-right':'0'});
          var totalWidth = $("div.tabContainer").attr("offsetWidth");
          var occupiedWidth = 0;
          var noofTabs = 0;
          $("div.tab").each(function(){
              noofTabs++;
             occupiedWidth+= $(this).attr("offsetWidth")
          });
          var availableWidth = totalWidth - occupiedWidth -(noofTabs * 1);      // 1px for the margin  + 10 px for left and right
          var paddingLeft = Math.floor(availableWidth / (noofTabs *2));
          var paddingRight = Math.floor(availableWidth / (noofTabs *2));
          occupiedWidth = 0;
          $("div.tab div.middle:not(div.tab div.middle:last)").each(function(){
            $(this).css({"padding-left": paddingLeft,"padding-right":paddingRight});
            occupiedWidth += $(this).attr("offsetWidth");
          });
          availableWidth = totalWidth - occupiedWidth - $("div.tab:last").attr("offsetWidth") -((noofTabs-1 ) * 11); // 1px for the margin
          paddingLeft = Math.floor(availableWidth / 2);
          paddingRight = Math.floor(availableWidth / 2);
          $("div.tab div.middle:last").css({"padding-left": paddingLeft,"padding-right":paddingRight});
		  $("div.tab:last").css({"margin-right":0});
      }
        $(document).ready(function(){
           // adjusttabs();
        });
    