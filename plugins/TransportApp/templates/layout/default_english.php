<?php
$cakeDescription = 'Transport Application';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">-->


        <?=
        $this->Html->css([
//            'normalize.min', 'milligram.min', 'cake',
            "/transport_app/assets/jquery.dataTables.min.css",
            "/transport_app/assets/bootstrap/dist/css/bootstrap.min.css",
            "/transport_app/assets/font-awesome/css/font-awesome.min.css",
            "/transport_app/assets/nprogress/nprogress.css",
            "/transport_app/assets/iCheck/skins/flat/green.css",
            "/transport_app/assets/datatables.net-bs/css/dataTables.bootstrap.min.css",
            "/transport_app/assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css",
            "/transport_app/assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css",
            "/transport_app/assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
            "/transport_app/assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css",
            "/transport_app/assets/custom.min.css",
            "/transport_app/assets/chosen/chosen.min.css"
            
        ])
        ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <?=
        $this->Html->script([
            "/transport_app/assets/jquery/dist/jquery.min.js",
            "/transport_app/assets/bootstrap/dist/js/bootstrap.bundle.min.js",
            "/transport_app/assets/fastclick/lib/fastclick.js",
            "/transport_app/assets/nprogress/nprogress.js",
            "/transport_app/assets/iCheck/icheck.min.js",
            "/transport_app/assets/datatables.net/js/jquery.dataTables.min.js",
            "/transport_app/assets/datatables.net-bs/js/dataTables.bootstrap.min.js",
            "/transport_app/assets/datatables.net-buttons/js/dataTables.buttons.min.js",
            "/transport_app/assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
            "/transport_app/assets/datatables.net-buttons/js/buttons.flash.min.js",
            "/transport_app/assets/datatables.net-buttons/js/buttons.html5.min.js",
            "/transport_app/assets/datatables.net-buttons/js/buttons.print.min.js",
            "/transport_app/assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
            "/transport_app/assets/datatables.net-keytable/js/dataTables.keyTable.min.js",
            "/transport_app/assets/datatables.net-responsive/js/dataTables.responsive.min.js",
            "/transport_app/assets/datatables.net-responsive-bs/js/responsive.bootstrap.js",
            "/transport_app/assets/datatables.net-scroller/js/dataTables.scroller.min.js",
            "/transport_app/assets/jszip/dist/jszip.min.js",
            "/transport_app/assets/pdfmake/build/pdfmake.min.js",
            "/transport_app/assets/pdfmake/build/vfs_fonts.js",
            "/transport_app/assets/custom.min.js",
            "/transport_app/assets/spahql.js",
            "/transport_app/assets/chosen/chosen.jquery.min.js"
        ]);
        ?>


    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="index.html" class="site_title"><i class="fa fa-truck"></i> <span>Transport Software</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                       
                        <!-- /menu profile quick info -->
                        <br />
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                               
                                <ul class="nav side-menu">
                                <li><a href=" <?php echo $this->Url->build(['controller' => 'Gst', 'action' => 'gstinvoice']); ?>"> <i class="fa fa-folder"></i> GST Sales<span class="fa fa-chevron-right"></span></a> </li>
                                <li><a href=" <?php echo $this->Url->build(['controller' => 'Gst', 'action' => 'purchaseinvoice']); ?>"> <i class="fa fa-folder"></i> GST Purchase<span class="fa fa-chevron-right"></span></a> </li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'insert2']); ?>"> <i class="fa fa-folder"></i> Dainik Entry   <span class="fa fa-chevron-right"></span></a> </li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'insert3']); ?>"> <i class="fa fa-folder"></i> Dainik Entry 2  <span class="fa fa-chevron-right"></span></a> </li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Customers', 'action' => 'index']); ?>"> <i class="fa fa-folder"></i> Sarva Khatedar  <span class="fa fa-chevron-right"></span></a>
                                    </li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'daily']); ?>"> <i class="fa fa-folder"></i> Dainik Tapshil  <span class="fa fa-chevron-right"></span></a>
                                    </li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Expenses', 'action' => 'index']); ?>"> <i class="fa fa-folder"></i> Dainik Kharch Yadi  <span class="fa fa-chevron-right"></span></a>
                                    </li>
                                    <li><a href="<?php echo $this->Url->build(['controller' => 'Customers', 'action' => 'add']); ?>"> <i class="fa fa-folder"></i>Navin Khatedar Form <span class="fa fa-chevron-right"></span></a>
                                  </li>

                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'displaylist']); ?>"> <i class="fa fa-folder"></i> Sagle Tapshil  <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Expenses', 'action' => 'add']); ?>"> <i class="fa fa-folder"></i> Dainik Kharch Form   <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'findday']); ?>"> <i class="fa fa-folder"></i> Tarkhenusar Dainik Tapshil  <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'makebill']); ?>"> <i class="fa fa-folder"></i> Vayaktik Khatedar Billing   <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Stocks', 'action' => 'add']); ?>"> <i class="fa fa-folder"></i> Dar Patrak Form   <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Stocks', 'action' => 'index']); ?>"> <i class="fa fa-folder"></i> Dar Patrak Yadi  <span class="fa fa-chevron-right"></span></a></li>
                             
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'SalesPayments', 'action' => 'pendingpayments']); ?>"> <i class="fa fa-folder"></i> Baki Payment Form   <span class="fa fa-chevron-right"></span></a></li>
                                    <li><a href=" <?php echo $this->Url->build(['controller' => 'Sales', 'action' => 'help']); ?>"> <i class="fa fa-folder"></i> Madat   <span class="fa fa-chevron-right"></span></a></li>
                                </ul>
                            </div>


                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <nav class="nav navbar-nav">
                            <ul class=" navbar-right">
                                <li class="nav-item dropdown open" style="padding-left: 15px;">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                        <img src="images/img.jpg" alt="">Administrator
                                    </a>
                                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item"  href="javascript:;"> Profile</a>
                                        <a class="dropdown-item"  href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                        <a class="dropdown-item"  href="javascript:;">Help</a>
                                        <a class="dropdown-item"  href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown open" style="padding-left: 15px;">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                        <img src="images/img.jpg" alt="">Messaging
                                    </a>
                                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item"  href="<?php echo $this->Url->build(['controller' => 'Sms', 'action' => 'sendsms']); ?>"> SMS </a>
                                        <a class="dropdown-item"  href="<?php echo $this->Url->build(['controller' => 'Email', 'action' => 'sendemail']); ?>"> Email</a>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Instarr Software - Serve An Infotech (Serveait) www.instarr.in 
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>



    </body>
</html>
