/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

(function(i){var q={vertical:false,rtl:false,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click",buttonNextCallback:null,buttonPrevCallback:null, itemFallbackDimension:null},r=false;i(window).bind("load.jcarousel",function(){r=true});i.jcarousel=function(a,c){this.options=i.extend({},q,c||{});this.autoStopped=this.locked=false;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===undefined)this.options.rtl=(i(a).attr("dir")||i("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical?this.options.rtl? "right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){i(a).removeClass(d[f]);b=d[f];break}if(a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"){this.list=i(a);this.container=this.list.parent();if(this.container.hasClass("jcarousel-clip")){if(!this.container.parent().hasClass("jcarousel-container"))this.container=this.container.wrap("<div></div>");this.container=this.container.parent()}else if(!this.container.hasClass("jcarousel-container"))this.container= this.list.wrap("<div></div>").parent()}else{this.container=i(a);this.list=this.container.find("ul,ol").eq(0)}b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.clip=this.list.parent();if(!this.clip.length||!this.clip.hasClass("jcarousel-clip"))this.clip=this.list.wrap("<div></div>").parent();this.buttonNext=i(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext= this.clip.after(this.options.buttonNextHTML).next();this.buttonNext.addClass(this.className("jcarousel-next"));this.buttonPrev=i(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=this.clip.after(this.options.buttonPrevHTML).next();this.buttonPrev.addClass(this.className("jcarousel-prev"));this.clip.addClass(this.className("jcarousel-clip")).css({overflow:"hidden",position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden", position:"relative",top:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"});!this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null;b=this.list.children("li");var e=this;if(b.size()>0){var g=0,k=this.options.offset;b.each(function(){e.format(this,k++);g+=e.dimension(this, j)});this.list.css(this.wh,g+100+"px");if(!c||c.size===undefined)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display","block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.reload()};this.options.initCallback!==null&&this.options.initCallback(this,"init");if(!r&&i.browser.safari){this.buttons(false,false);i(window).bind("load.jcarousel",function(){e.setup()})}else this.setup()}; var h=i.jcarousel;h.fn=h.prototype={jcarousel:"0.2.7"};h.fn.extend=h.extend=i.extend;h.fn.extend({setup:function(){this.prevLast=this.prevFirst=this.last=this.first=null;this.animating=false;this.tail=this.timer=null;this.inTail=false;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,true);this.prevFirst=this.prevLast=null;this.animate(a,false);i(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize)}}, reset:function(){this.list.empty();this.list.css(this.lt,"0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,h.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=false;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0; this.list.children("li").each(function(f){b+=a.dimension(this,c);if(f+1<a.first)d=b});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,false)},lock:function(){this.locked=true;this.buttons()},unlock:function(){this.locked=false;this.buttons()},size:function(a){if(a!==undefined){this.options.size=a;this.locked||this.buttons()}return this.options.size},has:function(a,c){if(c===undefined||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b= a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return false}return true},get:function(a){return i(".jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,f=i(c);if(b.length===0){var j,e=h.intval(a);for(b=this.create(a);;){j=this.get(--e);if(e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}}else d=this.dimension(b);if(f.get(0).nodeName.toUpperCase()=="LI"){b.replaceWith(f);b=f}else b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")), a);f=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null;d=this.dimension(b,f)-d;a>0&&a<this.first&&this.list.css(this.lt,h.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,h.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(!(!c.length||a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,h.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,h.intval(this.list.css(this.wh))- b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(false):this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(true):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!(this.locked|| this.animating||!this.tail)){this.pauseAuto();var c=h.intval(this.list.css(this.lt));c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){if(!(this.locked||this.animating)){this.pauseAuto();this.animate(this.pos(a),c)}},pos:function(a,c){var b=h.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;if(this.options.wrap!="circular")a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a;for(var d= this.first>a,f=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(f):this.get(this.last),e=d?f:f-1,g=null,k=0,l=false,m=0;d?--e>=a:++e<a;){g=this.get(e);l=!g.length;if(g.length===0){g=this.create(e).addClass(this.className("jcarousel-item-placeholder"));j[d?"before":"after"](g);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)){j=this.get(this.index(e));if(j.length)g=this.add(e,j.clone(true))}}j=g;m=this.dimension(g);if(l)k+= m;if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<=this.options.size)))b=d?b+m:b-m}f=this.clipping();var p=[],o=0,n=0;j=this.get(a-1);for(e=a;++o;){g=this.get(e);l=!g.length;if(g.length===0){g=this.create(e).addClass(this.className("jcarousel-item-placeholder"));j.length===0?this.list.prepend(g):j[d?"before":"after"](g);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)){j=this.get(this.index(e));if(j.length)g= this.add(e,j.clone(true))}}j=g;m=this.dimension(g);if(m===0) return false;if(this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size)p.push(g);else if(l)k+=m;n+=m;if(n>=f)break;e++}for(g=0;g<p.length;g++)p[g].remove();if(k>0){this.list.css(this.wh,this.dimension(this.list)+k+"px");if(d){b-=k;this.list.css(this.lt,h.intval(this.list.css(this.lt))-k+"px")}}k=a+o-1;if(this.options.wrap!="circular"&& this.options.size&&k>this.options.size)k=this.options.size;if(e>k){o=0;e=k;for(n=0;++o;){g=this.get(e--);if(!g.length)break;n+=this.dimension(g);if(n>=f)break}}e=k-o+1;if(this.options.wrap!="circular"&&e<1)e=1;if(this.inTail&&d){b+=this.tail;this.inTail=false}this.tail=null;if(this.options.wrap!="circular"&&k==this.options.size&&k-o+1>=1){d=h.margin(this.get(k),!this.options.vertical?"marginRight":"marginBottom");if(n-d>f)this.tail=n-f-d}if(c&&a===this.options.size&&this.tail){b-=this.tail;this.inTail= true}for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=k;return b},animate:function(a,c){if(!(this.locked||this.animating)){this.animating=true;var b=this,d=function(){b.animating=false;a===0&&b.list.css(b.lt,0);if(!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail))b.startAuto();b.buttons();b.notify("onAfterAnimation"); if(b.options.wrap=="circular"&&b.options.size!==null)for(var f=b.prevFirst;f<=b.prevLast;f++)if(f!==null&&!(f>=b.first&&f<=b.last)&&(f<1||f>b.options.size))b.remove(f)};this.notify("onBeforeAnimation");if(!this.options.animation||c===false){this.list.css(this.lt,a+"px");d()}else this.list.animate(!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},this.options.animation,this.options.easing,d)}},startAuto:function(a){if(a!==undefined)this.options.auto=a;if(this.options.auto===0)return this.stopAuto(); if(this.timer===null){this.autoStopped=false;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=true},pauseAuto:function(){if(this.timer!==null){window.clearTimeout(this.timer);this.timer=null}},buttons:function(a,c){if(a==null){a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size);if(!this.locked&&(!this.options.wrap||this.options.wrap== "first")&&this.options.size!==null&&this.last>=this.options.size)a=this.tail!==null&&!this.inTail}if(c==null){c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1);if(!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1)c=this.tail!==null&&this.inTail}var b=this;if(this.buttonNext.size()>0){this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext);a&&this.buttonNext.bind(this.options.buttonNextEvent+ ".jcarousel",this.funcNext);this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?false:true);this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)}else this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);if(this.buttonPrev.size()>0){this.buttonPrev.unbind(this.options.buttonPrevEvent+ ".jcarousel",this.funcPrev);c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev);this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?false:true);this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)}else this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b, null,c);this.buttonNextState=a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);if(this.prevFirst!==this.first){this.callback("itemFirstInCallback",a,c,this.first);this.callback("itemFirstOutCallback",a,c,this.prevFirst)}if(this.prevLast!==this.last){this.callback("itemLastInCallback",a,c,this.last);this.callback("itemLastOutCallback",a,c,this.prevLast)}this.callback("itemVisibleInCallback", a,c,this.first,this.last,this.prevFirst,this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var g=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(i.isFunction(g)){var k=this;if(d===undefined)g(k,b,c);else if(f===undefined)this.get(d).each(function(){g(k,this,d,b,c)});else{a=function(m){k.get(m).each(function(){g(k, this,m,b,c)})};for(var l=d;l<=f;l++)l!==null&&!(l>=j&&l<=e)&&a(l)}}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){a=i(a);for(var b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical? "-horizontal":"-vertical")},dimension:function(a,c){var b=a.jquery!==undefined?a[0]:a,d=!this.options.vertical?(b.offsetWidth||h.intval(this.options.itemFallbackDimension))+h.margin(b,"marginLeft")+h.margin(b,"marginRight"):(b.offsetHeight||h.intval(this.options.itemFallbackDimension))+h.margin(b,"marginTop")+h.margin(b,"marginBottom");if(c==null||d==c)return d;d=!this.options.vertical?c-h.margin(b,"marginLeft")-h.margin(b,"marginRight"):c-h.margin(b,"marginTop")-h.margin(b,"marginBottom");i(b).css(this.wh, d+"px");return this.dimension(b)},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-h.intval(this.clip.css("borderLeftWidth"))-h.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-h.intval(this.clip.css("borderTopWidth"))-h.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});h.extend({defaults:function(a){return i.extend(q,a||{})},margin:function(a,c){if(!a)return 0; var b=a.jquery!==undefined?a[0]:a;if(c=="marginRight"&&i.browser.safari){var d={display:"block","float":"none",width:"auto"},f,j;i.swap(b,d,function(){f=b.offsetWidth});d.marginRight=0;i.swap(b,d,function(){j=b.offsetWidth});return j-f}return h.intval(i.css(b,c))},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a}});i.fn.jcarousel=function(a){if(typeof a=="string"){var c=i(this).data("jcarousel"),b=Array.prototype.slice.call(arguments,1);return c[a].apply(c,b)}else return this.each(function(){i(this).data("jcarousel", new h(this,a))})}})(jQuery);
/**
 * jQuery lightBox plugin
 * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
 * and adapted to me for use like a plugin from jQuery.
 * @name jquery-lightbox-0.5.js
 * @author Leandro Vieira Pinho - http://leandrovieira.com
 * @version 0.5
 * @date April 11, 2008
 * @category jQuery plugin
 * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
 * @license CC Attribution-No Derivative Works 2.5 Brazil - http://creativecommons.org/licenses/by-nd/2.5/br/deed.en_US
 * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
 */

// Offering a Custom Alias suport - More info: http://docs.jquery.com/Plugins/Authoring#Custom_Alias
(function($) {
	/**
	 * $ is an alias to jQuery object
	 *
	 */
	$.fn.lightBox = function(settings) {
		// Settings to configure the jQuery lightBox plugin how you like
		settings = jQuery.extend({
			// Configuration related to overlay
			overlayBgColor: 		'#000',		// (string) Background color to overlay; inform a hexadecimal value like: #RRGGBB. Where RR, GG, and BB are the hexadecimal values for the red, green, and blue values of the color.
			overlayOpacity:			0.8,		// (integer) Opacity value to overlay; inform: 0.X. Where X are number from 0 to 9
			// Configuration related to navigation
			fixedNavigation:		false,		// (boolean) Boolean that informs if the navigation (next and prev button) will be fixed or not in the interface.
			// Configuration related to images
			imageLoading:			'/shared/bu/images/lightbox/lightbox-ico-loading.gif',		// (string) Path and the name of the loading icon
			imageBtnPrev:			'/shared/bu/images/lightbox/lightbox-btn-prev.gif',			// (string) Path and the name of the prev button image
			imageBtnNext:			'/shared/bu/images/lightbox/lightbox-btn-next.gif',			// (string) Path and the name of the next button image
			imageBtnClose:			'/shared/bu/images/lightbox/lightbox-btn-close.gif',		// (string) Path and the name of the close btn
			imageBlank:				'/shared/bu/images/lightbox/lightbox-blank.gif',			// (string) Path and the name of a blank image (one pixel)
			// Configuration related to container image box
			containerBorderSize:	10,			// (integer) If you adjust the padding in the CSS for the container, #lightbox-container-image-box, you will need to update this value
			containerResizeSpeed:	400,		// (integer) Specify the resize duration of container image. These number are miliseconds. 400 is default.
			// Configuration related to texts in caption. For example: Image 2 of 8. You can alter either "Image" and "of" texts.
			txtImage:				'Image',	// (string) Specify text "Image"
			txtOf:					'of',		// (string) Specify text "of"
			// Configuration related to keyboard navigation
			keyToClose:				'c',		// (string) (c = close) Letter to close the jQuery lightBox interface. Beyond this letter, the letter X and the SCAPE key is used to.
			keyToPrev:				'p',		// (string) (p = previous) Letter to show the previous image
			keyToNext:				'n',		// (string) (n = next) Letter to show the next image.
			// Don�t alter these variables in any way
			imageArray:				[],
			activeImage:			0
		},settings);
		// Caching the jQuery object with all elements matched
		var jQueryMatchedObj = this; // This, in this context, refer to jQuery object
        /**
		 * Initializing the plugin calling the start function
		 *
		 * @return boolean false
		 */
		function _initialize() {
			_start(this,jQueryMatchedObj); // This, in this context, refer to object (link) which the user have clicked
			return false; // Avoid the browser following the link
		}
		/**
		 * Start the jQuery lightBox plugin
		 *
		 * @param object objClicked The object (link) whick the user have clicked
		 * @param object jQueryMatchedObj The jQuery object with all elements matched
		 */
		function _start(objClicked,jQueryMatchedObj) {
			// Hime some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
			$('embed, object, select').css({ 'visibility' : 'hidden' });
			// Call the function to create the markup structure; style some elements; assign events in some elements.
			_set_interface();
			// Unset total images in imageArray
			settings.imageArray.length = 0;
			// Unset image active information
			settings.activeImage = 0;
			// We have an image set? Or just an image? Let�s see it.
			if ( jQueryMatchedObj.length == 1 ) {
				settings.imageArray.push(new Array(objClicked.getAttribute('href'),objClicked.getAttribute('title')));
			} else {
				// Add an Array (as many as we have), with href and title atributes, inside the Array that storage the images references
				for ( var i = 0; i < jQueryMatchedObj.length; i++ ) {
					settings.imageArray.push(new Array(jQueryMatchedObj[i].getAttribute('href'),jQueryMatchedObj[i].getAttribute('title')));
				}
			}
			while ( settings.imageArray[settings.activeImage][0] != objClicked.getAttribute('href') ) {
				settings.activeImage++;
			}
			// Call the function that prepares image exibition
			_set_image_to_view();
		}
		/**
		 * Create the jQuery lightBox plugin interface
		 *
		 * The HTML markup will be like that:
			<div id="jquery-overlay"></div>
			<div id="jquery-lightbox">
				<div id="lightbox-container-image-box">
					<div id="lightbox-container-image">
						<img src="../fotos/XX.jpg" id="lightbox-image">
						<div id="lightbox-nav">
							<a href="#" id="lightbox-nav-btnPrev"></a>
							<a href="#" id="lightbox-nav-btnNext"></a>
						</div>
						<div id="lightbox-loading">
							<a href="#" id="lightbox-loading-link">
								<img src="../images/lightbox-ico-loading.gif">
							</a>
						</div>
					</div>
				</div>
				<div id="lightbox-container-image-data-box">
					<div id="lightbox-container-image-data">
						<div id="lightbox-image-details">
							<span id="lightbox-image-details-caption"></span>
							<span id="lightbox-image-details-currentNumber"></span>
						</div>
						<div id="lightbox-secNav">
							<a href="#" id="lightbox-secNav-btnClose">
								<img src="../images/lightbox-btn-close.gif">
							</a>
						</div>
					</div>
				</div>
			</div>
		 *
		 */
		function _set_interface() {
			// Apply the HTML markup into body tag
			$('body').append('<div id="jquery-overlay"></div><div id="jquery-lightbox"><div id="lightbox-container-image-box"><div id="lightbox-container-image"><img id="lightbox-image"><div style="" id="lightbox-nav"><a href="#" id="lightbox-nav-btnPrev"></a><a href="#" id="lightbox-nav-btnNext"></a></div><div id="lightbox-loading"><a href="#" id="lightbox-loading-link"><img src="' + settings.imageLoading + '"></a></div></div></div><div id="lightbox-container-image-data-box"><div id="lightbox-container-image-data"><div id="lightbox-image-details"><span id="lightbox-image-details-caption"></span><span id="lightbox-image-details-currentNumber"></span></div><div id="lightbox-secNav"><a href="#" id="lightbox-secNav-btnClose"><img src="' + settings.imageBtnClose + '"></a></div></div></div></div>');
			// Get page sizes
			var arrPageSizes = ___getPageSize();
			// Style overlay and show it
			$('#jquery-overlay').css({
				backgroundColor:	settings.overlayBgColor,
				opacity:			settings.overlayOpacity,
				width:				arrPageSizes[0],
				height:				arrPageSizes[1]
			}).fadeIn();
			// Get page scroll
			var arrPageScroll = ___getPageScroll();
			// Calculate top and left offset for the jquery-lightbox div object and show it
			$('#jquery-lightbox').css({
				top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
				left:	arrPageScroll[0]
			}).show();
			// Assigning click events in elements to close overlay
			$('#jquery-overlay,#jquery-lightbox').click(function() {
				_finish();
			});
			// Assign the _finish function to lightbox-loading-link and lightbox-secNav-btnClose objects
			$('#lightbox-loading-link,#lightbox-secNav-btnClose').click(function() {
				_finish();
				return false;
			});
			// If window was resized, calculate the new overlay dimensions
			$(window).resize(function() {
				// Get page sizes
				var arrPageSizes = ___getPageSize();
				// Style overlay and show it
				$('#jquery-overlay').css({
					width:		arrPageSizes[0],
					height:		arrPageSizes[1]
				});
				// Get page scroll
				var arrPageScroll = ___getPageScroll();
				// Calculate top and left offset for the jquery-lightbox div object and show it
				$('#jquery-lightbox').css({
					top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
					left:	arrPageScroll[0]
				});
			});
		}
		/**
		 * Prepares image exibition; doing a image�s preloader to calculate it�s size
		 *
		 */
		function _set_image_to_view() { // show the loading
			// Show the loading
			$('#lightbox-loading').show();
			if ( settings.fixedNavigation ) {
				$('#lightbox-image,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
			} else {
				// Hide some elements
				$('#lightbox-image,#lightbox-nav,#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
			}
			// Image preload process
			var objImagePreloader = new Image();
			objImagePreloader.onload = function() {
                $('#lightbox-image').attr('src',settings.imageArray[settings.activeImage][0]);
				// Perfomance an effect in the image container resizing it
				_resize_container_image_box(objImagePreloader.width,objImagePreloader.height);
				//	clear onLoad, IE behaves irratically with animated gifs otherwise
				objImagePreloader.onload=function(){};
			};
			objImagePreloader.src = settings.imageArray[settings.activeImage][0];
		};
		/**
		 * Perfomance an effect in the image container resizing it
		 *
		 * @param integer intImageWidth The image�s width that will be showed
		 * @param integer intImageHeight The image�s height that will be showed
		 */
		function _resize_container_image_box(intImageWidth,intImageHeight) {
			// Get current width and height
			var intCurrentWidth = $('#lightbox-container-image-box').width();
			var intCurrentHeight = $('#lightbox-container-image-box').height();
			// Get the width and height of the selected image plus the padding
			var intWidth = (intImageWidth + (settings.containerBorderSize * 2)); // Plus the image�s width and the left and right padding value
			var intHeight = (intImageHeight + (settings.containerBorderSize * 2)); // Plus the image�s height and the left and right padding value
			// Diferences
			var intDiffW = intCurrentWidth - intWidth;
			var intDiffH = intCurrentHeight - intHeight;
			// Perfomance the effect
			$('#lightbox-container-image-data-box').css({width:intWidth-20});
			$('#lightbox-container-image-box').animate({ width: intWidth, height: intHeight },settings.containerResizeSpeed,function() { _show_image(); });
			if ( ( intDiffW == 0 ) && ( intDiffH == 0 ) ) {
				if ( $.browser.msie ) {
					___pause(250);
				} else {
					___pause(100);
				}
			}
			//$('#lightbox-container-image-data-box').css({ width: intImageWidth });
			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ height: intImageHeight + (settings.containerBorderSize * 2) });
		};
		/**
		 * Show the prepared image
		 *
		 */
		function _show_image() {
			$('#lightbox-loading').hide();
			$('#lightbox-image').fadeIn(function() {
				_show_image_data();
				_set_navigation();
			});
			_preload_neighbor_images();
		};
		/**
		 * Show the image information
		 *
		 */
		function _show_image_data() {
			$('#lightbox-container-image-data-box').slideDown('fast');
			$('#lightbox-image-details-caption').hide();
			if ( settings.imageArray[settings.activeImage][1] ) {
				$('#lightbox-image-details-caption').html(settings.imageArray[settings.activeImage][1]).show();
			}
			// If we have a image set, display 'Image X of X'
			if ( settings.imageArray.length > 1 ) {
				$('#lightbox-image-details-currentNumber').html(settings.txtImage + ' ' + ( settings.activeImage + 1 ) + ' ' + settings.txtOf + ' ' + settings.imageArray.length).show();
			}
		}
		/**
		 * Display the button navigations
		 *
		 */
		function _set_navigation() {
			$('#lightbox-nav').show();

			// Instead to define this configuration in CSS file, we define here. And it�s need to IE. Just.
			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });

			// Show the prev button, if not the first image in set
			if ( settings.activeImage != 0 ) {
				if ( settings.fixedNavigation ) {
					$('#lightbox-nav-btnPrev').css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' })
						.unbind()
						.bind('click',function() {
							settings.activeImage = settings.activeImage - 1;
							_set_image_to_view();
							return false;
						});
				} else {
					// Show the images button for Next buttons
					$('#lightbox-nav-btnPrev').unbind().hover(function() {
						$(this).css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' });
					},function() {
						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
					}).show().bind('click',function() {
						settings.activeImage = settings.activeImage - 1;
						_set_image_to_view();
						return false;
					});
				}
			} else {
				$('#lightbox-nav-btnPrev').unbind('click');
			}

			// Show the next button, if not the last image in set
			if ( settings.activeImage != ( settings.imageArray.length -1 ) ) {
				if ( settings.fixedNavigation ) {
					$('#lightbox-nav-btnNext').css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' })
						.unbind()
						.bind('click',function() {
							settings.activeImage = settings.activeImage + 1;
							_set_image_to_view();
							return false;
						});
				} else {
					// Show the images button for Next buttons
					$('#lightbox-nav-btnNext').unbind().hover(function() {
						$(this).css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' });
					},function() {
						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
					}).show().bind('click',function() {
						settings.activeImage = settings.activeImage + 1;
						_set_image_to_view();
						return false;
					});
				}
			} else {
				$('#lightbox-nav-btnNext').unbind('click');
			}
			// Enable keyboard navigation
			_enable_keyboard_navigation();
		}
		/**
		 * Enable a support to keyboard navigation
		 *
		 */
		function _enable_keyboard_navigation() {
			$(document).keydown(function(objEvent) {
				_keyboard_action(objEvent);
			});
		}
		/**
		 * Disable the support to keyboard navigation
		 *
		 */
		function _disable_keyboard_navigation() {
			$(document).unbind();
		}
		/**
		 * Perform the keyboard actions
		 *
		 */
		function _keyboard_action(objEvent) {
			// To ie
			if ( objEvent == null ) {
				keycode = event.keyCode;
				escapeKey = 27;
			// To Mozilla
			} else {
				keycode = objEvent.keyCode;
				escapeKey = objEvent.DOM_VK_ESCAPE;
			}
			// Get the key in lower case form
			key = String.fromCharCode(keycode).toLowerCase();
			// Verify the keys to close the ligthBox
			if ( ( key == settings.keyToClose ) || ( key == 'x' ) || ( keycode == escapeKey ) ) {
				_finish();
			}
			// Verify the key to show the previous image
			if ( ( key == settings.keyToPrev ) || ( keycode == 37 ) ) {
				// If we�re not showing the first image, call the previous
				if ( settings.activeImage != 0 ) {
					settings.activeImage = settings.activeImage - 1;
					_set_image_to_view();
					_disable_keyboard_navigation();
				}
			}
			// Verify the key to show the next image
			if ( ( key == settings.keyToNext ) || ( keycode == 39 ) ) {
				// If we�re not showing the last image, call the next
				if ( settings.activeImage != ( settings.imageArray.length - 1 ) ) {
					settings.activeImage = settings.activeImage + 1;
					_set_image_to_view();
					_disable_keyboard_navigation();
				}
			}
		}
		/**
		 * Preload prev and next images being showed
		 *
		 */
		function _preload_neighbor_images() {
			if ( (settings.imageArray.length -1) > settings.activeImage ) {
				objNext = new Image();
				objNext.src = settings.imageArray[settings.activeImage + 1][0];
			}
			if ( settings.activeImage > 0 ) {
				objPrev = new Image();
				objPrev.src = settings.imageArray[settings.activeImage -1][0];
			}
		}
		/**
		 * Remove jQuery lightBox plugin HTML markup
		 *
		 */
		function _finish() {
			$('#jquery-lightbox').remove();
			$('#jquery-overlay').fadeOut(function() { $('#jquery-overlay').remove(); });
			// Show some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
			$('embed, object, select').css({ 'visibility' : 'visible' });
		}
		/**
		 / THIRD FUNCTION
		 * getPageSize() by quirksmode.com
		 *
		 * @return Array Return an array with page width, height and window width, height
		 */
		function ___getPageSize() {
			var xScroll, yScroll;
			if (window.innerHeight && window.scrollMaxY) {
				xScroll = window.innerWidth + window.scrollMaxX;
				yScroll = window.innerHeight + window.scrollMaxY;
			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
				xScroll = document.body.scrollWidth;
				yScroll = document.body.scrollHeight;
			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
				xScroll = document.body.offsetWidth;
				yScroll = document.body.offsetHeight;
			}
			var windowWidth, windowHeight;
			if (self.innerHeight) {	// all except Explorer
				if(document.documentElement.clientWidth){
					windowWidth = document.documentElement.clientWidth;
				} else {
					windowWidth = self.innerWidth;
				}
				windowHeight = self.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (document.body) { // other Explorers
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}
			// for small pages with total height less then height of the viewport
			if(yScroll < windowHeight){
				pageHeight = windowHeight;
			} else {
				pageHeight = yScroll;
			}
			// for small pages with total width less then width of the viewport
			if(xScroll < windowWidth){
				pageWidth = xScroll;
			} else {
				pageWidth = windowWidth;
			}
			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
			return arrayPageSize;
		};
		/**
		 / THIRD FUNCTION
		 * getPageScroll() by quirksmode.com
		 *
		 * @return Array Return an array with x,y page scroll values.
		 */
		function ___getPageScroll() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;
			}
			arrayPageScroll = new Array(xScroll,yScroll);
			return arrayPageScroll;
		};
		 /**
		  * Stop the code execution from a escified time in milisecond
		  *
		  */
		 function ___pause(ms) {
			var date = new Date();
			curDate = null;
			do { var curDate = new Date(); }
			while ( curDate - date < ms);
		 };
		// Return the jQuery object for chaining. The unbind method is used to avoid click conflict when the plugin is called more than once
		return this.unbind('click').click(_initialize);
	};
})(jQuery); // Call and execute the function immediately passing the jQuery object
//document.onclick=auctionViewClick;
function auctionViewClick(e){
    if (!e){ 
        e = window.event;
    }
    var theTarget = e.target ? e.target : e.srcElement;
    var pNode;
    if($(theTarget).hasClass('tabNavMiddle'))
    {
        pNode = $(theTarget);
    }
    else {
        $(theTarget).removeAttr("href");   //to avoid anchor to be clicked
        pNode = $(theTarget).parent();
    }
    var pClass = pNode.attr("class");
    pClass = pClass.split(" ")[0];
    if(pClass=="tabNavMiddle"){
        var parentNode = $($(pNode).parent());
        if (parentNode.hasClass("selected")) {
            return false;
        }else {
            $('div.tabsContainer div.selected').removeClass("selected");
            parentNode.addClass("selected");
            eval(parentNode.attr("id")+"()");
            return false;
        }
    }
}


function summaryTab(){
    $('#summaryTabBody').show();
    $('#shippingTabBody').hide();
    $('#termsTabBody').hide();

}

function shippingTab(){
    $('#summaryTabBody').hide();
    $('#shippingTabBody').show();
    $('#termsTabBody').hide();
}

function termsTab(){
    $('#summaryTabBody').hide();
    $('#shippingTabBody').hide();
    $('#termsTabBody').show();
}

function activateLightBox(){
    $('#imageContainerLarge a').lightBox({
        fixedNavigation:true
    });
    $('.imageEnlargeContainer a').lightBox({
        fixedNavigation:true
    });
}

function changePicture(index){
    $('div#imageContainerLarge').find('a').each(function(){
        if($(this).attr('id')==index)
        {
            $(this).parent('.imageContainerClass').css({
                'display':'block'
            });
        }
        else
        {
            $(this).parent('.imageContainerClass').css({
                'display':'none'
            });
        }
    });
    activateLightBox();
}

function activateShippingPopup(){
    var shipping_terms = $('div#auctionView_shipping_terms label').html();
    if(!shipping_terms)
    {
        var shipping_terms_temp = $('#shippingTermsAsset').find('label')[1];
        shipping_terms=$(shipping_terms_temp).text();
    }
    var spaceDotFix = /[\s.]/g;
    if(shipping_terms)
    {
        shipping_terms = shipping_terms.replace(spaceDotFix,"_").toLowerCase();
        $('.'+shipping_terms).css({
            "display":"block",
            "width":"auto",
            'float':'none'
        });
    }
}

function constructCarousel(){
    if(auctionView_images.length>0){
        $('#imageContainerCarousel').jcarousel({
            size:auctionView_images.length,
            visible:4,
            scroll:4,
            //initCallback: storeCarousel,
            itemLoadCallback: {
                onBeforeAnimation: function(carousel,state){
                    for (var i = carousel.first; i <= carousel.last; i++) {
                        // Check if the item already exists
                        if (!carousel.has(i)) {
                            // Add the item
                            carousel.add(i, getAuctionViewItemHTML(auctionView_images[i-1],i-1));
                        }
                    }
                }
            }
        });
    }
}

$(document).ready(function(){

    //added to extract and display for __shipterms and _termscondition from description.
    $("div.__shipterms").show();
    $("div.__termscondition").show();
    $.each($("div.__shipterms").find('li'), function(){
        $('#description_shipterms').find('ul').append('<li>'+this.innerHTML+'</li>');
        $('#description_shipterms').show();
    });

    $.each($("div.__termscondition").find('li'), function(){
        $('#description_termscondition').find('ul').append('<li>'+this.innerHTML+'</li>');
        $('#description_termscondition').show();
    });


    // added to remove photo,adding width 100% to table,bgcolor='' and view all photos from the description table.
    $('#description_table').find('a[href="javascript:openSlideshow()"]').hide();
    var description_table=$('#description_table').find("table:first");
    if(description_table)
    {
        description_table.css({
            'width':'100%'
        });
        description_table.attr('bgcolor','');
        if($.browser.msie) {
            description_table.attr('bgColor','');
        }
        description_table.find("tr").eq(1).hide();
    }



    //populating the lightbox images.
    if(auctionView_images.length>0){
        for(var i=1;i<auctionView_images.length;i++){
            var display="none";
            if(i==0){
                display="block";
            }
            $('div#imageContainerLarge').append(getAuctionViewLightBoxHTML(auctionView_images[i],display,i));
        }
    }else{
        $('div#imageContainerLarge').html("No Images Available");
    }
    if(auctionView_images.length>0){
        for(var i=0;i<auctionView_images.length;i++){
            var display="none";
            if(i==0){
                display="block";
            }
            $('div.imageEnlargeContainer').append(getAuctionViewLightBoxTextHTML(auctionView_images[i],display));
        }
    }
    activateLightBox();
    if($('div#auction_type').length!=0)
    {
        var auctionTypePDiv = $('div#auction_type a').attr("helptxt").toLowerCase();
    }
    if($('div#auction_condition').length!=0 )
    {
        var auctionConditionPDiv = $('div#auction_condition a').attr("helptxt").toLowerCase();
    }
    if($('div#auction_condition_asset').length!=0 )
    {
        var auctionConditionPDiv = $('div#auction_condition_asset a').attr("helptxt").toLowerCase();
    }
    // set z-index hack for summary pop
    function setZindex(obj){
        $("div.sectionRow").is(":visible")? $("div.sectionRow").css("z-index" ,"0") : "";
        $(obj).parent().parent().css("z-index" ,"1");
    }

    $('div#auction_condition_asset a').bind("click",function(){
        //	var pDiv = $(this).attr("helptxt").toLowerCase();
        $('div.auctionConditionPopupAsset').is(":visible")?$('div.auctionConditionPopupAsset').hide().find("div."+auctionConditionPDiv).hide():$('div.auctionConditionPopupAsset').show().find("div."+auctionConditionPDiv).show();
        setZindex(this);
        $('div.proxyBiddingPopup').hide();
        $('div#auction_condition_asset').css('z-index','1')
        $('div.shippingTermsPopupAsset').hide();
        $('div#shippingTermsAsset').css('z-index','0');
		 
    });
	
    $('div#shippingTermsAsset a').bind("click",function(){
        $('div.shippingTermsPopupAsset').is(":visible")?$('div.shippingTermsPopupAsset').hide():$('div.shippingTermsPopupAsset').show();
       //setZindex(this);
        $('div#shippingTermsAsset').css('z-index','1');
        $('div#auction_condition_asset').css('z-index','0')
        $('div.proxyBiddingPopup').hide();
        $('div.auctionConditionPopupAsset').hide();
    });

    $('div#auction_type a').bind("click",function(){
        //var pDiv = $(this).attr("helptxt").toLowerCase();
        $('div.auctionTypePopup').is(":visible")?$('div.auctionTypePopup').hide().find("div."+auctionTypePDiv).hide():$('div.auctionTypePopup').show().find("div."+auctionTypePDiv).show();
        $('div.auctionConditionPopup').hide().find("div."+auctionConditionPDiv).hide();
        $('div.quantityInVariancePopup').hide();
        $('div.buyersPremiumPopup').hide();
        $('div.proxyBiddingPopup').hide();
        $('div.shippingTermsPopup_summary').hide();
        setZindex(this);
    });

    $('div#auction_condition a').bind("click",function(){
        //	var pDiv = $(this).attr("helptxt").toLowerCase();
        $('div.auctionConditionPopup').is(":visible")?$('div.auctionConditionPopup').hide().find("div."+auctionConditionPDiv).hide():$('div.auctionConditionPopup').show().find("div."+auctionConditionPDiv).show();
        $('div.auctionTypePopup').hide().find("div."+auctionTypePDiv).hide();
        $('div.quantityInVariancePopup').hide();
        $('div.buyersPremiumPopup').hide();
        $('div.proxyBiddingPopup').hide();
        $('div.shippingTermsPopup_summary').hide();
        $('div.shippingTermsPopup').hide();
        $('div.partNumberPopup').hide();
        $('div.billNumberPopup').hide();
        $('div.shippingRestrictionsPopup').hide();
        setZindex(this);
    });

    $('div#quantity_in_variance a').bind("click",function(){
        $('div.quantityInVariancePopup').is(":visible")?$('div.quantityInVariancePopup').hide():$('div.quantityInVariancePopup').show();
        $('div.auctionTypePopup').hide().find("div."+auctionTypePDiv).hide();
        $('div.auctionConditionPopup').hide().find("div."+auctionConditionPDiv).hide();
        $('div.buyersPremiumPopup').hide();
        $('div.proxyBiddingPopup').hide();
        $('div.shippingTermsPopup_summary').hide();
        setZindex(this);

    });
    $('div#buyers_premium a').bind("click",function(){
        $('div.buyersPremiumPopup').is(":visible")?$('div.buyersPremiumPopup').hide():$('div.buyersPremiumPopup').show();
        $('div.auctionTypePopup').hide().find("div."+auctionTypePDiv).hide();
        $('div.auctionConditionPopup').hide().find("div."+auctionConditionPDiv).hide();
        $('div.quantityInVariancePopup').hide();
        $('div.proxyBiddingPopup').hide();
        $('div.shippingTermsPopup_summary').hide();
        setZindex(this);
    });


    $('div#auctionView_size_class a').bind("click",function(){
        $('div.sizeClassPopup').is(":visible")?$('div.sizeClassPopup').hide():$('div.sizeClassPopup').show();
        $('div.shippingTermsPopup').hide();
        $('div.proxyBiddingPopup').hide();
        setZindex(this);
    });
    $('div#auctionView_shipping_terms a').bind("click",function(){
        $('div.shippingTermsPopup').is(":visible")?$('div.shippingTermsPopup').hide():$('div.shippingTermsPopup').show();
        $('div.sizeClassPopup').hide();
        $('div.proxyBiddingPopup').hide();
        $('div.partNumberPopup').hide();
        $('div.billNumberPopup').hide();
        $('div.shippingRestrictionsPopup').hide();
        setZindex(this);
    });
    $('div#auctionView_shipping_terms_summary a').bind("click",function(){
        $('div.shippingTermsPopup_summary').is(":visible")?$('div.shippingTermsPopup_summary').hide():$('div.shippingTermsPopup_summary').show();
        $('div.buyersPremiumPopup').hide();
        $('div.quantityInVariancePopup').hide();
        $('div.auctionConditionPopup').hide();
        $('div.auctionTypePopup').hide();
        setZindex(this);
    });

    $('div#auctionView_shipping_restrictions a').bind("click",function(){
        $('div.shippingRestrictionsPopup').is(":visible")?$('div.shippingRestrictionsPopup').hide():$('div.shippingRestrictionsPopup').show();
        $('div.partNumberPopup').hide();
        $('div.billNumberPopup').hide();
        $('div.auctionConditionPopup').hide();
        $('div.auctionTypePopup').hide();
        $('div.shippingTermsPopup').hide()
        setZindex(this);
    });


    $('div#proxyBiddingPopupId a').bind("click",function(){
        $('div.proxyBiddingPopup').is(":visible")?$('div.proxyBiddingPopup').hide():$('div.proxyBiddingPopup').show();
        $('div.auctionConditionPopup').hide().find("div."+auctionConditionPDiv).hide();
        $('div.quantityInVariancePopup').hide();
        $('div.buyersPremiumPopup').hide();
        $('div.auctionTypePopup').hide().find("div."+auctionTypePDiv).hide();
        $('div.sizeClassPopup').hide();
        $('div.shippingTermsPopup').hide();
        $('div.shippingTermsPopupAsset').hide();
        $('div#shippingTermsAsset').css('z-index','0');
        $('div.shippingTermsPopup_summary').hide();
        $('div.auctionConditionPopupAsset').hide();
        $('div#auction_condition_asset').css('z-index','1')
        $("div.sectionRow").is(":visible")? $("div.sectionRow").css("z-index" ,"0") : "";
        $(this).parent().css("z-index" ,"6");
    });
	
    $('div.auctionConditionPopupAsset img').bind("click",function(){
        $('div.auctionConditionPopupAsset').hide();
    });

    $('div.proxyBiddingPopup img').bind("click",function(){
        $('div.proxyBiddingPopup').hide();
    });
	
    $('div.shippingRestrictionsPopup img').bind("click",function(){
        $('div.shippingRestrictionsPopup').hide();
        $('div.shippingRestrictionsPopup').css('z-index','1');
    });
  	
    $('div.auctionTypePopup img').bind("click",function(){
        $('div.auctionTypePopup').hide();
    });
    $('div.auctionConditionPopup img').bind("click",function(){
        $('div.auctionConditionPopup').hide();
    });
    $('div.quantityInVariancePopup img').bind("click",function(){
        $('div.quantityInVariancePopup').hide();
    });
    $('div.buyersPremiumPopup img').bind("click",function(){
        $('div.buyersPremiumPopup').hide();
    });

    $('div.sizeClassPopup img').bind("click",function(){
        $('div.sizeClassPopup').hide();
    });
    $('div.shippingTermsPopup img').bind("click",function(){
        $('div.shippingTermsPopup').hide();
    });
    $('div.shippingTermsPopupAsset img').bind("click",function(){
        $('div.shippingTermsPopupAsset').hide();
        $('div#shippingTermsAsset').css('z-index','0');
    });
    $('div.shippingTermsPopup_summary img').bind("click",function(){
        $('div.shippingTermsPopup_summary').hide();
    });

    activateShippingPopup();
});

function carousel_itemVisibleInCallback(carousel, item, i, state, evt)
{
    // The index() method calculates the index from a
    // given index who is out of the actual item range.
    var idx = carousel.index(i, auctionView_images.length);
    // filter duplicate entries.
    if (!carousel.has(i)) {
        // pass the position id for heat map tracking.
        //carousel.add(i, getItemHTML(carousel_new_list[idx - 1],container_id, i-1));
        carousel.add(i, getAuctionViewItemHTML(auctionView_images[idx-1],idx-1));
    }
};

function carousel_itemVisibleOutCallback(carousel, item, i, state, evt)
{
    carousel.remove(i);
};
	
function bidBoxRefresh(id,status)
{

    Invoker.bidBoxDataCall(id,status,function(data){
        if(data && !data.error)
        {
            var result=data.result[0];
            if(result.winner_flag == 1){
                $('.currentBidUpdate').html('<b>$'+result.current_bid+'</b>').formatCurrency({ symbol: lsi.currencySymbol() });
            }else{
                $('.currentBidUpdate').html('$'+result.current_bid).formatCurrency({ symbol: lsi.currencySymbol() });
            }
	     if(result.buy_now_price && result.buy_now_price.buy_now_eligible=="Y"){
			$('.nextBidUpdate').html('$'+result.buy_now_price.buynow_price).formatCurrency({ symbol: lsi.currencySymbol() });
			var unitPrice = result.buy_now_price.buynow_price / result.quantity;
			$('.ppUpdate').html('$'+unitPrice).formatCurrency({ symbol: lsi.currencySymbol() });
	     }else{
            		$('.nextBidUpdate').html('$'+result.next_bid).formatCurrency({ symbol: lsi.currencySymbol() });
			$('.ppUpdate').html('$'+result.price_per_unit).formatCurrency({ symbol: lsi.currencySymbol() });
		}	
//            $('.nextBidUpdate').html('$'+result.next_bid).formatCurrency({ symbol: lsi.currencySymbol() });
//            $('.ppUpdate').html('$'+result.price_per_unit).formatCurrency({ symbol: lsi.currencySymbol() });
            $('.noOfBidsUpdate').html(result.no_of_bids);
            $('.strRetailPrice').html('$'+result.retail_price).formatCurrency({ symbol: lsi.currencySymbol() });
			if(result.buy_now_price.buy_now_eligible=="Y")
			{
				$('.strBuyNowPrice').html('$'+result.buy_now_price.buynow_price).formatCurrency({ symbol: lsi.currencySymbol() });
				if(result.buy_now_price.discount_rate > 0)
				{
					$('.strOfferRateUpdate').html(result.buy_now_price.discount_rate*100+'%'+' off of retail');
				}	
			}	
	
            time_json=eval( '(' + result.time_until_close + ')');
            if(time_json != null)  {
                $('#daysUpdate').html(time_json.days);
                $('#hoursUpdate').html(time_json.hours);
                $('#minutesUpdate').html(time_json.minutes);
                $('#secondsUpdate').html(time_json.seconds);
            }else{
                $("#AuctionClosedDiv").show();
                $("#winnigBidDiv").show();
                $("#TimeDisplayDiv").hide();
                $(".formDisplayDiv").hide();

            }
	
            $('#fullTime').html(calcESTTime(result.close_time));
	
            if(result.record_status=="A" && result.auction_type_code!="SLB" && result.number_of_bids > 0 )
                $('.bidHistoryUpdate').html('Total Bids (<span><a  href="/auction/bidhistory?cmd=historyCurrent&auctionId='+result.auction_view.id+'">View History</a></span>):').show();
            if(result.number_of_bids > 0)
            {
                $('#winnerHtml').show();
                if(!result.winner_flag)
                {
                    $('#winnerHtml').html('<label>Current High Bidder: </label><label class="value">'+result.winner+'</label>');
                }
                else
                {
                    $('#winnerHtml').html('<label>You are the Current Highest Bidder</label>');
                }
		
            }
            var now = data.meta.time;
            if ((result.close_flag == 0 && result.close_time > now && result.number_of_lots_remaining > 0 && result.open_time <= now))
            {
                $(".formDisplayDiv").show();
                $("#TimeDisplayDiv").show();

                $(".winnigBidDiv").hide();
                $("#AuctionClosedDiv").hide();

            }
            else
            {
                $(".winnigBidDiv").show();
                $("#AuctionClosedDiv").show();
                $(".formDisplayDiv").hide();
                $("#TimeDisplayDiv").hide();
		 
            }
	
            var seconds_flag=0;
            if(time_json!=null && time_json.days==0 && time_json.hours==0 && time_json.minutes < 15 )
            {
                seconds_flag=1;
            }
	
            if(seconds_flag)
            {	
                $("#SecondsDiv").show();
                $('.std3MinMessage1').show();
                $($('.std3MinMessage2')[0]).css('border','0');
            }
            clearInterval(onemin_tt);clearInterval(fivemin_tt);
            if(result.close_flag == 0 && result.close_time > now && result.number_of_lots_remaining > 0 && result.open_time <= now)
            {
                if(time_json!=null){
                    if (time_json.days==0 && time_json.hours==0 && time_json.minutes < 6 ){
                        onemin_tt=setInterval(function(){
                            bidBoxRefresh(result.auction_view.id,result.record_status);
                        },60000);
                    }else{
                        fivemin_tt=setInterval(function(){
                            bidBoxRefresh(result.auction_view.id,result.record_status);
                        },300000);
                    }
                }
            }
			
            if(result.auction_type_code=="REG")
            {
                $('.bidAmountCheck').html('Enter $'+result.next_bid+' or more');
                $('.standardAuctionUpdate').removeAttr('onclick').bind('click',function(){
			  
                    //  return bidAmountValidate(this,result.next_bid);
                    return validateAndLogin(this,result.next_bid,username,'/auction/bid?cmd=confirm&auctionId='+id+'');
			  
                });
				
            }
            if(result.auction_type_code=="SLB")
            {
                $('.bidAmountCheck').html('Enter $'+result.next_bid+' or more');
                $('.sealedAuctionUpdate').removeAttr('onclick').bind('click',function(){
			  
                    //  return bidAmountValidate(this,result.next_bid);
                    return validateAndLogin(this,result.next_bid,username,'/auction/bid?cmd=confirm&auctionId='+id+'');
			  
                });
            }
			
			
			
			
        }

    });


}

function calcESTTime(time) {
    // create Date object for current location
    d = new Date(time*1000);

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    //offset of EST
    var offset = '-5';

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*offset));

    // return time as a Date
    getFormattime(new Date(nd.toLocaleString()));
}


function getFormattime(now){
    var curr_date = now.getDate();
    var curr_month = now.getMonth();
    curr_month = (curr_month+1).toString();
    var curr_year = (now.getFullYear()).toString();
    curr_year = curr_year.substring(2,curr_year.length);
    if(curr_month.length == 1){
        curr_month = "0"+curr_month;
    }
    var curr_hour = now.getHours();
    var curr_min = (now.getMinutes()).toString();
    if(curr_min.length == 1){
        curr_min = "0"+curr_min;
    }
    if (curr_hour < 12)
    {    
        a_p = "AM";
    }
    else
    {    
        a_p = "PM";
    }
    if (curr_hour == 0)
    {    
        curr_hour = 12;
    }
    if (curr_hour > 12)
    {    
        curr_hour = curr_hour - 12;
    }
    var dateString = curr_month+"/"+curr_date+"/"+curr_year+" "+curr_hour+":"+curr_min+" "+a_p;
    return dateString;
}

function validateAndLogin(obj,minimum_bid,userId,url){
   	var bidAmount = $("input[type='text'][name='bidAmount']").val();
	bidAmount = bidAmount.split(",").join("");
    url=url+"&bidAmount="+bidAmount+"&bidQuantity="+$("input[type='hidden'][name='bidQuantity']").val();
    if(bidAmountValidate(obj,minimum_bid)){
        loginPopupCheck(userId,url);
        return false;
    }
    return false;
}

function loginPopupCheck(userId,url){
    var id= userId?userId:global_user_id;
    if(id == ""||id == undefined){
        url = "/login?page="+escape(url);
    }else{
    }
    window.location.href=url;
    return false;
}

function logAuctionView(auctionId) {
	var logged = $.cookie('auctionViewLog');
	var auctionIds = [];
	if ( logged ) {
		auctionIds = logged.split(",");
	}
	var foundId = $.grep( auctionIds, function(n, i) {
		return auctionIds[i] == auctionId
	});
	if ( foundId.length == 0 ) {
		auctionIds.push(auctionId);
	}
	if ( auctionIds.length > 0 ) {
		if ( auctionIds.length > 30 ) {
			/* use the most recent 30 views */
			/* can't use shift() b/c IE doesn't support it */
			var start = auctionIds.length - 30;
			auctionIds = auctionIds.slice(start);
		}
		$.cookie('auctionViewLog',auctionIds.join(","), { expires: 365, path: "/" });
	}
}

$("#auctionViewLogTrigger").ready(function() {
	var auctionId = $("#auctionViewLogTrigger").attr("value");
	logAuctionView(auctionId);
});
