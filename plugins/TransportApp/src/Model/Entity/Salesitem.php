<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Salesitem Entity
 *
 * @property int $id
 * @property string|null $itemname
 * @property int|null $itemquantity
 * @property float|null $itemrate
 * @property string|null $total
 * @property int|null $sale_id
 * @property int|null $balance_id
 *
 * @property \TransportApp\Model\Entity\Sale $sale
 * @property \TransportApp\Model\Entity\Balance $balance
 */
class Salesitem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'itemname' => true,
        'itemquantity' => true,
        'itemrate' => true,
        'total' => true,
        'sale_id' => true,
        'balance_id' => true,
        'sale' => true,
        'balance' => true,
    ];
}
