<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\ExpensesTable;

/**
 * TransportApp\Model\Table\ExpensesTable Test Case
 */
class ExpensesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\ExpensesTable
     */
    protected $Expenses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Expenses',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Expenses') ? [] : ['className' => ExpensesTable::class];
        $this->Expenses = $this->getTableLocator()->get('Expenses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Expenses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
