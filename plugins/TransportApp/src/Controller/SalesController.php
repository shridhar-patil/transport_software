<?php

declare(strict_types=1);

namespace TransportApp\Controller;

use TransportApp\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use TransportApp\Model\Table\ExpensesTable as Expenses;
use Cake\Http\Client;

/**
 * Sales Controller
 *
 * @property \TransportApp\Model\Table\SalesTable $Sales
 * @method \TransportApp\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() {
        $http = new Client();



// Simple get with querystring
        $response = $http->get('https://newsapi.org/v2/top-headlines', ['country' => 'in', 'apiKey' => '728638b1cb0c478e9c8331b0c2a0331c']);
        $news = json_decode($response->getStringBody());
        if(file_exists(WWW_ROOT.'/news.json')){
            file_put_contents(WWW_ROOT.'/news.json', $news);
        }
        
        $newslist = $news->articles;
        $this->set(compact('newslist'));
    }

    /**
     * View method
     *
     * @param string|null $id Sale id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $sale = $this->Sales->get($id, [
            'contain' => ['Customers', 'Salesitems'],
        ]);
        //var_dump( $sale);
        $this->set(compact('sale'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $sale = $this->Sales->newEmptyEntity();
        if ($this->request->is('post')) {
            $sale = $this->Sales->patchEntity($sale, $this->request->getData());
            if ($this->Sales->save($sale)) {
                $this->Flash->success(__('The sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sale could not be saved. Please, try again.'));
        }
        $customers = $this->Sales->Customers->find('list', ['limit' => 200]);
        $this->set(compact('sale', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sale id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $sale = $this->Sales->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sale = $this->Sales->patchEntity($sale, $this->request->getData());
            if ($this->Sales->save($sale)) {
                $this->Flash->success(__('The sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sale could not be saved. Please, try again.'));
        }
        $customers = $this->Sales->Customers->find('list', ['limit' => 200]);
        $this->set(compact('sale', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sale id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $sale = $this->Sales->get($id);
        if ($this->Sales->delete($sale)) {
            $this->Flash->success(__('The sale has been deleted.'));
        } else {
            $this->Flash->error(__('The sale could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function insert2() {

        $sale = $this->Sales->newEmptyEntity();

        if ($this->request->is('ajax') || $this->request->is('post')) {

            $customer_name = $this->Sales->Customers->findById($this->request->getData('customer_id'))->first()->name ;
            // $_POST['name']=$customer_name;
            $sale = $this->Sales->patchEntity($sale, $this->request->getData());
            $sale->name =$customer_name;
            if ($this->Sales->save($sale)) {
                $this->Flash->success(__('The sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sale could not be saved. Please, try again.'));
        }
        $customers = $this->Sales->Customers->find('list')->select(['id', 'name'=> 'CONCAT(id,\'_Name:\',name, \'_Phone: \',phone )'])->toArray();
        $this->set(compact('sale', 'customers'));
    }

    public function insert3() {
        $sale = $this->Sales->newEmptyEntity();

        if ($this->request->is('ajax') || $this->request->is('post')) {

            $sale = $this->Sales->patchEntity($sale, $this->request->getData());
            if ($this->Sales->save($sale)) {
                $this->Flash->success(__('The sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sale could not be saved. Please, try again.'));
        }
        $customers = $this->Sales->Customers->find()->toArray();
        $this->set(compact('sale', 'customers'));
    }

    public function daily() {

        $connection = ConnectionManager::get('default');
        $date = date('Y-m-d');

        $sales = $connection->prepare(
                'SELECT * FROM sales WHERE date >= :date limit 300'
        );

// Bind multiple values
// Bind a single value
        $sales->bindValue('date', $date, 'datetime');
        $sales->execute();

// Read all rows.
        $sales_rows = $sales->fetchAll('assoc');
        $expenses = $connection->prepare(
                'SELECT * FROM expenses WHERE date >= :date limit 300'
        );
// Bind multiple values
// Bind a single value
        $expenses->bindValue('date', $date, 'datetime');
        $expenses->execute();
// Read all rows.
        $expenses_rows = $expenses->fetchAll('assoc');



        $sales_payments = $this->Sales->find()
                        ->select([
                            'total_salespayment_done' => 'SUM(sp.payment_amount)',
                            'id' => 'Sales.id',
                            'name' => 'Sales.name',
                            'date' => 'Sales.date',
                            'customer_id' => 'Sales.customer_id',
                            'itemname' => 'Sales.itemname',
                            'itemrate' => 'Sales.itemrate',
                            'itemquantity' => 'Sales.itemquantity',
                            'total' => 'Sales.total',
                            'payment_id' => 'sp.id',
                            'payment_amount' => 'sp.payment_amount'
                        ])
                        ->join([
                            'table' => 'sales_payments',
                            'alias' => 'sp',
                            'type' => 'LEFT',
                            'conditions' => 'sp.sales_id = Sales.id',
                        ])
                        ->where(['sp.date >= ' => date('Y-m-d')])
                        ->limit(300)->all()->toArray();
//        dd($sales_payments);
//        $this->set("sales_payments", $sales_payments);
        $this->set(compact('sales_rows', 'expenses_rows', 'sales_payments'));
    }

    public function help() {
        
    }

    public function displaylist() {
        $this->paginate = [
            'contain' => ['Customers'],
        ];
        $sales_rows = $this->paginate($this->Sales);

        $this->set(compact('sales_rows'));
    }

    public function makebill() {

        if ($this->request->is('post')) {
            $name = $this->request->getData('Salename');
            $startdate = $this->request->getData('startdate');
            $enddate = $this->request->getData('enddate');

            $query = $this->Sales->find()
                            ->select([
                                'total_salespayment_done' => 'SUM(sp.payment_amount)',
                                'id' => 'Sales.id',
                                'name' => 'Sales.name',
                                'date' => 'Sales.date',
                                'customer_id' => 'Sales.customer_id',
                                'itemname' => 'Sales.itemname',
                                'itemrate' => 'Sales.itemrate',
                                'itemquantity' => 'Sales.itemquantity',
                                'total' => 'Sales.total',
                                'payment_id' => 'sp.id'
                            ])
                            ->where(['Sales.date >= ' => $startdate])
                            ->where(['Sales.date <= ' => $enddate])
                            ->where(['Sales.name' => $name])
                            ->join([
                                'table' => 'sales_payments',
                                'alias' => 'sp',
                                'type' => 'LEFT',
                                'conditions' => 'sp.sales_id = Sales.id',
                            ])->all()->toArray();
            $this->set("Sale", $query);
        }
    }

    public function findday() {

        if ($this->request->is('post')) {

            $startdate = $this->request->getData('startdate');
            $enddate = $this->request->getData('enddate');

            $data = $this->Sales->find()
                            ->where(['Sales.date >=' => $startdate])
                            ->where(['Sales.date <=' => $enddate])
                            ->all()->toArray();
            $this->set('Sale', $data);
            $Expenses = new Expenses();
            $expenses_data = $Expenses->find()
                            ->where(['Expenses.date >=' => $startdate])
                            ->where(['Expenses.date <=' => $enddate])
                            ->all()->toArray();
            $this->set('Expense', $expenses_data);
        }
    }

    public function listitems2() {

        $data = $this->Sales->find()
                        ->where(['Sales.date >=' => date('Y-m-d')])
                        ->all()->toArray();
        $this->set('Sales', $data);
    }

    public function delete2($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $sale = $this->Sales->get($id);
        if ($this->Sales->delete($sale)) {
            $this->Flash->success(__('The sale has been deleted.'));
        } else {
            $this->Flash->error(__('The sale could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'listitems2', 'controller' => 'SalesController']);
    }

    public function invoices(){
        
    }

    public function oilsales(){
        
    }

}
