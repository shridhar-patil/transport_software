<style>
    .container{
        padding:5px;
    }
    .item1,.item3,.item6{
        border:2px;
        display:flex;
        flex-direction:row;
        flex-wrap:wrap;
    }
    .item2 > label > input{
        display:inline;
        width:80px;
    }
    .item > label ,
    .item  > .input > label ,
    .item > .input > label{
        width:150px;
        margin-left:20px;      
    } 
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        border: 1px solid #ddd;
    }
    #quality{
        width:100px;
        margin-right:0.1px;
        margin-right:0.1px;
    }
    #unit,#taga,#quantity,#rate,#amount{
        width:70px;
        margin-left:0.1px;
        margin-right:0.1px;
    }
</style>    
<h1> Gst Invoice जि  एस टी सेल्स  </h1>
<?php echo $this->Form->create($gst , array('id'=>'GstForm')); ?>
<div class="container">
    <div class="item1">
        <div class="item1.1">
               <div class="item"><?php echo $this->Form->label('Sale Account');echo $this->Form->select('sale_ac', $sets ,["class"=>'chosenselect']);?></div>
                
                <div class="item"><?php echo $this->Form->control('invoice_no',["label"=>'Invoice Number','class'=>'']); ?></div>

                <div class="item"><?php echo $this->Form->control('invoice_no_date',["label"=>'Invoice No Date']); ?></div>

                <div class="item"><?php echo $this->Form->control('party_name'); ?></div>

                <div class="item"><?php echo $this->Form->control('date'); ?></div>
        </div>
        <div class="item1.2">
        </div>
        <div class="item1.3">
                <div class="item"><?php echo $this->Form->control('order_no'); ?></div>

                <div class="item"><?php echo $this->Form->control('order_no_date',["label"=>'Order Date']); ?></div>

                <div class="item"><?php echo $this->Form->label('transport');echo $this->Form->select('transport', $sets3 , ["class"=>'chosenselect']);?></div>

                <div class="item"><?php echo $this->Form->label('station');echo $this->Form->select('station', $sets4 , ["class"=>'chosenselect'] );?></div>
               
                <div class="item"><?php echo $this->Form->control('lorry_no'); ?></div>

                <div class="item"><?php echo $this->Form->control('lorry_no_date',["label"=>'Lorry Date']); ?></div>
        </div>
    </div>

    <div style="overflow-x:auto;">
        <table >
            <tr>
                <td><?php echo $this->Form->label('from_ps'); echo $this->Form->select('from_ps', $sets1 ); ?></td>
                
                <td><?php echo $this->Form->label('to_ps'); echo $this->Form->select('to_ps', $sets1 ); ?></td>
               
                <td><?php echo $this->Form->control('quality'); ?></td>

                <td><?php echo $this->Form->label('hs_code'); echo $this->Form->select('hs_code', $sets2 ); ?></td>
            
                <td><?php echo $this->Form->control('unit'); ?></td>

                <td><?php echo $this->Form->control('taga'); ?></td>

                <td><?php echo $this->Form->control('quantity'); ?></td>            
           
                <td><?php echo $this->Form->control('rate'); ?></td>

                <td><?php echo $this->Form->control('amount'); ?></td>

                <td><button id="addRow" onclick="additem(event);">Add</button></td>

            </tr>
            </table>         
    </div>  
    <div class="item5">
        <table id="itemstable">
            <thead>
                <th>From Ps</th>
                <th>To Ps</th>
                <th>Quality</th>
                <th>HS Code</th>
                <th>Unit</th>
                <th>Taga</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </thead>
        </table>
    </div>
    <div class="item3">
            <div class="item3.1">
                <div class="item"><?php echo $this->Form->control('addition1'); ?></div>               
                    
                <div class="item"><?php echo $this->Form->control('addition2'); ?></div>               

                <div class="item"><?php echo $this->Form->control('addition3'); ?></div>               
            </div>

            <div class="item3.2">
                <div class="item"><?php echo $this->Form->control('addition_value1',["label"=>'Add Amount 1']); ?></div>

                <div class="item"><?php echo $this->Form->control('addition_value2',["label"=>'Add Amount 2']); ?></div>

                <div class="item"><?php echo $this->Form->control('addition_value3',["label"=>'Add Amount 3']); ?></div>
            </div>  <br>
        </div>    
        <div class="item6">             
            <div class="item3.3">
                <div class="item"><?php echo $this->Form->control('deduction1'); ?></div>               

                <div class="item"><?php echo $this->Form->control('deduction2'); ?></div>               

                <div class="item"><?php echo $this->Form->control('deduction3'); ?></div>    
            </div>

            <div class="item3.4">
                <div class="item"><?php echo $this->Form->control('deduction_value1',["label"=>'Deduction Amount 1']); ?></div>
                
                <div class="item"><?php echo $this->Form->control('deduction_value2',["label"=>'Deduction Amount 2']); ?></div>
                    
                <div class="item"><?php echo $this->Form->control('deduction_value3',["label"=>'Deduction Amount 3']); ?></div>
            </div>
        </div>
    </div>
      
    <div class="item4">
            <div class="item"><?php echo $this->Form->control('due_date'); ?></div>

            <div class="item"><?php echo $this->Form->control('gst_date'); ?></div>

            <div class="item"><?php echo $this->Form->submit(); ?></div> 
    </div> 
</div>
        <?php echo $this->Form->end();?>
<hr>
<div id="listitems" class="first span-10"></div>

<script>

 $(".chosenselect").chosen({disable_search_threshold: 10});
 function additem(e){
     e.preventDefault();    
 }
 $(document).ready(function() {

    var t = $('#itemstable').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching":false
    });     
        $('#addRow').on( 'click', function () {
        
        var from_ps= $("td:nth-child(1) > select:nth-child(2)").val();
        if(from_ps != '') {   
        var to_ps= $("td:nth-child(2) > select:nth-child(2)").val();
        var quality= $("#quality").val();
        var hs_code= $("td:nth-child(4) > select:nth-child(2)").val();
        var unit= $("#unit").val();
        var taga= $("#taga").val();
        var quantity= $("#quantity").val();
        var rate= $("#rate").val();
        var amount= $("#amount").val();     
            t.row.add( [        
                from_ps,to_ps ,quality,hs_code,unit,taga,quantity,rate,amount        
            ] ).draw( false );       
        }
    } );
} );
var table = $('#itemstable').DataTable();
$('#itemstable tbody').on( 'click', 'tr', function () {
	console.log( table.row( this ).data() );
} );

</script>