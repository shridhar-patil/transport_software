
<style>
    
  @media print {
  td,tr{
    line-height: 2px;
    border-style: 1px solid rgb(233, 225, 225);
    color: rgb(253, 249, 249);
    font-size: 10px;
    font-weight: lighter;}
  }
  .top_nav{
      position: absolute;
        top: -9999px;
         left: -9999px;
   }
  
  
</style>
<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $stocks
 */
?>

<div class="index content">
    
    <h3><?= __('Stocks दर पत्रक यादी') ?></h3>
    <div   >
        <table class= "table table-bordered table-hover" id="testTabl">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('itemname') ?></th>
                    <th><?= $this->Paginator->sort('itemrate') ?></th>
                 
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($stocks as $stock): ?>
                <tr>
                    <td><?= $this->Number->format($stock->id) ?></td>
                    
                   
                    
                    <td><?= h($stock->itemname) ?></td>
                     <td><?= h($stock->itemrate) ?></td>
                    
                    <td class="actions">
                    
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $stock->id]) ?>
                 
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#testTabl').dataTable();
} );
</script>



