<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gst Model
 *
 * @method \TransportApp\Model\Entity\Sm newEmptyEntity()
 * @method \TransportApp\Model\Entity\Sm newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Sm findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Sm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sm saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GstTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gst');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');
       
        $validator
            ->scalar('sale_ac')
            ->maxLength('sale_ac', 30)
            ->allowEmptyString('sale_ac');

        $validator
            ->scalar('invoice_no')
            ->maxLength('invoice_no', 30)
            ->allowEmptyString('invoice_no');

        $validator
            ->date('invoice_no_date')
            ->allowEmptyDate('invoice_no_date');

        $validator
            ->scalar('party_name')
            ->maxLength('party_name', 30)
            ->allowEmptyString('party_name');

        $validator
            ->date('date')
            ->allowEmptyDate('date');
                    
        $validator
            ->scalar('order_no')
            ->maxLength('order_no', 30)
            ->allowEmptyString('order_no');

        $validator
            ->date('order_no_date')
            ->allowEmptyDate('order_no_date');
        
        $validator
            ->scalar('transport')
            ->maxLength('transport', 30)
            ->allowEmptyString('transport');
        
        $validator
            ->scalar('station')
            ->maxLength('station', 30)
            ->allowEmptyString('station');

        $validator
            ->scalar('lorry_no')
            ->maxLength('lorry_no', 15)
            ->allowEmptyString('lorry_no');

        $validator
            ->date('lorry_no_date')
            ->allowEmptyDate('lorry_no_date');

        $validator
            ->scalar('from_ps')
            ->maxLength('from_ps', 30)
            ->allowEmptyString('from_ps');
            
        $validator
            ->scalar('to_ps')
            ->maxLength('to_ps', 30)
            ->allowEmptyString('to_ps');
        
        $validator
            ->scalar('quality')
            ->maxLength('quality', 30)
            ->allowEmptyString('quality');

        $validator
            ->scalar('hs_code')
            ->maxLength('hs_code', 15)
            ->allowEmptyString('hs_code');

        $validator
            ->integer('unit')
            ->allowEmptyString('unit');
       
        $validator
            ->scalar('taga')
            ->maxLength('taga', 30)
            ->allowEmptyString('taga');
            
        $validator
            ->scalar('quantity')
            ->maxLength('quantity', 30)
            ->allowEmptyString('quantity');
        
        $validator
            ->integer('rate')
            ->allowEmptyString('rate');

        $validator
            ->integer('amount')
            ->allowEmptyString('amount');
          
        $validator
            ->scalar('addition1')
            ->maxLength('addition1', 15)
            ->allowEmptyString('addition1');

        $validator
            ->scalar('addition_value1')
            ->maxLength('addition_value1', 30)
            ->allowEmptyString('addition_value1');

        $validator
            ->scalar('addition2')
            ->maxLength('addition2', 15)
            ->allowEmptyString('addition2');

        $validator
            ->scalar('addition_value2')
            ->maxLength('addition_value2', 30)
            ->allowEmptyString('addition_value2');

        $validator
            ->scalar('addition3')
            ->maxLength('addition3', 15)
            ->allowEmptyString('addition3');
        
        $validator
            ->scalar('addition_value3')
            ->maxLength('addition_value3', 30)
            ->allowEmptyString('addition_value3');
        
        $validator
            ->scalar('deduction1')
            ->maxLength('deduction1', 15)
            ->allowEmptyString('deduction1');

        $validator
            ->scalar('deduction_value1')
            ->maxLength('deduction_value1', 15)
            ->allowEmptyString('deduction_value1');

        $validator
            ->scalar('deduction2')
            ->maxLength('deduction2', 15)
            ->allowEmptyString('deduction2');
            
        $validator
            ->scalar('deduction_value2')
            ->maxLength('deduction_value2', 30)
            ->allowEmptyString('deduction_value2');
        
        $validator
            ->scalar('deduction3')
            ->maxLength('deduction3', 30)
            ->allowEmptyString('deduction3');

        $validator
            ->scalar('deduction_value3')
            ->maxLength('deduction_value3', 15)
            ->allowEmptyString('deduction_value3');

        $validator
            ->date('due_date')
            ->allowEmptyDate('due_date');

        $validator
            ->date('gst_date')
            ->allowEmptyDate('gst_date');
        
        return $validator;
    }
}