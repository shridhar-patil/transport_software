var STTAFFUNC_COMMERCE = {
	haveCustomData: false,
	imageSrc:'',
	details :'',
	perUnitPrice:'',
	perLotPrice:'',
	auctionType:'',
	closeTime:'',
	seller:'',
	imgDisplay:'',
	cw:function(obj, paramObj){
		var customData = STTAFFUNC_COMMERCE.getCustomData();
      		STTAFFUNC.cw(obj, {id:paramObj.id, link: paramObj.link, title: paramObj.title, custom:customData});
	},
	showHoverMap:function(obj,id,link,title){
		var customData = STTAFFUNC_COMMERCE.getCustomData();
		STTAFFUNC.showHoverMap(obj,id,link,title, customData);
	},
	getCustomData:function(){
		var imageEle = document.getElementById("productimage");
		if(imageEle && imageEle.innerHTML!="")
			STTAFFUNC_COMMERCE.imageSrc = imageEle.innerHTML;	
		
	
		STTAFFUNC_COMMERCE.perUnitPrice = document.getElementById("perUnitPrice").innerHTML;
		STTAFFUNC_COMMERCE.perLotPrice = document.getElementById("perLotPrice").innerHTML;
		STTAFFUNC_COMMERCE.closeTime = document.getElementById("closeTime").innerHTML;
		var customObj = {
			'perUnitPrice':STTAFFUNC_COMMERCE.perUnitPrice,
			'productimage':STTAFFUNC_COMMERCE.imageSrc,
			'closeTime':STTAFFUNC_COMMERCE.closeTime,
			'perLotPrice':STTAFFUNC_COMMERCE.perLotPrice,
			'minimumBid':document.getElementById("minimumBid").innerHTML,
			'totalbids':document.getElementById("totalbids").innerHTML,
			'bidhistory':document.getElementById("bidhistoryurl").innerHTML,
			'placebidimg':document.getElementById("placebidbutton").innerHTML,
			'bidlabeltext':document.getElementById("bidlabeltext").innerHTML
		}
		return customObj;
	}	
}
