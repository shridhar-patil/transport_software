<style>
    .container {
        display:flex;
        flex-direction:row;
        flex-wrap:wrap;
    }
</style>
<h1> Send Message</h1>
<?php echo $this->Form->create($sms, array('id'=>'SmsForm')); ?>
<div class="container">
    <div class="item2"><?php echo $this->Form->control('from_mobile',["label"=>'From']); ?></div>

    <div class="item3"><?php echo $this->Form->control('to_mobile',["label"=>'To']); ?></div>

    <div class="item4"><?php echo $this->Form->control('message'); ?></div>
    
    <div class="item5"><?php echo $this->Form->button(__('Submit'));  ?> </div>  
</div>
    <?php echo $this->Form->end();?>
<hr>
<div id="listitems" class="first span-10"></div>