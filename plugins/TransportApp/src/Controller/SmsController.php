<?php

declare(strict_types=1);

namespace TransportApp\Controller;

use TransportApp\Controller\AppController;
use TransportApp\Model\Entity\Email;
use TransportApp\Model\Table\EmailTable;
/**
 * Sms Controller
 *
 * @property \TransportApp\Model\Table\SMSTable $Sms
 * @method \TransportApp\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SmsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() {
        $sms = $this->paginate($this->Sms);

        $this->set(compact('sms'));
    }

    /**
     * View method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $sms = $this->Sms->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('sms'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $sms = $this->Sms->newEmptyEntity();
        if ($this->request->is('post')) {
            $sms = $this->Sms->patchEntity($sms, $this->request->getData());
            if ($this->Sms->save($sms)) {
                $this->Flash->success(__('The sms has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sms could not be saved. Please, try again.'));
        }
        $this->set(compact('sms'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $sms = $this->Sms->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sms = $this->Sms->patchEntity($sms, $this->request->getData());
            if ($this->Sms->save($sms)) {
                $this->Flash->success(__('The sms has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sms could not be saved. Please, try again.'));
        }
        $this->set(compact('sms'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $sms = $this->Sms->get($id);
        if ($this->Sms->delete($sms)) {
            $this->Flash->success(__('The sms has been deleted.'));
        } else {
            $this->Flash->error(__('The sms could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function sendsms(){
        $sms = $this->Sms->newEmptyEntity();
        if ($this->request->is('post')) {
            $sms = $this->Sms->patchEntity($sms, $this->request->getData());
            if ($this->Sms->save($sms)) {
                $this->Flash->success(__('The sms has been saved.'));

                return $this->redirect(['action' => 'sendsms']);
            }
            $this->Flash->error(__('The sms could not be saved. Please, try again.'));
        }
        $this->set(compact('sms'));    
    }
    public function sendemail(){
        
        $this->loadModel('TransportApp.Email');
        
         $email = $this->Email->newEmptyEntity();
          if ($this->request->is('post')) {
              $email = $this->Email->patchEntity($email, $this->request->getData());
              if ($this->Email->save($email)) {
                  $this->Flash->success(__('The email has been saved.'));
  
                  return $this->redirect(['action' => 'sendemail']);
              }
              $this->Flash->error(__('The email could not be saved. Please, try again.'));
          }
          $this->set(compact('email'));
      }     
    
}