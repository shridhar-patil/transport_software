<html>
<head>
<?php 
echo $this->Html->charset(); 
$rel="stylesheet";
echo $this->Html->css('blueprint/screen.css');
echo $this->Html->css('blueprint/src/typography.css');
echo $this->Html->css('blueprint/src/forms.css');
echo $this->Html->css('blueprint/src/grid.css');
echo $this->Html->script('jquery');
?>



</head>
<body padding="0px" margin="0px">

<div id="content" class="">
	<div class="container">
		<div class="span-24">
				<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
			<?php  echo $scripts_for_layout ;?>
	      	
		</div>
	</div>


</div>
</body>
</html>