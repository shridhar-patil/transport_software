<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SalesPayment $salesPayment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesPayment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesPayment->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Sales Payments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="salesPayments form content">
            <?= $this->Form->create($salesPayment) ?>
            <fieldset>
                <legend><?= __('Edit Sales Payment') ?></legend>
                <?php
                    echo $this->Form->control('sales_id', ['options' => $sales]);
                    echo $this->Form->control('payment_amount');
                    echo $this->Form->control('date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
