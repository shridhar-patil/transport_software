<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesPayments Model
 *
 * @property \TransportApp\Model\Table\SalesTable&\Cake\ORM\Association\BelongsTo $Sales
 *
 * @method \TransportApp\Model\Entity\SalesPayment newEmptyEntity()
 * @method \TransportApp\Model\Entity\SalesPayment newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\SalesPayment[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SalesPaymentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sales_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sales_id',
            'joinType' => 'INNER',
            'className' => 'TransportApp.Sales',
        ]);
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('payment_amount')
            ->requirePresence('payment_amount', 'create')
            ->notEmptyString('payment_amount');

        $validator
            ->dateTime('date')
            ->notEmptyDateTime('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['sales_id'], 'Sales'), ['errorField' => 'sales_id']);

        return $rules;
    }
}
