<h1>दैनिक खर्च यादी</h1>
<div class="expenses index content">
    <h3><?= __('Expenses') ?></h3>
    <div class="table-responsive">
        <table class= "table table-bordered table-hover" id="testTabl">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('expense') ?></th>
                    <th><?= $this->Paginator->sort('cost') ?></th>
                 
                    <th><?= $this->Paginator->sort('date') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($expenses as $expense): ?>
                <tr>
                    <td><?= $this->Number->format($expense->id) ?></td>
                    <td><?= h($expense->expense) ?></td>
                    <td><?= h($expense->cost) ?></td>
                    <td><?= h($expense->date) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.table').dataTable();
} );
</script>