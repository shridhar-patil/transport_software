<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\EmailTable;

/**
 * TransportApp\Model\Table\EmailTable Test Case
 */
class EmailTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\EmailTable
     */
    protected $Email;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Email',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Email') ? [] : ['className' => EmailTable::class];
        $this->Email = $this->getTableLocator()->get('Email', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Email);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
