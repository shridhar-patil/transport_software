<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\SalesPaymentsTable;

/**
 * TransportApp\Model\Table\SalesPaymentsTable Test Case
 */
class SalesPaymentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\SalesPaymentsTable
     */
    protected $SalesPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.SalesPayments',
        'plugin.TransportApp.Sales',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SalesPayments') ? [] : ['className' => SalesPaymentsTable::class];
        $this->SalesPayments = $this->getTableLocator()->get('SalesPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SalesPayments);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
