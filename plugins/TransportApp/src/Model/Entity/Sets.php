<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $area
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string $vehicle
 *
 * @property \TransportApp\Model\Entity\Sale[] $sales
 */
class Sets extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'type' => true
    ];
}
