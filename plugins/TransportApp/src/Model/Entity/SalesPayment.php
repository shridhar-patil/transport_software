<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesPayment Entity
 *
 * @property int $id
 * @property int $sales_id
 * @property int $payment_amount
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \TransportApp\Model\Entity\Sale $sale
 */
class SalesPayment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sales_id' => true,
        'payment_amount' => true,
        'date' => true,
        'sale' => true,
    ];
}
