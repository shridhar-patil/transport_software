<div align="center"><br>
    <h1>WELCOME</h1> <br>
    <a class="btn btn-danger" href="<?php echo $this->Url->build(array('action' => 'add', 'controller' => 'customers')); ?>" ><button>नवीन खातेदार </button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'insert2', 'controller' => 'sales')); ?>" ><button>दैनिक एन्ट्री</button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'insert', 'controller' => 'sales')); ?>" ><button>सगळे तपशील</button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'add', 'controller' => 'expenses')); ?>" ><button>दैनिक खर्च</button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'findday', 'controller' => 'sales')); ?>" ><button>तारखेनुसार दैनिक तपशील</button></a>

    <a href="<?php echo $this->Url->build(array('action' => 'makebill', 'controller' => 'sales')); ?>"><button>वयक्तिक  खातीदार</button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'add', 'controller' => 'stocks')); ?>"><button>दर पत्रक</button></a>
    <a href="<?php echo $this->Url->build(array('action' => 'displaylist', 'controller' => 'stocks')); ?>"><button>दर पत्रक लिस्ट</button></a>



    <a href="<?php echo $this->Url->build(array('action' => 'help', 'controller' => 'sales')); ?>" ><button>मदत</button></a><br>

</div>



<div class="row">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-caret-square-o-right"></i>
            </div>
            <div class="count">179</div>

            <h3>Sarva Khatedar </h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-comments-o"></i>
            </div>
            <div class="count">179</div>

            <h3>Dainik Tapshil </h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i>
            </div>
            <div class="count">179</div>

            <h3>Dainik Kharch Yadi</h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-check-square-o"></i>
            </div>
            <div class="count">179</div>

            <h3>Sagle Tapshil </h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-check-square-o"></i>
            </div>
            <div class="count">179</div>

            <h3>Tarkhenusar Dainik Tapshil  </h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-check-square-o"></i>
            </div>
            <div class="count">179</div>

            <h3>Dar Patrak Yadi  </h3>
            <p>Lorem ipsum psdea itgum rixt.</p>
        </div>
    </div>
</div>




<div class="col-md-6 col-sm-6  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Instarr News Network</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <ul class="list-unstyled timeline">


                <?php foreach ($newslist as $news): ?>
                    <li>
                        <div class="block">
                            <div class="tags">
                                <a href="" class="tag">
                                    <span><?= h($news->author) ?></span>
                                </a>
                            </div>
                            <div class="block_content">
                                <h2 class="title">
                                    <a><?= h($news->title) ?></a>
                                </h2>
                                <div class="byline">
                                    <span><?= h($news->publishedAt) ?></span> by <a><?= h($news->author) ?></a>
                                </div>
                                <p class="excerpt"> <?= h($news->description) ?> <a href="<?= h($news->url) ?>" target="blank">Read&nbsp;More</a>
                                </p>
                            </div>
                        </div>
                    </li>

                <?php endforeach; ?>
                

            </ul>

        </div>
    </div>
</div>