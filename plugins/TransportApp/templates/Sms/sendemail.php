<style>
    .container {
        display:flex;
        flex-direction:row;
        flex-wrap:wrap;
    }
</style>
<h1> Send Email</h1>
<?php echo $this->Form->create($email , array('id'=>'EmailForm')); ?>
<div class="container">
    <div class="item2"><?php echo $this->Form->control('from_user',["label"=>'From']); ?></div>

    <div class="item3"><?php echo $this->Form->control('to_user',["label"=>'To']); ?></div>

    <div class="item4"><?php echo $this->Form->control('message'); ?></div>

    <div class="item5"><?php echo $this->Form->submit(); ?> </div>  
</div>
        <?php echo $this->Form->end();?>
<hr>
<div id="listitems" class="first span-10"></div>