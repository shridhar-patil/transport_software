<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\SalesitemsTable;

/**
 * TransportApp\Model\Table\SalesitemsTable Test Case
 */
class SalesitemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\SalesitemsTable
     */
    protected $Salesitems;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Salesitems',
        'plugin.TransportApp.Sales',
        'plugin.TransportApp.Balances',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Salesitems') ? [] : ['className' => SalesitemsTable::class];
        $this->Salesitems = $this->getTableLocator()->get('Salesitems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Salesitems);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
