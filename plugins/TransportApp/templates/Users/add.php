
<!-- app/View/Users/add.ctp -->

<div class=""><a href="/serveait/ekac/users/login/">Login</a></div>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('role', array(
            'options' => array('administrators' => 'administrators', 'managers' => 'managers','users'=>'users')
        ));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>