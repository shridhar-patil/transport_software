<?php

declare(strict_types=1);

namespace TransportApp;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * Plugin for TransportApp
 */
class Plugin extends BasePlugin {

    /**
     * Load all the plugin configuration and bootstrap logic.
     *
     * The host application is provided as an argument. This allows you to load
     * additional plugin dependencies, or attach events.
     *
     * @param \Cake\Core\PluginApplicationInterface $app The host application
     * @return void
     */
    public function bootstrap(PluginApplicationInterface $app): void {
        
    }

    /**
     * Add routes for the plugin.
     *
     * If your plugin has many routes and you would like to isolate them into a separate file,
     * you can create `$plugin/config/routes.php` and delete this method.
     *
     * @param \Cake\Routing\RouteBuilder $routes The route builder to update.
     * @return void
     */
    public function routes(RouteBuilder $routes): void {
        $routes->plugin(
                'TransportApp',
                ['path' => '/transport-app'],
                function (RouteBuilder $builder) {
            // Add custom routes here

            $builder->connect('/', ['controller' => 'Sales', 'action' => 'index']);
            $builder->fallbacks();
        }
        );

//        Router::plugin('TransportApp', function (RouteBuilder $routes) {
//            // Routes connected here are prefixed with '/debug_kit' and
//            // have the plugin route element set to 'DebugKit'.
//
//            $routes->connect('/transport-app', ['controller' => 'Sales', 'action' => 'index']);
//        });


        parent::routes($routes);
    }

    /**
     * Add middleware for the plugin.
     *
     * @param \Cake\Http\MiddlewareQueue $middleware The middleware queue to update.
     * @return \Cake\Http\MiddlewareQueue
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue {
        // Add your middlewares here

        return $middlewareQueue;
    }

}
