<!DOCTYPE html>
<html>
<head>
<?php echo $this->Html->charset(); ?>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="text/xml; charset=utf-8" http-equiv="Content-Type">
<title>Distribution Software v2</title>
<?php
$rel="stylesheet";

//echo $this->Html->css('serveait.css');
echo $this->Html->css('blueprint/screen.css');
echo $this->Html->css('blueprint/src/reset.css');
//echo $this->Html->css('blueprint/print.css');
echo $this->Html->css('jquery-ui.css');
echo $this->Html->css('blueprint/src/typography.css');
echo $this->Html->script('jquery');
echo $this->Html->script('jquery-ui');
echo $this->Html->script('spahql');
echo $this->Html->css('jquery-ui.css');
echo $this->Html->css('jquery.dataTables.min.css');
echo $this->Html->css('dataTables.tableTools.css');
//echo $this->Html->css('bootstrap.css');
echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->script('dataTables.tableTools');
//echo $this->Html->script('jquery-ui-1.8.4.custom.min');

echo $this->Html->css('blueprint/src/forms.css');
//      echo $this->Html->script('dhtmlxtree/dhtmlxcommon.js');
//     echo $this->Html->script('dhtmlxtree/dhtmlxtree.js');
//      echo $this->Html->css('dhtmlxtree/dhtmlxtree.css');

echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<script>
  $(function() {
    $( ".menu" ).menu();
  });
  </script>
<style>
.ui-menu {
	width: 150px;
}
</style>
</head>

<style>
body {
	margin: 0px;
	padding: 0px; //
	//background-color: #DED5D5;
	//	background-color: gray;
	
	//background-image: url('.././img/nature.jpg');
//	background-opacity:80%;
}

.menutable {
	align: left;
	width: 50%;
}

table tr td {
	
}

#header2 {
	background:black;
}
</style>

<body>

	<div id="header2" >
		<table class="menutable">
			<tr>
				<td>
					<ul id="menu" class="menu">

						<li tabindex="1">Masters
							<ul>
								<li>Customers
								<ul>
										<li><a href="/serve/unee/customers/add">Add New</a></li>
										<li><a href="/serve/unee/customers/displaylist">List</a></li>
								</ul>
								</li>
								<li>Sales
									<ul>
										<li><a href="/serve/unee/sales/add3">Add New</a></li>
										<li><a href="/serve/unee/sales/add">Add</a></li>
										<li><a href="/serve/unee/salesitems/displaylist">Edit Sales
												items</a></li>
										<li><a href="/serve/unee/sales/displaylist">List</a></li>
									</ul>
								</li>
								<li>Stocks
									<ul>
										<li><a href="/serve/unee/stocks/add">Add</a></li>
										<li><a href="/serve/unee/stocks/displaylist">List</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul></td>
				<td>
					<ul id="menu" class="menu">

						<li>Billing
							<ul>
								<li><a href="/serve/unee/sales/report">All Bills</a></li>
								<li><a href="/serve/unee/sales/distbill">Distributor Bills</a></li>
								<li><a href="/serve/unee/sales/retailbill">Retailer Bills </a></li>
								<li><a href="/serve/unee/sales/directbill">Direct Bills </a></li>
							</ul>
						</li>
					</ul></td>
				<td>
					<ul id="menu" class="menu">
						<li>Sales Reports
							<ul>

								<li><a href="/serve/unee/sales/salesdate/">Sales Report Datewise</a>
								</li>
								<li><a href="/serve/unee/sales/salesstock/">Sales Report
										Stockwise</a></li>

								<li><a href="/serve/unee/sales/salesdist">Sales Report
										DistributorWise</a></li>
								
								</li>
								<li><a href="/serve/unee/sales/invoicesummary">Invoice Summary</a>	</li>
								<li><a href="/serve/unee/sales/profitloss">Profit Loss Summary</a>	</li>
								<li><a href="/serve/unee/sales/customersummary">Customer Summary</a>	</li>
							</ul>
						</li>

					</ul></td>

				<td>
					<ul id="menu" class="menu">

						<li>Stocks Reports
							<ul>

								<li><a href="/serve/unee/stocks/stockremain">Stock Remaining</a>
								</li>
								<li><a href="/serve/unee/stocks/stockdate">Stock Datewise</a></li>


							</ul>
						</li>

					</ul></td>


				<td>
					<ul id="menu" class="menu">

						<li>Deletes
							<ul>
							 <li><a href="/serve/unee/stocks/delete/">Stock Delete</a></li>
							 <li><a href="/serve/unee/sales/delete/">Sales Delete</a></li>
						
							 <li><a href="/serve/unee/customers/delete/">Customer Delete</a></li>
							 
							</ul>
						</li>

					</ul></td>
			</tr>
		</table>














		<?php //echo $this->Html->image('saldistributors2.jpg',array('width'=>"60px",'height'=>"50px"));
		?>

		<script>

			
			function printing(){
				
				$("a").contents().unwrap();
						window.print();

				}
			</script>
	</div>


	</div>








	<div id="con" class="">
		<div class="container">
			<div id="content" class="span-24">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
			<?php  echo $scripts_for_layout ;?>

			<?php echo $this->Js->writeBuffer(array('cache'=>'TRUE')); ?>

			</div>
		</div>

	</div>

	<div id="footer">

	<?php //echo $this->element('sql_dump'); ?>








		<p align="center">Copyright &#169; 2014 - Serve Software, Ichalkaranji
			Support-91 9545627599 | info@serveait.in</p>
	</div>


	<script type="text/javascript">
//works in chrome ,firefox enter key as tab code, based on firefox
$('input').keydown( function(e) {
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
    }
});
</script>

	<script>
//script to put inputs under labels for alignment purpose
$("label1").after("<br>");
</script>
	<script>
//export to excel pdf csv script--works perfectly, flash player needed for buttons to work based datatable.tabletools css and js which plugin of jquery.datatable.js 


$(document).ready( function () {
    var table = $('#testTable').dataTable();
    var tableTools = new $.fn.dataTable.TableTools( table, {
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );
      
    $( tableTools.fnContainer() ).insertBefore('#testTable');
} );

   
</script>

</body>
</html>
