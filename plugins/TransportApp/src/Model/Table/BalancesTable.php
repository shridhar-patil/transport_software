<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Balances Model
 *
 * @property \TransportApp\Model\Table\BillsTable&\Cake\ORM\Association\BelongsTo $Bills
 * @property \TransportApp\Model\Table\SalesitemsTable&\Cake\ORM\Association\HasMany $Salesitems
 *
 * @method \TransportApp\Model\Entity\Balance newEmptyEntity()
 * @method \TransportApp\Model\Entity\Balance newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Balance[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Balance get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Balance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Balance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Balance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Balance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Balance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Balance[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Balance[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Balance[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Balance[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class BalancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('balances');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Bills', [
            'foreignKey' => 'bill_id',
            'joinType' => 'INNER',
            'className' => 'TransportApp.Bills',
        ]);
        $this->hasMany('Salesitems', [
            'foreignKey' => 'balance_id',
            'className' => 'TransportApp.Salesitems',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('payed')
            ->requirePresence('payed', 'create')
            ->notEmptyString('payed');

        $validator
            ->nonNegativeInteger('pending')
            ->requirePresence('pending', 'create')
            ->notEmptyString('pending');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['bill_id'], 'Bills'), ['errorField' => 'bill_id']);

        return $rules;
    }
}
