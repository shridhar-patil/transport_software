function _uGC(l,n,s) {
   // used to obtain a value form a string of key=value pairs
   if (!l || l=="" || !n || n=="" || !s || s=="") return "-";
   var i,i2,i3,c="-";
   i=l.indexOf(n);
   i3=n.indexOf("=")+1;
   if (i > -1) {
      i2=l.indexOf(s,i); if (i2 < 0) { i2=l.length; }
      c=l.substring((i+i3),i2);
   }
   return c;
}

function checkFirst(){
   // check if this is a first time visitor and if so, set flag
   newVisitor = 0;
   var myCookie = " " + document.cookie + ";";
   var searchName = "__utma=";
   var startOfCookie = myCookie.indexOf(searchName)
   if (startOfCookie == -1) {   // i.e. first time visitor
      newVisitor = 1;
   }
}

function grabReferrer(){
   // if first visit, grab utmz cookie values and put in utmv
   if (newVisitor) {
      var z = _uGC(document.cookie, "__utmz=", ";");
      urchin_source = _uGC(z,"utmcsr=", "|");
      urchin_medium = _uGC(z,"utmcmd=", "|");
      urchin_term = _uGC(z,"utmctr=", "|");
      urchin_content = _uGC(z,"utmcct=", "|");
      urchin_campaign = _uGC(z,"utmccn=", "|");
      var gclid = _uGC(z,"utmgclid=","|");
      if (gclid != "-")  {
		urchin_source.value = "google";
		urchin_medium.value = "cpc";
		urchin_term.value = "cpc";
		urchin_content.value = "cpc";
		urchin_campaign.value = "cpc";
      }
      if (urchin_term != "-"){
         urchin_term = urchin_term.replace(/%27/g, "'");      //decode
         urchin_term = urchin_term.replace(/%22/g, "'");      //decode
         //urchin_term = urchin_term.replace(/\+/g, " ");       //decode
         urchin_term = urchin_term.replace(/%20/g, "_");      //decode
		_gaq.push(['_setCustomVar',
			2,
			'First_Touch',
			urchin_term + "(" + urchin_source + "_" + urchin_medium,
			1
		]);
      }
   }
}(function(d){d.formatCurrency={};d.formatCurrency.regions=[];d.formatCurrency.regions[""]={symbol:"$",positiveFormat:"%s%n",negativeFormat:"(%s%n)",decimalSymbol:".",digitGroupSymbol:",",groupDigits:true};
d.fn.formatCurrency=function(e,f){if(arguments.length==1&&typeof e!=="string"){f=e;e=false}var g={name:"formatCurrency",colorize:false,region:"",global:true,roundToDecimalPlace:2,eventOnDecimalsEntered:false};
g=d.extend(g,d.formatCurrency.regions[""]);f=d.extend(g,f);if(f.region.length>0){f=d.extend(f,b(f.region))}f.regex=a(f);return this.each(function(){$this=d(this);
var o="0";o=$this[$this.is("input, select, textarea")?"val":"html"]();if(o.search("\\(")>=0){o="-"+o}if(o===""||(o==="-"&&f.roundToDecimalPlace===-1)){return
}if(isNaN(o)){o=o.replace(f.regex,"");if(o===""||(o==="-"&&f.roundToDecimalPlace===-1)){return}if(f.decimalSymbol!="."){o=o.replace(f.decimalSymbol,".")
}if(isNaN(o)){o="0"}}var m=String(o).split(".");var r=(o==Math.abs(o));var l=(m.length>1);var k=(l?m[1].toString():"0");var j=k;o=Math.abs(m[0]);
o=isNaN(o)?0:o;if(f.roundToDecimalPlace>=0){k=parseFloat("1."+k);k=k.toFixed(f.roundToDecimalPlace);if(k.substring(0,1)=="2"){o=Number(o)+1}k=k.substring(2)
}o=String(o);if(f.groupDigits){for(var n=0;n<Math.floor((o.length-(1+n))/3);n++){o=o.substring(0,o.length-(4*n+3))+f.digitGroupSymbol+o.substring(o.length-(4*n+3))
}}if((l&&f.roundToDecimalPlace==-1)||f.roundToDecimalPlace>0){o+=f.decimalSymbol+k}var q=r?f.positiveFormat:f.negativeFormat;var h=q.replace(/%s/g,f.symbol);
h=h.replace(/%n/g,o);var p=d([]);if(!e){p=$this}else{p=d(e)}p[p.is("input, select, textarea")?"val":"html"](h);if(l&&f.eventOnDecimalsEntered&&j.length>f.roundToDecimalPlace){p.trigger("decimalsEntered",j)
}if(f.colorize){p.css("color",r?"black":"red")}})};d.fn.toNumber=function(e){var f=d.extend({name:"toNumber",region:"",global:true},d.formatCurrency.regions[""]);
e=jQuery.extend(f,e);if(e.region.length>0){e=d.extend(e,b(e.region))}e.regex=a(e);return this.each(function(){var g=d(this).is("input, select, textarea")?"val":"html";
d(this)[g](d(this)[g]().replace("(","(-").replace(e.regex,""))})};d.fn.asNumber=function(f){var g=d.extend({name:"asNumber",region:"",parse:true,parseType:"Float",global:true},d.formatCurrency.regions[""]);
f=jQuery.extend(g,f);if(f.region.length>0){f=d.extend(f,b(f.region))}f.regex=a(f);f.parseType=c(f.parseType);var h=d(this).is("input, select, textarea")?"val":"html";
var e=d(this)[h]();e=e?e:"";e=e.replace("(","(-");e=e.replace(f.regex,"");if(!f.parse){return e}if(e.length==0){e="0"}if(f.decimalSymbol!="."){e=e.replace(f.decimalSymbol,".")
}return window["parse"+f.parseType](e)};function b(g){var f=d.formatCurrency.regions[g];if(f){return f}else{if(/(\w+)-(\w+)/g.test(g)){var e=g.replace(/(\w+)-(\w+)/g,"$1");
return d.formatCurrency.regions[e]}}return null}function c(e){switch(e.toLowerCase()){case"int":return"Int";case"float":return"Float";default:throw"invalid parseType"
}}function a(e){if(e.symbol===""){return new RegExp("[^\\d"+e.decimalSymbol+"-]","g")}else{var f=e.symbol.replace("$","\\$").replace(".","\\.");
return new RegExp(f+"|[^\\d"+e.decimalSymbol+"-]","g")}}})(jQuery);/**
 * jQuery lightBox plugin
 * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
 * and adapted to me for use like a plugin from jQuery.
 * @name jquery-lightbox-0.5.js
 * @author Leandro Vieira Pinho - http://leandrovieira.com
 * @version 0.5
 * @date April 11, 2008
 * @category jQuery plugin
 * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
 * @license CC Attribution-No Derivative Works 2.5 Brazil - http://creativecommons.org/licenses/by-nd/2.5/br/deed.en_US
 * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
 */

// Offering a Custom Alias suport - More info: http://docs.jquery.com/Plugins/Authoring#Custom_Alias
(function($) {
	/**
	 * $ is an alias to jQuery object
	 *
	 */
	$.fn.lightBox = function(settings) {
		// Settings to configure the jQuery lightBox plugin how you like
		settings = jQuery.extend({
			// Configuration related to overlay
			overlayBgColor: 		'#000',		// (string) Background color to overlay; inform a hexadecimal value like: #RRGGBB. Where RR, GG, and BB are the hexadecimal values for the red, green, and blue values of the color.
			overlayOpacity:			0.8,		// (integer) Opacity value to overlay; inform: 0.X. Where X are number from 0 to 9
			// Configuration related to navigation
			fixedNavigation:		false,		// (boolean) Boolean that informs if the navigation (next and prev button) will be fixed or not in the interface.
			// Configuration related to images
			imageLoading:			'/shared/bu/images/lightbox/lightbox-ico-loading.gif',		// (string) Path and the name of the loading icon
			imageBtnPrev:			'/shared/bu/images/lightbox/lightbox-btn-prev.gif',			// (string) Path and the name of the prev button image
			imageBtnNext:			'/shared/bu/images/lightbox/lightbox-btn-next.gif',			// (string) Path and the name of the next button image
			imageBtnClose:			'/shared/bu/images/lightbox/lightbox-btn-close.gif',		// (string) Path and the name of the close btn
			imageBlank:				'/shared/bu/images/lightbox/lightbox-blank.gif',			// (string) Path and the name of a blank image (one pixel)
			// Configuration related to container image box
			containerBorderSize:	10,			// (integer) If you adjust the padding in the CSS for the container, #lightbox-container-image-box, you will need to update this value
			containerResizeSpeed:	400,		// (integer) Specify the resize duration of container image. These number are miliseconds. 400 is default.
			// Configuration related to texts in caption. For example: Image 2 of 8. You can alter either "Image" and "of" texts.
			txtImage:				'Image',	// (string) Specify text "Image"
			txtOf:					'of',		// (string) Specify text "of"
			// Configuration related to keyboard navigation
			keyToClose:				'c',		// (string) (c = close) Letter to close the jQuery lightBox interface. Beyond this letter, the letter X and the SCAPE key is used to.
			keyToPrev:				'p',		// (string) (p = previous) Letter to show the previous image
			keyToNext:				'n',		// (string) (n = next) Letter to show the next image.
			// Don�t alter these variables in any way
			imageArray:				[],
			activeImage:			0
		},settings);
		// Caching the jQuery object with all elements matched
		var jQueryMatchedObj = this; // This, in this context, refer to jQuery object
        /**
		 * Initializing the plugin calling the start function
		 *
		 * @return boolean false
		 */
		function _initialize() {
			_start(this,jQueryMatchedObj); // This, in this context, refer to object (link) which the user have clicked
			return false; // Avoid the browser following the link
		}
		/**
		 * Start the jQuery lightBox plugin
		 *
		 * @param object objClicked The object (link) whick the user have clicked
		 * @param object jQueryMatchedObj The jQuery object with all elements matched
		 */
		function _start(objClicked,jQueryMatchedObj) {
			// Hime some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
			$('embed, object, select').css({ 'visibility' : 'hidden' });
			// Call the function to create the markup structure; style some elements; assign events in some elements.
			_set_interface();
			// Unset total images in imageArray
			settings.imageArray.length = 0;
			// Unset image active information
			settings.activeImage = 0;
			// We have an image set? Or just an image? Let�s see it.
			if ( jQueryMatchedObj.length == 1 ) {
				settings.imageArray.push(new Array(objClicked.getAttribute('href'),objClicked.getAttribute('title')));
			} else {
				// Add an Array (as many as we have), with href and title atributes, inside the Array that storage the images references
				for ( var i = 0; i < jQueryMatchedObj.length; i++ ) {
					settings.imageArray.push(new Array(jQueryMatchedObj[i].getAttribute('href'),jQueryMatchedObj[i].getAttribute('title')));
				}
			}
			while ( settings.imageArray[settings.activeImage][0] != objClicked.getAttribute('href') ) {
				settings.activeImage++;
			}
			// Call the function that prepares image exibition
			_set_image_to_view();
		}
		/**
		 * Create the jQuery lightBox plugin interface
		 *
		 * The HTML markup will be like that:
			<div id="jquery-overlay"></div>
			<div id="jquery-lightbox">
				<div id="lightbox-container-image-box">
					<div id="lightbox-container-image">
						<img src="../fotos/XX.jpg" id="lightbox-image">
						<div id="lightbox-nav">
							<a href="#" id="lightbox-nav-btnPrev"></a>
							<a href="#" id="lightbox-nav-btnNext"></a>
						</div>
						<div id="lightbox-loading">
							<a href="#" id="lightbox-loading-link">
								<img src="../images/lightbox-ico-loading.gif">
							</a>
						</div>
					</div>
				</div>
				<div id="lightbox-container-image-data-box">
					<div id="lightbox-container-image-data">
						<div id="lightbox-image-details">
							<span id="lightbox-image-details-caption"></span>
							<span id="lightbox-image-details-currentNumber"></span>
						</div>
						<div id="lightbox-secNav">
							<a href="#" id="lightbox-secNav-btnClose">
								<img src="../images/lightbox-btn-close.gif">
							</a>
						</div>
					</div>
				</div>
			</div>
		 *
		 */
		function _set_interface() {
			// Apply the HTML markup into body tag
			$('body').append('<div id="jquery-overlay"></div><div id="jquery-lightbox"><div id="lightbox-container-image-box"><div id="lightbox-container-image"><img id="lightbox-image"><div style="" id="lightbox-nav"><a href="#" id="lightbox-nav-btnPrev"></a><a href="#" id="lightbox-nav-btnNext"></a></div><div id="lightbox-loading"><a href="#" id="lightbox-loading-link"><img src="' + settings.imageLoading + '"></a></div></div></div><div id="lightbox-container-image-data-box"><div id="lightbox-container-image-data"><div id="lightbox-image-details"><span id="lightbox-image-details-caption"></span><span id="lightbox-image-details-currentNumber"></span></div><div id="lightbox-secNav"><a href="#" id="lightbox-secNav-btnClose"><img src="' + settings.imageBtnClose + '"></a></div></div></div></div>');
			// Get page sizes
			var arrPageSizes = ___getPageSize();
			// Style overlay and show it
			$('#jquery-overlay').css({
				backgroundColor:	settings.overlayBgColor,
				opacity:			settings.overlayOpacity,
				width:				arrPageSizes[0],
				height:				arrPageSizes[1]
			}).fadeIn();
			// Get page scroll
			var arrPageScroll = ___getPageScroll();
			// Calculate top and left offset for the jquery-lightbox div object and show it
			$('#jquery-lightbox').css({
				top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
				left:	arrPageScroll[0]
			}).show();
			// Assigning click events in elements to close overlay
			$('#jquery-overlay,#jquery-lightbox').click(function() {
				_finish();
			});
			// Assign the _finish function to lightbox-loading-link and lightbox-secNav-btnClose objects
			$('#lightbox-loading-link,#lightbox-secNav-btnClose').click(function() {
				_finish();
				return false;
			});
			// If window was resized, calculate the new overlay dimensions
			$(window).resize(function() {
				// Get page sizes
				var arrPageSizes = ___getPageSize();
				// Style overlay and show it
				$('#jquery-overlay').css({
					width:		arrPageSizes[0],
					height:		arrPageSizes[1]
				});
				// Get page scroll
				var arrPageScroll = ___getPageScroll();
				// Calculate top and left offset for the jquery-lightbox div object and show it
				$('#jquery-lightbox').css({
					top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
					left:	arrPageScroll[0]
				});
			});
		}
		/**
		 * Prepares image exibition; doing a image�s preloader to calculate it�s size
		 *
		 */
		function _set_image_to_view() { // show the loading
			// Show the loading
			$('#lightbox-loading').show();
			if ( settings.fixedNavigation ) {
				$('#lightbox-image,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
			} else {
				// Hide some elements
				$('#lightbox-image,#lightbox-nav,#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
			}
			// Image preload process
			var objImagePreloader = new Image();
			objImagePreloader.onload = function() {
                $('#lightbox-image').attr('src',settings.imageArray[settings.activeImage][0]);
				// Perfomance an effect in the image container resizing it
				_resize_container_image_box(objImagePreloader.width,objImagePreloader.height);
				//	clear onLoad, IE behaves irratically with animated gifs otherwise
				objImagePreloader.onload=function(){};
			};
			objImagePreloader.src = settings.imageArray[settings.activeImage][0];
		};
		/**
		 * Perfomance an effect in the image container resizing it
		 *
		 * @param integer intImageWidth The image�s width that will be showed
		 * @param integer intImageHeight The image�s height that will be showed
		 */
		function _resize_container_image_box(intImageWidth,intImageHeight) {
			// Get current width and height
			var intCurrentWidth = $('#lightbox-container-image-box').width();
			var intCurrentHeight = $('#lightbox-container-image-box').height();
			// Get the width and height of the selected image plus the padding
			var intWidth = (intImageWidth + (settings.containerBorderSize * 2)); // Plus the image�s width and the left and right padding value
			var intHeight = (intImageHeight + (settings.containerBorderSize * 2)); // Plus the image�s height and the left and right padding value
			// Diferences
			var intDiffW = intCurrentWidth - intWidth;
			var intDiffH = intCurrentHeight - intHeight;
			// Perfomance the effect
			$('#lightbox-container-image-data-box').css({width:intWidth-20});
			$('#lightbox-container-image-box').animate({ width: intWidth, height: intHeight },settings.containerResizeSpeed,function() { _show_image(); });
			if ( ( intDiffW == 0 ) && ( intDiffH == 0 ) ) {
				if ( $.browser.msie ) {
					___pause(250);
				} else {
					___pause(100);
				}
			}
			//$('#lightbox-container-image-data-box').css({ width: intImageWidth });
			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ height: intImageHeight + (settings.containerBorderSize * 2) });
		};
		/**
		 * Show the prepared image
		 *
		 */
		function _show_image() {
			$('#lightbox-loading').hide();
			$('#lightbox-image').fadeIn(function() {
				_show_image_data();
				_set_navigation();
			});
			_preload_neighbor_images();
		};
		/**
		 * Show the image information
		 *
		 */
		function _show_image_data() {
			$('#lightbox-container-image-data-box').slideDown('fast');
			$('#lightbox-image-details-caption').hide();
			if ( settings.imageArray[settings.activeImage][1] ) {
				$('#lightbox-image-details-caption').html(settings.imageArray[settings.activeImage][1]).show();
			}
			// If we have a image set, display 'Image X of X'
			if ( settings.imageArray.length > 1 ) {
				$('#lightbox-image-details-currentNumber').html(settings.txtImage + ' ' + ( settings.activeImage + 1 ) + ' ' + settings.txtOf + ' ' + settings.imageArray.length).show();
			}
		}
		/**
		 * Display the button navigations
		 *
		 */
		function _set_navigation() {
			$('#lightbox-nav').show();

			// Instead to define this configuration in CSS file, we define here. And it�s need to IE. Just.
			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });

			// Show the prev button, if not the first image in set
			if ( settings.activeImage != 0 ) {
				if ( settings.fixedNavigation ) {
					$('#lightbox-nav-btnPrev').css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' })
						.unbind()
						.bind('click',function() {
							settings.activeImage = settings.activeImage - 1;
							_set_image_to_view();
							return false;
						});
				} else {
					// Show the images button for Next buttons
					$('#lightbox-nav-btnPrev').unbind().hover(function() {
						$(this).css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' });
					},function() {
						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
					}).show().bind('click',function() {
						settings.activeImage = settings.activeImage - 1;
						_set_image_to_view();
						return false;
					});
				}
			} else {
				$('#lightbox-nav-btnPrev').unbind('click');
			}

			// Show the next button, if not the last image in set
			if ( settings.activeImage != ( settings.imageArray.length -1 ) ) {
				if ( settings.fixedNavigation ) {
					$('#lightbox-nav-btnNext').css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' })
						.unbind()
						.bind('click',function() {
							settings.activeImage = settings.activeImage + 1;
							_set_image_to_view();
							return false;
						});
				} else {
					// Show the images button for Next buttons
					$('#lightbox-nav-btnNext').unbind().hover(function() {
						$(this).css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' });
					},function() {
						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
					}).show().bind('click',function() {
						settings.activeImage = settings.activeImage + 1;
						_set_image_to_view();
						return false;
					});
				}
			} else {
				$('#lightbox-nav-btnNext').unbind('click');
			}
			// Enable keyboard navigation
			_enable_keyboard_navigation();
		}
		/**
		 * Enable a support to keyboard navigation
		 *
		 */
		function _enable_keyboard_navigation() {
			$(document).keydown(function(objEvent) {
				_keyboard_action(objEvent);
			});
		}
		/**
		 * Disable the support to keyboard navigation
		 *
		 */
		function _disable_keyboard_navigation() {
			$(document).unbind();
		}
		/**
		 * Perform the keyboard actions
		 *
		 */
		function _keyboard_action(objEvent) {
			// To ie
			if ( objEvent == null ) {
				keycode = event.keyCode;
				escapeKey = 27;
			// To Mozilla
			} else {
				keycode = objEvent.keyCode;
				escapeKey = objEvent.DOM_VK_ESCAPE;
			}
			// Get the key in lower case form
			key = String.fromCharCode(keycode).toLowerCase();
			// Verify the keys to close the ligthBox
			if ( ( key == settings.keyToClose ) || ( key == 'x' ) || ( keycode == escapeKey ) ) {
				_finish();
			}
			// Verify the key to show the previous image
			if ( ( key == settings.keyToPrev ) || ( keycode == 37 ) ) {
				// If we�re not showing the first image, call the previous
				if ( settings.activeImage != 0 ) {
					settings.activeImage = settings.activeImage - 1;
					_set_image_to_view();
					_disable_keyboard_navigation();
				}
			}
			// Verify the key to show the next image
			if ( ( key == settings.keyToNext ) || ( keycode == 39 ) ) {
				// If we�re not showing the last image, call the next
				if ( settings.activeImage != ( settings.imageArray.length - 1 ) ) {
					settings.activeImage = settings.activeImage + 1;
					_set_image_to_view();
					_disable_keyboard_navigation();
				}
			}
		}
		/**
		 * Preload prev and next images being showed
		 *
		 */
		function _preload_neighbor_images() {
			if ( (settings.imageArray.length -1) > settings.activeImage ) {
				objNext = new Image();
				objNext.src = settings.imageArray[settings.activeImage + 1][0];
			}
			if ( settings.activeImage > 0 ) {
				objPrev = new Image();
				objPrev.src = settings.imageArray[settings.activeImage -1][0];
			}
		}
		/**
		 * Remove jQuery lightBox plugin HTML markup
		 *
		 */
		function _finish() {
			$('#jquery-lightbox').remove();
			$('#jquery-overlay').fadeOut(function() { $('#jquery-overlay').remove(); });
			// Show some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
			$('embed, object, select').css({ 'visibility' : 'visible' });
		}
		/**
		 / THIRD FUNCTION
		 * getPageSize() by quirksmode.com
		 *
		 * @return Array Return an array with page width, height and window width, height
		 */
		function ___getPageSize() {
			var xScroll, yScroll;
			if (window.innerHeight && window.scrollMaxY) {
				xScroll = window.innerWidth + window.scrollMaxX;
				yScroll = window.innerHeight + window.scrollMaxY;
			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
				xScroll = document.body.scrollWidth;
				yScroll = document.body.scrollHeight;
			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
				xScroll = document.body.offsetWidth;
				yScroll = document.body.offsetHeight;
			}
			var windowWidth, windowHeight;
			if (self.innerHeight) {	// all except Explorer
				if(document.documentElement.clientWidth){
					windowWidth = document.documentElement.clientWidth;
				} else {
					windowWidth = self.innerWidth;
				}
				windowHeight = self.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (document.body) { // other Explorers
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}
			// for small pages with total height less then height of the viewport
			if(yScroll < windowHeight){
				pageHeight = windowHeight;
			} else {
				pageHeight = yScroll;
			}
			// for small pages with total width less then width of the viewport
			if(xScroll < windowWidth){
				pageWidth = xScroll;
			} else {
				pageWidth = windowWidth;
			}
			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
			return arrayPageSize;
		};
		/**
		 / THIRD FUNCTION
		 * getPageScroll() by quirksmode.com
		 *
		 * @return Array Return an array with x,y page scroll values.
		 */
		function ___getPageScroll() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;
			}
			arrayPageScroll = new Array(xScroll,yScroll);
			return arrayPageScroll;
		};
		 /**
		  * Stop the code execution from a escified time in milisecond
		  *
		  */
		 function ___pause(ms) {
			var date = new Date();
			curDate = null;
			do { var curDate = new Date(); }
			while ( curDate - date < ms);
		 };
		// Return the jQuery object for chaining. The unbind method is used to avoid click conflict when the plugin is called more than once
		return this.unbind('click').click(_initialize);
	};
})(jQuery); // Call and execute the function immediately passing the jQuery object
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};(function(){


var rsplit = function(string, regex) {
	var result = regex.exec(string),retArr = new Array(), first_idx, last_idx, first_bit;
	while (result != null)
	{
		first_idx = result.index; last_idx = regex.lastIndex;
		if ((first_idx) != 0)
		{
			first_bit = string.substring(0,first_idx);
			retArr.push(string.substring(0,first_idx));
			string = string.slice(first_idx);
		}
		retArr.push(result[0]);
		string = string.slice(result[0].length);
		result = regex.exec(string);
	}
	if (! string == '')
	{
		retArr.push(string);
	}
	return retArr;
},
chop =  function(string){
    return string.substr(0, string.length - 1);
},
extend = function(d, s){
    for(var n in s){
        if(s.hasOwnProperty(n))  d[n] = s[n]
    }
}


EJS = function( options ){
	options = typeof options == "string" ? {view: options} : options
    this.set_options(options);
	if(options.precompiled){
		this.template = {};
		this.template.process = options.precompiled;
		EJS.update(this.name, this);
		return;
	}
    if(options.element)
	{
		if(typeof options.element == 'string'){
			var name = options.element
			options.element = document.getElementById(  options.element )
			if(options.element == null) throw name+'does not exist!'
		}
		if(options.element.value){
			this.text = options.element.value
		}else{
			this.text = options.element.innerHTML
		}
		this.name = options.element.id
		this.type = '['
	}else if(options.url){
        options.url = EJS.endExt(options.url, this.extMatch);
		this.name = this.name ? this.name : options.url;
        var url = options.url
        //options.view = options.absolute_url || options.view || options.;
		var template = EJS.get(this.name /*url*/, this.cache);
		if (template) return template;
	    if (template == EJS.INVALID_PATH) return null;
        try{
            this.text = EJS.request( url+(this.cache ? '' : '?'+Math.random() ));
        }catch(e){}

		if(this.text == null){
            throw( {type: 'EJS', message: 'There is no template at '+url}  );
		}
		//this.name = url;
	}
	var template = new EJS.Compiler(this.text, this.type);

	template.compile(options, this.name);


	EJS.update(this.name, this);
	this.template = template;
};
/* @Prototype*/
EJS.prototype = {
	/**
	 * Renders an object with extra view helpers attached to the view.
	 * @param {Object} object data to be rendered
	 * @param {Object} extra_helpers an object with additonal view helpers
	 * @return {String} returns the result of the string
	 */
    render : function(object, extra_helpers){
        object = object || {};
        this._extra_helpers = extra_helpers;
		var v = new EJS.Helpers(object, extra_helpers || {});
		return this.template.process.call(object, object,v);
	},
    update : function(element, options){
        if(typeof element == 'string'){
			element = document.getElementById(element)
		}
		if(options == null){
			_template = this;
			return function(object){
				EJS.prototype.update.call(_template, element, object)
			}
		}
		if(typeof options == 'string'){
			params = {}
			params.url = options
			_template = this;
			params.onComplete = function(request){
				var object = eval( request.responseText )
				EJS.prototype.update.call(_template, element, object)
			}
			EJS.ajax_request(params)
		}else
		{
			element.innerHTML = this.render(options)
		}
    },
	out : function(){
		return this.template.out;
	},
    /**
     * Sets options on this view to be rendered with.
     * @param {Object} options
     */
	set_options : function(options){
        this.type = options.type || EJS.type;
		this.cache = options.cache != null ? options.cache : EJS.cache;
		this.text = options.text || null;
		this.name =  options.name || null;
		this.ext = options.ext || EJS.ext;
		this.extMatch = new RegExp(this.ext.replace(/\./, '\.'));
	}
};
EJS.endExt = function(path, match){
	if(!path) return null;
	match.lastIndex = 0
	return path+ (match.test(path) ? '' : this.ext )
}




/* @Static*/
EJS.Scanner = function(source, left, right) {

    extend(this,
        {left_delimiter: 	left +'%',
         right_delimiter: 	'%'+right,
         double_left: 		left+'%%',
         double_right:  	'%%'+right,
         left_equal: 		left+'%=',
         left_comment: 	left+'%#'})

	this.SplitRegexp = left=='[' ? /(\[%%)|(%%\])|(\[%=)|(\[%#)|(\[%)|(%\]\n)|(%\])|(\n)/ : new RegExp('('+this.double_left+')|(%%'+this.double_right+')|('+this.left_equal+')|('+this.left_comment+')|('+this.left_delimiter+')|('+this.right_delimiter+'\n)|('+this.right_delimiter+')|(\n)') ;

	this.source = source;
	this.stag = null;
	this.lines = 0;
};

EJS.Scanner.to_text = function(input){
	if(input == null || input === undefined)
        return '';
    if(input instanceof Date)
		return input.toDateString();
	if(input.toString)
        return input.toString();
	return '';
};

EJS.Scanner.prototype = {
  scan: function(block) {
     scanline = this.scanline;
	 regex = this.SplitRegexp;
	 if (! this.source == '')
	 {
	 	 var source_split = rsplit(this.source, /\n/);
	 	 for(var i=0; i<source_split.length; i++) {
		 	 var item = source_split[i];
			 this.scanline(item, regex, block);
		 }
	 }
  },
  scanline: function(line, regex, block) {
	 this.lines++;
	 var line_split = rsplit(line, regex);
 	 for(var i=0; i<line_split.length; i++) {
	   var token = line_split[i];
       if (token != null) {
		   	try{
	         	block(token, this);
		 	}catch(e){
				throw {type: 'EJS.Scanner', line: this.lines};
			}
       }
	 }
  }
};


EJS.Buffer = function(pre_cmd, post_cmd) {
	this.line = new Array();
	this.script = "";
	this.pre_cmd = pre_cmd;
	this.post_cmd = post_cmd;
	for (var i=0; i<this.pre_cmd.length; i++)
	{
		this.push(pre_cmd[i]);
	}
};
EJS.Buffer.prototype = {

  push: function(cmd) {
	this.line.push(cmd);
  },

  cr: function() {
	this.script = this.script + this.line.join('; ');
	this.line = new Array();
	this.script = this.script + "\n";
  },

  close: function() {
	if (this.line.length > 0)
	{
		for (var i=0; i<this.post_cmd.length; i++){
			this.push(pre_cmd[i]);
		}
		this.script = this.script + this.line.join('; ');
		line = null;
	}
  }

};


EJS.Compiler = function(source, left) {
    this.pre_cmd = ['var ___ViewO = [];'];
	this.post_cmd = new Array();
	this.source = ' ';
	if (source != null)
	{
		if (typeof source == 'string')
		{
		    source = source.replace(/\r\n/g, "\n");
            source = source.replace(/\r/g,   "\n");
			this.source = source;
		}else if (source.innerHTML){
			this.source = source.innerHTML;
		}
		if (typeof this.source != 'string'){
			this.source = "";
		}
	}
	left = left || '<';
	var right = '>';
	switch(left) {
		case '[':
			right = ']';
			break;
		case '<':
			break;
		default:
			throw left+' is not a supported deliminator';
			break;
	}
	this.scanner = new EJS.Scanner(this.source, left, right);
	this.out = '';
};
EJS.Compiler.prototype = {
  compile: function(options, name) {
  	options = options || {};
	this.out = '';
	var put_cmd = "___ViewO.push(";
	var insert_cmd = put_cmd;
	var buff = new EJS.Buffer(this.pre_cmd, this.post_cmd);
	var content = '';
	var clean = function(content)
	{
	    content = content.replace(/\\/g, '\\\\');
        content = content.replace(/\n/g, '\\n');
        content = content.replace(/"/g,  '\\"');
        return content;
	};
	this.scanner.scan(function(token, scanner) {
		if (scanner.stag == null)
		{
			switch(token) {
				case '\n':
					content = content + "\n";
					buff.push(put_cmd + '"' + clean(content) + '");');
					buff.cr();
					content = '';
					break;
				case scanner.left_delimiter:
				case scanner.left_equal:
				case scanner.left_comment:
					scanner.stag = token;
					if (content.length > 0)
					{
						buff.push(put_cmd + '"' + clean(content) + '")');
					}
					content = '';
					break;
				case scanner.double_left:
					content = content + scanner.left_delimiter;
					break;
				default:
					content = content + token;
					break;
			}
		}
		else {
			switch(token) {
				case scanner.right_delimiter:
					switch(scanner.stag) {
						case scanner.left_delimiter:
							if (content[content.length - 1] == '\n')
							{
								content = chop(content);
								buff.push(content);
								buff.cr();
							}
							else {
								buff.push(content);
							}
							break;
						case scanner.left_equal:
							buff.push(insert_cmd + "(EJS.Scanner.to_text(" + content + ")))");
							break;
					}
					scanner.stag = null;
					content = '';
					break;
				case scanner.double_right:
					content = content + scanner.right_delimiter;
					break;
				default:
					content = content + token;
					break;
			}
		}
	});
	if (content.length > 0)
	{
		// Chould be content.dump in Ruby
		buff.push(put_cmd + '"' + clean(content) + '")');
	}
	buff.close();
	this.out = buff.script + ";";
	var to_be_evaled = '/*'+name+'*/this.process = function(_CONTEXT,_VIEW) { try { with(_VIEW) { with (_CONTEXT) {'+this.out+" return ___ViewO.join('');}}}catch(e){e.lineNumber=null;throw e;}};";

	try{
		eval(to_be_evaled);
	}catch(e){
		if(typeof JSLINT != 'undefined'){
			JSLINT(this.out);
			for(var i = 0; i < JSLINT.errors.length; i++){
				var error = JSLINT.errors[i];
				if(error.reason != "Unnecessary semicolon."){
					error.line++;
					var e = new Error();
					e.lineNumber = error.line;
					e.message = error.reason;
					if(options.view)
						e.fileName = options.view;
					throw e;
				}
			}
		}else{
			throw e;
		}
	}
  }
};


//type, cache, folder
/**
 * Sets default options for all views
 * @param {Object} options Set view with the following options
 * <table class="options">
				<tbody><tr><th>Option</th><th>Default</th><th>Description</th></tr>
				<tr>
					<td>type</td>
					<td>'<'</td>
					<td>type of magic tags.  Options are '&lt;' or '['
					</td>
				</tr>
				<tr>
					<td>cache</td>
					<td>true in production mode, false in other modes</td>
					<td>true to cache template.
					</td>
				</tr>
	</tbody></table>
 *
 */
EJS.config = function(options){
	EJS.cache = options.cache != null ? options.cache : EJS.cache;
	EJS.type = options.type != null ? options.type : EJS.type;
	EJS.ext = options.ext != null ? options.ext : EJS.ext;

	var templates_directory = EJS.templates_directory || {}; //nice and private container
	EJS.templates_directory = templates_directory;
	EJS.get = function(path, cache){
		if(cache == false) return null;
		if(templates_directory[path]) return templates_directory[path];
  		return null;
	};

	EJS.update = function(path, template) {
		if(path == null) return;
		templates_directory[path] = template ;
	};

	EJS.INVALID_PATH =  -1;
};
EJS.config( {cache: true, type: '<', ext: '.js' } );



/**
 * @constructor
 * By adding functions to EJS.Helpers.prototype, those functions will be available in the
 * views.
 * @init Creates a view helper.  This function is called internally.  You should never call it.
 * @param {Object} data The data passed to the view.  Helpers have access to it through this._data
 */
EJS.Helpers = function(data, extras){
	this._data = data;
    this._extras = extras;
    extend(this, extras );
};
/* @prototype*/
EJS.Helpers.prototype = {
    /**
     * Renders a new view.  If data is passed in, uses that to render the view.
     * @param {Object} options standard options passed to a new view.
     * @param {optional:Object} data
     * @return {String}
     */
	view: function(options, data, helpers){
        if(!helpers) helpers = this._extras
		if(!data) data = this._data;
		return new EJS(options).render(data, helpers);
	},
    /**
     * For a given value, tries to create a human representation.
     * @param {Object} input the value being converted.
     * @param {Object} null_text what text should be present if input == null or undefined, defaults to ''
     * @return {String}
     */
	to_text: function(input, null_text) {
	    if(input == null || input === undefined) return null_text || '';
	    if(input instanceof Date) return input.toDateString();
		if(input.toString) return input.toString().replace(/\n/g, '<br />').replace(/''/g, "'");
		return '';
	}
};
    EJS.newRequest = function(){
	   var factories = [function() { return new ActiveXObject("Msxml2.XMLHTTP"); },function() { return new XMLHttpRequest(); },function() { return new ActiveXObject("Microsoft.XMLHTTP"); }];
	   for(var i = 0; i < factories.length; i++) {
	        try {
	            var request = factories[i]();
	            if (request != null)  return request;
	        }
	        catch(e) { continue;}
	   }
	}

	EJS.request = function(path){
	   var request = new EJS.newRequest()
	   request.open("GET", path, false);

	   try{request.send(null);}
	   catch(e){return null;}

	   if ( request.status == 404 || request.status == 2 ||(request.status == 0 && request.responseText == '') ) return null;

	   return request.responseText
	}
	EJS.ajax_request = function(params){
		params.method = ( params.method ? params.method : 'GET')

		var request = new EJS.newRequest();
		request.onreadystatechange = function(){
			if(request.readyState == 4){
				if(request.status == 200){
					params.onComplete(request)
				}else
				{
					params.onComplete(request)
				}
			}
		}
		request.open(params.method, params.url)
		request.send(null)
	}


})();String.prototype.escapeHTML = function(){
    var result = "";
    for(var i = 0; i < this.length; i++){
        if(this.charAt(i) == "&"
              && this.length-i-1 >= 4
              && this.substr(i, 4) != "&amp;"){
            result = result + "&amp;";
        } else if(this.charAt(i)== "<"){
            result = result + "&lt;";
        } else if(this.charAt(i)== ">"){
            result = result + "&gt;";
        }  else if(this.charAt(i)== "$"){
            result = result + "&#36;";
        } else {
            result = result + this.charAt(i);
        }
    }
    return result;
};

function escapeHTML(str) {
    var div = document.createElement('div');
    var text = document.createTextNode(str);
    div.appendChild(text);
    return div.innerHTML;
}

function unescapeHTML(str) {
    var div = document.createElement('div');
    div.innerHTML = str.replace(/<\/?[^>]+>/gi, '');
    return div.childNodes[0] ? div.childNodes[0].nodeValue : '';
}
$.fn.autoEllipsis = function(text, options) {
    if (!options) options = {};
    var elements = $(this);
    var lines = options.lines || 1;
    var truncationChr = options.trucationChar || "&#x2026;";
    var showTitle = options.showTitle || true;
    $.each(elements, function() {
        var element = $(this);
        var width = options.width || element.attr("offsetWidth") - element.getPadding("right") - element.getPadding("left");
        var eleHeight = options.height || element.attr("offsetHeight");
        var originalText = text || element.html();
        originalText = unescapeHTML(originalText);
        element.html("");
        var mySpan = document.createElement("span");
        mySpan = $(mySpan).addClass("autowrapSpan");
        element.append(mySpan);
        mySpan.html('<span>' + escapeHTML(originalText) + '</span>');
        if (mySpan.attr("offsetWidth") > width || (eleHeight > 0 && mySpan.attr("offsetHeight") > eleHeight)) {
            var displayText = originalText;
            mySpan.html('');
            for (var x = 0; (lines == 0 || x < lines); x++) {
                if (x > 0) {
                    mySpan.append("<br/>");
                }
                var newspan = document.createElement("span");
                newspan = $(newspan);
                mySpan.append(newspan);
                var i = 1;
                var thisLine = displayText;
                newspan.html(escapeHTML(thisLine));
                i = displayText.length * width / newspan.attr("offsetWidth");
                thisLine = displayText.substr(0, i);
                newspan.html(escapeHTML(thisLine));
                if (newspan.attr("offsetWidth") > width) {
                    while (newspan.attr("offsetWidth") > width && i > 0) {
                        thisLine = displayText.substr(0, i);
                        i--;
                        newspan.html(escapeHTML(thisLine));
                    }
                } else {
                while (newspan.attr("offsetWidth") <= width && i <= displayText.length) {
                    thisLine = displayText.substr(0, i);
                    i++;
                    newspan.html(escapeHTML(thisLine));
                }
            }
            if (newspan.attr("offsetWidth") > width) {
                thisLine = thisLine.substr(0, thisLine.length - 1);
                newspan.html(escapeHTML(thisLine));
            }
            displayText = displayText.substr(thisLine.length, displayText.length);
            if (displayText.length == 0 || thisLine.length == 0) {
                break;
            }
        }
        if (truncationChr != null && displayText != '') {
            var beforeTruncation = thisLine;
            newspan.html(escapeHTML(thisLine) + truncationChr);
            while (newspan.attr("offsetWidth") > width) {
                if (thisLine.length - 1 < 0) {
                    newspan.html(escapeHTML(beforeTruncation));
                    break;
                }
                thisLine = thisLine.substr(0, thisLine.length - 1);
                newspan.html(escapeHTML(thisLine) + truncationChr);
            }
        }
    } else {
    element.innerHTML = originalText;
}
if (showTitle) {
    element.attr("title",originalText);
}
});
};

$.fn.autoEllipsisByWord = function(text, options) {
    if(typeof text == "object"){
        options = text;
        text = '';
    }
    if (!options) options = {};
    var elements = $(this);
    var lines = options.lines || 1;
    var truncationChr = options.trucationChar || "&#x2026;";
    var showTitle = options.showTitle || true;
    $.each(elements, function() {
        var element = $(this);
        var width = options.width || element.width() - element.getPadding("right") - element.getPadding("left");
        var eleHeight = options.height || element.attr("offsetHeight");
        var originalText = text || element.html();
        originalText = unescapeHTML(originalText);
        element.html("");
        var mySpan = $('<span class="autowrapSpan"></span>');
        element.append(mySpan);
        mySpan.html('<span>' + originalText.escapeHTML() + '</span>');
        if (mySpan.width() > width || (eleHeight > 0 && mySpan.height() > eleHeight)) {
            var displayText = originalText;
            mySpan.html('');
            for (var x = 0; (lines == 0 || x < lines); x++) {
                if (x > 0) {
                    mySpan.append("<br/>");
                }
                var newspan = $("<span/>");
                mySpan.append(newspan);
                var i = 1;
                var thisLine = displayText;
                newspan.html(thisLine.escapeHTML());
                var tempStr = '';
                if (newspan.width() > width) {
	                i = displayText.length * width / newspan.width();
					thisLine = displayText.substr(0, i);
					thisLine = thisLine.substr(0,thisLine.lastIndexOf(' '));
					i = thisLine.length;
					newspan.html(thisLine.escapeHTML());
                    while (newspan.width() > width && i > 0) {
                        tempStr = displayText.substr(0, i);
                        var strEnd = tempStr.lastIndexOf(' ') > tempStr.lastIndexOf('-') ? tempStr.lastIndexOf(' ') : tempStr.lastIndexOf('-');
                        thisLine = tempStr.substr(0,strEnd);
                        i = thisLine.length;
                        newspan.html(thisLine.escapeHTML());
                    }
                } else {
                    while (newspan.width() <= width && i <= displayText.length) {
                        tempStr = displayText.substr(thisLine.length+1, displayText.length);
	                    var index = tempStr.indexOf(' ') != -1 ? tempStr.indexOf(' ') : tempStr.length;
	                    thisLine += " "+tempStr.substr(0, index);
                        i = thisLine.length;
                        newspan.html(thisLine.escapeHTML());
                    }
                }
                if (newspan.width() > width) {
                    thisLine = thisLine.substr(0, thisLine.lastIndexOf(' '));
                    newspan.html(thisLine.escapeHTML());
                }
                displayText = displayText.substr(thisLine.length+1, displayText.length);
                if (displayText.length == 0 || thisLine.length == 0) {
                    break;
                }
            }
            if (truncationChr != null && displayText != '') {
                var beforeTruncation = thisLine;
                newspan.html(thisLine.escapeHTML() + truncationChr);
                while (newspan.width() > width) {
                    if (thisLine.length - 1 < 0) {
                        newspan.html(beforeTruncation.escapeHTML());
                        break;
                    }
                    thisLine = thisLine.substr(0, thisLine.lastIndexOf(' '));
                    newspan.html(thisLine.escapeHTML() + truncationChr);
                }
            }
        } else {
            element.innerHTML = originalText;
        }
        if (showTitle) {
            element.attr("title", originalText);
        }
    });
};

$.fn.getPadding = function(side) {
    var paddingStr = $(this).css("padding-" + side);
    if (paddingStr.indexOf("px") != -1) {
        paddingStr = paddingStr.substr(0, paddingStr.length - 2);
        return parseInt(paddingStr);
    }
    return 0;
};

function customSelectFunction() {
    $('select.customselect').css('visibility','hidden').each(function() {
        var currentObject = $(this);
        var cID = currentObject.attr('id') != "" ? currentObject.attr('id') : '_cust_'+ $('div.customselect').length;
        var cW = currentObject.width() || currentObject.css('width');
        var cPL = currentObject.css('padding-left');
        var cPR = currentObject.css('padding-right');
        var cML = currentObject.css('margin-left');
        var cMR = currentObject.css('margin-right');        
        var cFloat = currentObject.css('float');
        var isMul = currentObject.attr('multiple');        
        var currentOptions = currentObject.find("option");
        var parentNode = currentObject.parent();
        var cWNoPx = cW.toString().replace(/px/,"");
        var cOffWid = parseInt(cWNoPx)+parseInt(cPL.split("px")[0])+parseInt(cPR.split("px")[0]);
        var bGP = (cOffWid - 18) + "px 1px";
        var currentText = "Select ";
        var cName = currentObject.attr("name");
        var cSName = currentObject.attr("sName") ? currentObject.attr("sName"): cName;
        var cPName = currentObject.attr('pName') || cName;
        if(isMul) { 
            currentText+= "up to "+ currentOptions.length + " " + cPName;
        }
        else { 
            currentText += "One "+ cSName;
        }
        currentObject.remove();
        parentNode.append("<div class='customselect'><div class='customselect-label'></div><div class='customselect-container'></div></div>");
        var container = $(parentNode).find('div.customselect');
        container.attr('name',cName).attr('pName',cPName).attr('sName',cSName);
        var lContainer = $(container.find('div.customselect-label'));
        lContainer.css({
            'background-position':bGP,
            'width':cOffWid
        }).hover(function(){
            $(this).addClass('customeHover');
        },function(){
            $(this).removeClass('customeHover');
        }).autoEllipsis(currentText,{'width':(cOffWid - 20)});
        if(cID != "") container.attr('id',cID);
        container.css({
            'width':cW,
            'padding-left':cPL,
            'padding-right':cPR,
            'margin-left':cML,
            'margin-right':cMR,
            'float':cFloat
        });
        if(!isMul){
            container.append("<input type='hidden' id='hidden_"+cID+"' name='"+cName+"' value=''>"); //hidden variable for single select to send as params in form submit
        }

        var mContainer = $(container.find('div.customselect-container'));
        var dropDownList = $('<div></div>');
        dropDownList.addClass('menu');
        var widthCalculator = $('<span></span>');
        var maxContainerWidth = 0;
        widthCalculator.attr("id","widthCalculator");
        $('body').append(widthCalculator);
        for (var x =0 ; x < currentOptions.length; x++) {
            var listOption = $('<div></div>');
            dropDownList.append(listOption);
            if($(currentOptions[x]).attr("selected")){ //set default value for hidden variables.
                $("#hidden_"+cID).val($(currentOptions[x]).val());
            }
            listOption.attr("id","option"+x).attr("index",x).attr("name", $(currentOptions[x]).val()).addClass('item');
            var inHTML = "";
            if(isMul){
                // for condition & auction type list box the param name sent condition_USED (REFUR etc..) & type_REG (FIX etc..)
                // hence the below condition
                if(cID=="condition" || cID=="type")
                {
                    inHTML = "<input class='inputClass' type='checkbox' name='"+cName+"_"+$(currentOptions[x]).val()+"' value='"+$(currentOptions[x]).val()+"' id='"+cID+"option"+x+"'><span class='spanClass'><label class='labelClass'>"+$(currentOptions[x]).html()+"</label></span>";
                }
                else 
                {
                    inHTML = "<input class='inputClass' type='checkbox' name='"+cName+"' value='"+$(currentOptions[x]).val()+"' id='"+cID+"option"+x+"'><span class='spanClass'><label class='labelClass'>"+$(currentOptions[x]).html()+"</label></span>";
                }
            } else {
                inHTML = "<span class='spanClass'><label class='labelClass'>"+$(currentOptions[x]).html()+"</label></span>";
            }
            widthCalculator.html(inHTML);
            listOption.html(inHTML);
            listOption.css({
                'background-color':'white'
            }).hover(function(){
                $(this).css({
                    'background-color':'#adc3e0'
                });
            },function(){
                $(this).css({
                    'background-color':'white'
                });
            });
            if(isMul){
                $(listOption.find('label')).attr('for',listOption.find('input:checkbox').attr('id'));
                $(listOption).find('input:checkbox').bind('click',{
                    con:container
                },function(event){
                    var con = event.data.con;
                    var sO = con.find('input:checkbox:checked').length;
                    if(sO == 0 ) {
                        con.find('div.customselect-label').autoEllipsis("Select up to " + con.find('div.item').length + " "+ con.attr('pName'),{
                            'width':con.attr('offsetWidth') - 20
                        });
                    } else if(sO == 1) {
                        con.find('div.customselect-label').html("1 "+con.attr('sName')+" selected");
                    } else {
                        con.find('div.customselect-label').html(sO+" "+con.attr('pName')+" selected").autoEllipsis(sO+" "+con.attr('pName')+" selected",{
                            'width':con.attr('offsetWidth') - 30
                        });
                    }
                });
            }
            else {
                $(listOption).bind('click', function(){
                    var cS = $(this);
                    var cP = cS.parents('div.customselect');
                    var sValue = cS.attr('name');
                    var sHTML =  cS.find('label').html();
                    $("#hidden_"+cP.attr("id")).val(sValue);
                    cP.attr('val',sValue).find('div.customselect-label').autoEllipsis(sHTML,{
                        'width':cP.attr('offsetWidth') - 20
                    });
                    cP.find('div.customselect-container').hide();
                });
            }
            if($(currentOptions[x]).attr('selected')) {
                if(isMul)
                {
                    listOption.find('input:checkbox').attr('checked','true');
                }
                else {
                    lContainer.autoEllipsis($(currentOptions[x]).html(),{
                        'width':cOffWid -20
                    });
                    container.val('attr',$(currentOptions[x]).val());
                }
            }
            if(maxContainerWidth < widthCalculator.attr('offsetWidth')) {
                maxContainerWidth = widthCalculator.attr('offsetWidth');
            }
        }
        widthCalculator.remove();
        maxContainerWidth = maxContainerWidth > cOffWid ? maxContainerWidth : cOffWid;
        mContainer.css({
            'width':(maxContainerWidth+20)+"px",
            'height':(dropDownList.attr('offsetHeight'))+"px"
        }).append(dropDownList);
        $('div.header').find('div#location').each(function(){
            $(this).find('div.menu').css({
                "height":"255px",
                "width":"150px",
                "overflow":"auto"
            }) ; //add overflow for location select alone.
            $(this).find('div.customselect-container').css({
                "width":"150px"
            });
        });
        $('div.advancedSearch').find('div#location').each(function(){
            $(this).find('div.menu').css({
                "height":"237px",
                "width":"195px",
                "overflow":"auto"
            }) ; //add overflow for location select alone.
            $(this).find('div.customselect-container').css({
                "width":"195px"
            });
        });
        $('div.header').find('div#item_condition').each(function(){
            $(this).find('div.menu').css({
                "width":"148px",
                "overflow":"auto"
            }) ; //add overflow for condition select alone.
            $(this).find('div.customselect-container').css({
                "width":"148px"
            });
        });
        $('div.advancedSearch').find('div#match').each(function(){
            //add overflow for match select alone.
            $(this).find('div.customselect-container').css({
                "width":"195px"
            });
        });
        $('div.advancedSearch').find('div#size').each(function(){
            $(this).find('div.customselect-container').css({
                "width":"177px"
            });
            $(this).find('div.deselectMiddle').css({
                "width":"167px"
            });
        });
        $('div.advancedSearch').find('div#condition').each(function(){
            $(this).find('div.customselect-container').css({
                "width":"177px"
            });
            $(this).find('div.deselectMiddle').css({
                "width":"167px"
            });
        });
        $('div.advancedSearch').find('div#type').each(function(){
            $(this).find('div.customselect-container').css({
                "width":"177px"
            });
            $(this).find('div.deselectMiddle').css({
                "width":"167px"
            });
        });
        $('div.advancedSearch').find('div#perPage').each(function(){
            $(this).find('div.customselect-container').css({
                "width":"171px"
            });
            $(this).find('div.deselectMiddle').css({
                "width":"171px"
            });
        });
        $('div.advancedSearch').find('div#sort').each(function(){
            $(this).find('div.customselect-container').css({
                "width":"187px"
            });
            $(this).find('div.deselectMiddle').css({
                "width":"187px"
            });
        });
		
        //deselect bar at the bottom of multiselect
        if(isMul){
            var deselectContWidth = mContainer.css('width');
            deselectContWidth = (deselectContWidth.substring(0,deselectContWidth.length-2));
            var deselectContainerHTML = "<div class='deselectContainer'><div class='deselectLeft'/><div class='deselectMiddle'  style='width:"+(deselectContWidth-10)+"px'><a class='leftLink'>Deselect all</a><span style='float: right;'><a class='rightLink'><div id='ddcbl_close'></div></a></span></div><div class='deselectRight'></div>";
            mContainer.append(deselectContainerHTML);
        }
        //hide the container when close link in deselect bar is clicked
        $('a.rightLink').bind("click",function(){
            $('div.customselect-container').hide();
        });
        //deselect all checkbox when deselect all link in deselect bar is clicked
        $('a.leftLink').bind("click",function(){
            $(this).parents('div.customselect-container').find('input:checkbox').each(function(){
                if(this.checked = true)
                {    
                    this.checked = false;
                }
            });
            var container = $(this).parents('div.customselect');
            container.find('div.customselect-label').autoEllipsis("Select up to " + container.find('div.item').length + " "+ container.attr('pName'),{
                'width':container.attr('offsetWidth') - 20
            });
        });
        lContainer.bind('click',{
            con:mContainer
        },clickOperation);
    });
    function clickOperation(event) {
        if (event.data.con.css('display') == 'none') {
            event.data.con.css('display','block');
            $(document).bind('click', {
                con:event.data.con
            }, function(event) {
                if (($(event.target).parents('div.customselect').length) == 0 || $(event.target).parents('div.customselect').attr('id') != event.data.con.parents('div.customselect').attr('id')) {
                    $(document).unbind('click',clickOperation);
                    event.data.con.hide();
                }
            });
        } else {
            event.data.con.css('display','none');
            $(document).unbind('click',clickOperation);            
        }
    }
}
var AdvancedSearchObj;
if (!AdvancedSearchObj) {
    AdvancedSearchObj = {};
}
//following should get triggered only if endeca is switched on
AdvancedSearchObj.buildFromAdvanceSearchJSON = function(data,callback) {
    var d_item_condition = data.item_condition_code;
    var d_locations = data.location;
    var d_auction_type = data.auction_type_code;
    var d_lot_price = data.lot_price;
    var d_retail_price = data.retail_price;
    var d_shipping_terms = data.shipping_terms_code;
    var d_shipping_size_classification = data.size_classification_code;

    //fill up category,locations,condition and other drop downs from js file
    var srch_con = $('#new_search');
    var adv_srch_con = $('#newadvsearch');

    //fill up category,locations,condition and other drop downs from js file
    $.getJSON('/json/category_with_auctions.js',function(d_category){
        var categoryHTML = AdvancedSearchObj.createOptionString(d_category);
        srch_con.find('select[name*=category1]').append(categoryHTML);
        adv_srch_con.find('select[name*=category1]').html(categoryHTML);
        if (typeof callback == "function") {
            callback();
        }
    });
    var str = "";
    if (typeof d_locations != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_locations);
        srch_con.find('select[id*=location]').append(str);
        adv_srch_con.find('select[id*=location]').append(str);
    }

    if (typeof d_item_condition != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_item_condition);
        srch_con.find('select[id*=item_condition]').append(str);
        adv_srch_con.find('select[id*=condition]').html(str);
    }

    if (typeof d_auction_type != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_auction_type);
        adv_srch_con.find('select[id*=type]').html(str);
    }

    if (typeof d_shipping_size_classification != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_shipping_size_classification);
        adv_srch_con.find('select[id*=size]').html(str);
    }

    if (typeof d_shipping_terms != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_shipping_terms);
        adv_srch_con.find('select[id*=shipping_terms]').append(str);
    }
    if (typeof d_retail_price != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_retail_price);
        $('select#retailPriceRange', adv_srch_con).html(str);
    }
    if (typeof d_lot_price != "undefined") {
        str = AdvancedSearchObj.createOptionString(d_lot_price);
        $('select#lotPriceRange', adv_srch_con).html(str);
    }
};
AdvancedSearchObj.createOptionString = function(obj){
    var optionStr = '';
    for(var x in obj){
        optionStr+= '<option value="'+obj[x].id+'">'+obj[x].name+'</option>';
    }
    return optionStr;
};

AdvancedSearchObj.triggerSearch = function (context,dimension_id,query,selected){
    var form = $(context).parents('form');
    if(selected==1)
        form.find('input:hidden[name=dim_rmv]').val(dimension_id);
	else 
		form.find('input:hidden[name=dim_rmv]').attr('disabled','disabled');
    form.find('input:hidden[name=query]').val(query);
    form.trigger('submit');
};
AdvancedSearchObj.validateAdvSearch = function(form){
	if(form.searchparam_words && form.searchparam_words.value == "-- Search --")
		form.searchparam_words.value= "";
    if(form.searchparam_username_seller && form.searchparam_username_seller.value == "-- Name --")
        form.searchparam_username_seller.value = "";
    if(form.searchparam_auction_id && form.searchparam_auction_id.value == "-- Number --")
        form.searchparam_auction_id.value = "";
    if(form.searchparam_words.value!=""){
    	form.searchparam_words.value = form.searchparam_words.value;
    }
//    $("#new_advSearch").trigger('submit');
    return true;	
};
AdvancedSearchObj.validateSearch  = function(form){
    var url = form.action+"?cmd=keyword";
    var value = '';
    if(form.searchparam_words.value=="-- Search --")
        form.searchparam_words.value="";
    if(isBlank(form.searchparam_words.value) && isBlank(getSelectField(form.searchparam_dimension[1])) && isBlank(getSelectField(form.searchparam_dimension[0])) && isBlank(getSelectField(form.searchparam_category1)))
        return false;
    url += (!isBlank(form.searchparam_words.value)) ? '&searchparam_words=' + form.searchparam_words.value : '';
    url += (!isBlank(value = getSelectField(form.searchparam_dimension[1]))) ? '&searchparam_dimension=' + value : '';
    url += (!isBlank(value = getSelectField(form.searchparam_dimension[0]))) ? '&searchparam_dimension=' + value : '';
    url += (!isBlank(value = getSelectField(form.searchparam_category1))) ? '&searchparam_category1=' + value : '';
    url += (!isBlank(form.flag.value)) ?'&flag=' + form.flag.value : '';
    window.location = url;
    return false;
};
AdvancedSearchObj.handleLeftNavigationLabels = function(){
    $('ul.leftNavMenu li label a').bind('click',function(){
        $(this).parents('table').find('input:checkbox').trigger('click');
        return false;
    }).css({'text-decoration':'none'});
};
AdvancedSearchObj.setSavedSearchParams = function(srchform){
	if($('#search_name').val() != "") {
			var dataObj = {};
		var flag= false;
		if($('#srch_auction_title',srchform).is(':checked')) $('#srch_words',srchform).attr('name','auction_title');
		var x = $('#srch_words',srchform).val();
		if(document.agentform.words_mode.value=='Exact' && x != "") {
			$('#srch_words',srchform).val('\"'+x+'\"');
			$('#srch_words_mode',srchform).val('All');
		}
		$(srchform).find('.to-submit').each(function(){
			var c = $(this);
			if(c.val()){
				flag = true;
				if(dataObj[c.attr('name')]){
					dataObj[c.attr('name')] = dataObj[c.attr('name')]+ "," + c.val().toString();
				}else {
					dataObj[c.attr('name')] = c.val().toString();
				}
			}
		});
		if(flag){
			$.getJSON("/api/v1/auction_search//endeca_record_search",dataObj,function(data){
				if(data.meta.query) {
					$('#query_string',srchform).val(data.meta.query);
					$('#endeca_search_params',srchform).val(JSON.stringify(data.result.saved_search_query));
					document.agentform.submit();
				} else {
					$('#errorMsg').show().find('li:first').show().next().hide();
					window.location.href+="#errorMsg";
				}            
			});		
		}
	} else {
		$('#search_name').focus();
		$('#errorMsg').show().find('li:last').show().prev().hide();
		window.location.href+="#errorMsg";
	}
	
    return false;
};
AdvancedSearchObj.checkClosedAuctionCount = function () {
	var tC = $('div.tableContainer');
	var closedAuctions = tC.find('div.closed-auction');
	var closed_count = closedAuctions.length;
	var chkbox = $('#closedToggle input:checkbox');
	if(closedAuctions.length == tC.find('div.tableContents').length){
		closedAuctions.show();
		chkbox.attr('checked',true).attr('disabled','true').parent().css({'opacity':0.5});
	} else if(closed_count!=0){		
		var prev_selection = $.cookie('show_closed');
		if(prev_selection && prev_selection =="true"){
			chkbox.attr('checked',true);
			closedAuctions.show();
		} else {
			chkbox.attr('checked',false);
			closedAuctions.hide();
		}
	} else{
		chkbox.attr('checked',false).attr('disabled','true').parent().hide();
	}
	if(!chkbox.is(':disabled')){
		chkbox.bind('click',function(){
			if($(this).is(':checked')){
				closedAuctions.show();
				$.cookie('show_closed',true);
			} else {
				$.cookie('show_closed',false);
				closedAuctions.hide();
			}
		});
	}
};
AdvancedSearchObj.changeUrl = function(url){
    if (url) window.location.href = url;
};
AdvancedSearchObj.initialize = function (callback) {
    $.getJSON("/json/custom_advanced_dim_minified.js",function(data){
        $(document).ready(function(){
			$('#new_advSearch').attr('action',_LSI_NORMAL_URL+'/auction/search');
            //in case of endeca being switched on, rebuild category dropdown to single select - REVERTED
            //AdvancedSearchObj.rebuildCategoryDropDown();
            //build some dropdown drom advanced search json for endeca
            AdvancedSearchObj.buildFromAdvanceSearchJSON(data,callback);
            //change values of location dropdown options to integrate with endeca
            //AdvancedSearchObj.resetLocationOptionValues();
            //handling the left navigation links
            AdvancedSearchObj.handleLeftNavigationLabels();
			AdvancedSearchObj.checkClosedAuctionCount();            
        });
    });
};
var global_user_id;
var images_path;
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
function checkMobile(){
	return jQuery.browser.mobile;
}
function isSTR(){
	return (window.location.href.indexOf('direct') != -1);	
}
if(!isSTR()){
	AdvancedSearchObj.initialize(customSelectFunction);		
}

function formatToDay(openTime){
    var open_time = new Date(openTime*1000);
    var day = open_time.getDate();
    var month = open_time.getMonth()+1;
    month = month>9?month:"0"+month;
    var year = open_time.getFullYear();
    return month+"/"+day+"/"+year;
}

function getCookie(name) {
    //alert(1);
    var dc = document.cookie;
    //alert(dc);
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0)
        {
            return null;
        }
    }
    else
    {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1)
    {
        end = dc.length;
    }

    return unescape(dc.substring(begin + prefix.length, end));
}

//used for carousel
function getItemHTML(item,layout_label,position)
{
    var itemTitle = item.title ;
    var tempDiv = $('<div id="tempDiv" style="visibility:hidden"></div>');
    $('body').append(tempDiv);
    tempDiv.html(itemTitle).autoEllipsisByWord('',{width:100,lines:3});
    itemTitle = tempDiv.html();
    tempDiv.remove();
    /*itemTitle = (itemTitle && itemTitle.length > 40 ) ? itemTitle.substring(0,40)+"..." : itemTitle ;
    // word break
    itemTitle = wbr(itemTitle, 14);*/
    var args={};
    var carouselStrTmpl = new EJS({
        url: "/shared/bu/js/template/Carousel_Structure.js"
    });
    var _LSIURL = window.location.protocol + '//' + window.location.host;
    args.url = _LSIURL+""+item.thumbnail_url;
    args.auctionId = item.auction_id;
    args.title = item.title;
    args.itemTitle = itemTitle;
    args.number_of_items_per_lot = item.number_of_items_per_lot;
    var current_bid = item.current_bid;
    if(current_bid) 
    {
        current_bid =current_bid.substring(0,(current_bid.indexOf(".") + 3));
    }
    else
    {
        current_bid="0.00";
    }
    
    args.current_bid = current_bid;
    args.auction_type_code = item.auction_type_code;
    args.close_time=item.close_time;
    args.layout_label = layout_label;
    args.position = position;
   	args.currency_symbol = "$";
    if ( typeof(uk_flag) != "undefined" && uk_flag == true ) {
    	args.currency_symbol = "&pound;";
    }
    /*var current_price;
    if(args.auction_type_code == "SLB"){
        current_price = "Sealed Bid";
    }else{
        current_price = args.current_bid;
    }
    var carousel_structure = "<li><div class='imageClass' style='background-image: url("+args.url+");'><a ceid="+args.layout_label+"-image-"+args.position+" href='/auction/view?id="+args.auctionId+"'><img style='border:1px solid #fff;cursor:pointer;' src='/shared/bu/images/imageframe.gif' src="+args.url+" /></a></div><br/><div class='ellipsis'><a  ceid="+args.layout_label+"-text-"+args.position+"  href='/auction/view?id="+args.auctionId+"'>"+args.itemTitle+"</a></div><div style='line-height: 18px;'>Quantity in Lot: "+args.number_of_items_per_lot+"</div><div>Lot Price: "+current_price+"</div>";
    if(args.auction_type_code=="closingsoon"){
        carousel_structure +="<div style='color:#F88B3A'>Closing:"+args.close_time+"</div></li>";
    }else{
        carousel_structure += "</li>";
    }*/
    var carousel_structure = carouselStrTmpl.render(args);
    return carousel_structure;
}

//used for auction view carousel
function getAuctionViewItemHTML(item,index)
{
    var args={};
    var carouselStrTmpl = new EJS({
        url: "/shared/bu/js/template/AuctionView_Carousel_Structure.js"
    });
    args.index=index;
    args.path=images_path;
    args.url = item.tinythumb_url;
    var carousel_structure = carouselStrTmpl.render(args);
    return carousel_structure;
}

//used for auction view lighbox generation
function getAuctionViewLightBoxHTML(item,display,index)
{
    var args={};
    var carouselStrTmpl = new EJS({
        url: "/shared/bu/js/template/AuctionView_LightBox_Structure.js"
    });
    args.largeImageUrl = item.largethumb_url;
    args.widgetImageUrl = item.image_url;
    args.display = display;
    args.path=images_path;
    args.index = index;
    var carousel_structure = carouselStrTmpl.render(args);
    return carousel_structure;
}

function getAuctionViewLightBoxTextHTML(item,display){
    var args={};
    var carouselStrTmpl = new EJS({
        url: "/shared/bu/js/template/AuctionView_LightBoxText_Structure.js"
    });
    args.widgetImageUrl =item.image_url;
    if(display == "block"){
        args.bgUrl = "url('/shared/bu/images/imageEnlargeIcon.gif')";
    }else{
        args.bgUrl = "none";
    }
    args.display = display;
    var carousel_structure = carouselStrTmpl.render(args);
    return carousel_structure;
}

function processClick(e){
    if (!e) {
        e = window.event;
    }
    var theTarget = e.target ? e.target : e.srcElement;
    var pNode = theTarget;
    var pClass = pNode.className;
    pClass = pClass.split(" ")[0];
    if(pClass=="tabNavMiddle"){
        var parentNode = $($(pNode).parent());
        var loggedIn = username ? "loggedIn" : "notLoggedIn";
        var layout = "";
        var layoutEl = document.getElementsByTagName('layout');  //$('layout').each() not working in IE8, so using the traditional way of getting elements.
        for(var i=0;i<layoutEl.length;i++){
            if(layoutData[layoutEl[i].id]){
                layout = layoutData[layoutEl[i].id][loggedIn];
            }else{
                layout = layoutData[loggedIn];
            }
        }
        if (parentNode.hasClass("selected")) {
            return false;
        } else if(layout.collections.carousel.length == 1){
            return false;
        }else {
            $('div.tabContainer div.selected').removeClass("selected");
            parentNode.addClass("selected");
            for(var i=0;i<layout.collections.carousel.length;i++){
                if(parentNode.attr("id") == layout.collections.carousel[i]+"Tabbed"){
                    $("#"+layout.collections.carousel[i]+'TabbedBody').css({
                        'display':'block'
                    });
                    if(carouselObj[layout.collections.carousel[i]+'TabbedCarousel'])
                    {
                        carouselObj[layout.collections.carousel[i]+'TabbedCarousel'].reset();
                    }
                }else{
                    $("#"+layout.collections.carousel[i]+'TabbedBody').css({
                        'display':'none'
                    });
                }
            }
        }

    }
}

$(document).ready(function() {
	//commenting typeahead code
	/*$('#new_words').autocomplete({
        source: function( request, response ) {
			$.ajax({url: "/api/v1/auction_search//endeca_dimension_search",
				data : {words:request.term+"*"},
				dataType: "json",				
				success: function(data) {
					if(data.result.length){
						var typeAheads = [];
						var otherData = [];
						for(var i =0 ;i < data.result.length;i++){
							if(data.result[i].label != "TypeAhead"){
								otherData = otherData.concat(formatTypeAheadData(data.result[i].options,data.result[i].label,5));
							} else {
								typeAheads = formatTypeAheadData(data.result[i].options,"",10); 
							}
						}	
						x = typeAheads.concat(otherData)
						response(x);
					}else {
						response([]);
					}				
				},
				error : function(){
					console.log('something went wrong');
					response([]);
				}
			});			
		},
		minLength: 4
    });*/



    //Making changes for endeca
    if(!isSTR()){
        $('#old_search_con').remove();
        $('#new_search_con').show();
		if(checkMobile()){
			$('.header .topMenuContainer').prepend('<span class="bcktomobile" onclick="backtomobile()" >View Mobile Site</span>');	
		}
    } else {
        $('#new_search_con').remove();
    }
    $('.inputText').bind("focus",function(){
        if($(this).val() == $(this).attr("title")){
            $(this).val("");
            $(this).css({
                'color':'#333333'
            });
        }
    });
    $('.inputText').bind("blur",function(){
        if($(this).val() == $(this).attr("title") || $(this).val() == ""){
            $(this).val($(this).attr("title"));
            $(this).css({
                'color':'#999999'
            });
        }
    });
	
    if(isSTR()){
    	//get data from generic_json.js file
	     $.getJSON( "/json/generic_json.js", function(data) {
	        d_category = data.category;
	        d_locations = data.locations;
	        d_item_condition = data.item_condition ;
	        d_auction_type = data.auction_type;
	        d_shipping_size_classification = data.shipping_size_classification;
	        d_shipping_terms = data.shipping_terms;
	
	        var adv_srch_con = $('div.advancedSearch');
	        adv_srch_con.data('generic_data',data);
	        var srch_con = $('div.searchContent');
	
	
	    
	        //fill up category,locations,condition and other drop downs from js file
	        var categoryHTML = "";
	        var locationsHTML= "";
	        var locationsHTMLwoLA = "";
	        var conditionsHTML= "";
	        var auctionTypeHTML= "";
	        var shippingSizeClassHTML= "";
	        var shippingTermsHTML= "";
	    
	        if(typeof d_category != "undefined"){
	            for(var i=0;i<d_category.length;i++){
	                categoryHTML +='<option value="'+d_category[i].id+'">'+d_category[i].name+'</option>';
	            }
	            srch_con.find('select[name*=category1]').append(categoryHTML);
	            adv_srch_con.find('select[name*=category1]').html(categoryHTML);
	        }else {
	            $.getJSON( "/json/category.js", function(data) {
	                d_category = data;
	                for(var i=0;i<d_category.length;i++){
	                    categoryHTML +='<option value="'+d_category[i].id+'">'+d_category[i].name+'</option>';
	                }
	                srch_con.find('select[name*=category1]').append(categoryHTML);
	                adv_srch_con.find('select[name*=category1]').html(categoryHTML);
	            });
	        }
	
	        if(typeof d_locations != "undefined" ){
	            for(var i=0;i<d_locations.length;i++){
	                locationsHTML +='<option value="'+d_locations[i].id+'">'+d_locations[i].name+'</option>';
	            }
	            srch_con.find('select[name*=location]').append(locationsHTML);
	            adv_srch_con.find('select[name*=location]').append(locationsHTML);
	        }else {
	            $.getJSON( "/json/locations.js", function(data) {
	                d_locations = data;
	                for(var i=0;i<d_locations.length;i++){
	                    locationsHTML +='<option value="'+d_locations[i].id+'">'+d_locations[i].name+'</option>';
	                }
	                srch_con.find('select[name*=location]').append(locationsHTML);
	                adv_srch_con.find('select[name*=location]').append(locationsHTML);
	            });
	        }
	    
	        if(typeof d_item_condition != "undefined" ){
	            for(var i=0;i<d_item_condition.length;i++){
	                conditionsHTML +='<option value="'+d_item_condition[i].id+'">'+d_item_condition[i].name+'</option>';
	            }
	            srch_con.find('select[name*=item_condition]').append(conditionsHTML);
	            adv_srch_con.find('select[name*=condition]').html(conditionsHTML);
	        }else {
	            $.getJSON( "/json/item_condition.js", function(data) {
	                d_item_condition = data;
	                for(var i=0;i<d_item_condition.length;i++){
	                    conditionsHTML +='<option value="'+d_item_condition[i].id+'">'+d_item_condition[i].name+'</option>';
	                }
	                srch_con.find('select[name*=item_condition]').append(conditionsHTML);
	                adv_srch_con.find('select[name*=condition]').html(conditionsHTML);
	            });
	        }
	    
	        if(typeof d_auction_type != "undefined" ){
	            for(var i=0;i<d_auction_type.length;i++){
	                if(d_auction_type[i].id!="DUT")
	                {
	                    auctionTypeHTML +='<option value="'+d_auction_type[i].id+'">'+d_auction_type[i].name+'</option>';
	                }
	            }
	            adv_srch_con.find('select[name*=type]').html(auctionTypeHTML);
	        }else {
	            $.getJSON( "/json/auction_type.js", function(data) {
	                d_auction_type = data;
	                for(var i=0;i<d_auction_type.length;i++){
	                    if(d_auction_type[i].id!="DUT")
	                    {
	                        auctionTypeHTML +='<option value="'+d_auction_type[i].id+'">'+d_auction_type[i].name+'</option>';
	                    }
	                }
	                adv_srch_con.find('select[name*=type]').html(auctionTypeHTML);
	            });
	        }
	   
	        if(typeof d_shipping_size_classification != "undefined" ){
	            for(var i=0;i<d_shipping_size_classification.length;i++){
	                shippingSizeClassHTML +='<option value="'+d_shipping_size_classification[i].id+'">'+d_shipping_size_classification[i].name+'</option>';
	            }
	            adv_srch_con.find('select[name*=size]').html(shippingSizeClassHTML);
	        }else {
	            $.getJSON( "/json/shipping_size_classification.js", function(data) {
	                d_shipping_size_classification = data;
	                for(var i=0;i<d_shipping_size_classification.length;i++){
	                    shippingSizeClassHTML +='<option value="'+d_shipping_size_classification[i].id+'">'+d_shipping_size_classification[i].name+'</option>';
	                }
	                adv_srch_con.find('select[name*=size]').html(shippingSizeClassHTML);
	            });
	        }
	    
	        if(typeof d_shipping_terms != "undefined" ){
	            for(var i=0;i<d_shipping_terms.length;i++){
	                shippingTermsHTML +='<option value="'+d_shipping_terms[i].id+'">'+d_shipping_terms[i].name+'</option>';
	            }
	            adv_srch_con.find('select[name*=shipping_terms]').append(shippingTermsHTML);
	            
	        }else {
	            $.getJSON( "/json/shipping_terms.js", function(data) {
	                d_shipping_terms = data;
	                for(var i=0;i<d_shipping_terms.length;i++){
	                    shippingTermsHTML +='<option value="'+d_shipping_terms[i].id+'">'+d_shipping_terms[i].name+'</option>';
	                }
	                adv_srch_con.find('select[name*=shipping_terms]').append(shippingTermsHTML);
	                
	            });
	        }
             customSelectFunction();
	    });
    }
});

function backtomobile(){
	clearCookie('MOBILE');
	location.href = "http://"+location.hostname;
}
function clearCookie(name, domain, path){
    var domain = domain || document.domain;
	var today = new Date();
    var path = path || "/";
    document.cookie = name + "=1; expires=" +today.getTime()+ "; domain=" + domain + "; path=" + path;
};

// Remove watchlist to be used in auction view & success bid page
function removeWatchlist(el,auction_id){
    $("#watchlistIcon").before("<div class='watchListLoading'>Removing...</div>").remove();
    Invoker.RemoveFromWatchlist(auction_id,{}, function(data,o){
        if(data.error && data.error[0]){
            $(".watchListLoading").before("<div class='itemAddedErrorWatchIcon'>"+data.error[0].error_text+"</div>").remove();
            setTimeout(function(){
                $('.itemAddedErrorWatchIcon').fadeOut("slow",function(){
                    if(isSTR()){
                        $('.itemAddedErrorWatchIcon').before("<a pagetype='STR' class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'>Remove Listing</a>").remove();
                    }else{
                        $('.itemAddedErrorWatchIcon').before("<a class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'><b>Remove Auction from Watchlist</b></a>").remove();                        
                    }
                });
            },2000);
        }else if(data.result){
            var u_id = getLoggedInUserId();
            if(isSTR()){
                $(".watchListLoading").before("<a pagetype='STR' href='#' onclick='return addToWatchList(this,"+u_id+","+auction_id+");'>Add to My Watchlist</a>").remove();
            }else{
                $(".watchListLoading").before("<a class='watchIcon' id='watchlistIcon' href='#' onclick='return addToWatchList(this,"+u_id+","+auction_id+");'><b>Add to My Watchlist</b></a>").remove();
            }
        }
    });
    return false;
}




function addToWatchList(el,user_id,auction_id){
    var params={
        "user_id":user_id,
        "auction_id":auction_id
    };
    $(el).before("<div class='watchListLoading'>Adding...</div>").remove();
    Invoker.addItemToWatchList(params,function(data){
        $('.watchListLoading').before(el).remove();
        if(!data.error){
            //$(el).before("<div class='itemAddedSuccessWatchIcon'>Auction added to <a href='/account/main?tab=WatchList#WatchListAnchor'>Watchlist</a></div>").remove();
            if($(el).attr("pagetype")=="STR"){
                $(el).before("<a pagetype='STR' class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'>Remove Listing</a>").remove();
            }else{
                if(str)
                {
                    $(el).before("<a class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'><b>Remove Listing from Watchlist</b></a>").remove();
                }
                else
                {
                    $(el).before("<a class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'><b>Remove Auction from Watchlist</b></a>").remove();
                }
            }
        }else{
            if(data.error[0].status_code == "403"){
                if(isSTR()){
                    $(el).before("<a pagetype='STR' class='notLoggedinErrorIcon' href='javascript:void(0);'>Please Log In</a>").remove();
                }else{
                    $(el).before("<a class='notLoggedinErrorIcon' href='javascript:void(0);'><b>Please Log In to Add to Your Watchlist</b></a>").remove();
                }
				var url=location.pathname+location.search+"&isAddWatchlist=true";
				url += !isSTR() ? "&flag=new" : "";
                var element = $('.notLoggedinErrorIcon');
                window.location.href = "/login?page="+escape(url);
            }else if(data.error[0].status_code == "200"){
                if(isSTR()){
                    $(el).before("<a pagetype='STR' class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'>Remove Listing</a>").remove();
                }else{
                    $(el).before("<a class='removeIcon' id='watchlistIcon' href='#' onclick='javascript:return removeWatchlist(this,"+auction_id+");'><b>Remove Auction from Watchlist</b></a>").remove();
				}
            //$(el).before("<div class='itemAddedErrorWatchIcon'>"+data.error[0].error_text+"</div>").remove();
            }else{
                if(isSTR()){
                    $(el).before("<div pagetype='STR' class='itemAddedErrorWatchIcon'>Error!!</div>").remove();
                }else{
                    $(el).before("<div class='itemAddedErrorWatchIcon'>Error, Please try again later</div>").remove();
                }
                setTimeout(function(){
                    $('.itemAddedErrorWatchIcon').fadeOut("slow",function(){
                        $('.itemAddedErrorWatchIcon').before(el).remove();
                    });
                },2000);
            }
        }
    });
    return false;
}

function listingAddtoWatchlist(el,user_id,auction_id,search_query,param){

    var params={
        "user_id":user_id ? user_id : global_user_id,   
        "auction_id":auction_id
    };
	if(!params.user_id) return true;
    var bgurl = $(el).css("background");
    $(el).css({
        "background":"url('/shared/bu/images/add_watchlist_loading.gif') no-repeat"
    });
    $(el).removeAttr("href");
    Invoker.addItemToWatchList(params,function(data){
        if(!data.error){
            $(el).css({
                "background":"url('/shared/bu/images/saveSearch-success.gif') no-repeat"
            });
            $(el).next().find('label').html("Auction added to Watchlist");
            el.onclick=null;
            $(el).click(function(){
                window.location.href = "/account/main?tab=WatchList#WatchListAnchor";
            });
        }else{
            if(data.error[0].status_code == "403"){
                //  $(el).css({"background":"url('/shared/bu/images/saveSearch-error.gif') -3px 0px no-repeat"});
                if(param) {
                    var url=location.pathname+"?query="+search_query+"&auctionId="+auction_id+"&_page="+param.page+"&sort="+param.sort+"&ascending="+param.ascending+"&isAddWatchlist=true";
					url += !isSTR() ? "&flag=new" : "";
                }else{
                    var url = "/";
                }
                window.location.href = "/login?page="+escape(url);
            }else if(data.error[0].status_code == "200"){
                $(el).css({
                    "background":"url('/shared/bu/images/saveSearch-error.gif') 0px 0px no-repeat"
                });
                $(el).next().find('label').html(data.error[0].error_text);
                $(el).next().show();
                el.onclick=null;
                if($(el).next().attr('id')=="watch_tooltip")
                {
                    setTimeout(function(){
                        $(el).next().fadeOut("slow");
                    },2000);
                }
            }else{
                $(el).css({
                    "background":"url('/shared/bu/images/saveSearch-error.gif') 0px 0px no-repeat"
                });
                $(el).next().html(data.error[0].error_text);
                el.onclick=null;
                setTimeout(function(){
                    $(el).fadeOut("slow",function(){
                        $(el).css("background",bgurl);
                        $(el).show();
                        $(el).next().find('label').html("Add to Watchlist");
                      
                        el.onclick=listingAddtoWatchlist(el,global_user_id,auction_id,search_query,param);
                        el.href="/account/main?tab=WatchList&auctionId="+auction_id+"&addSubmit.x=1";
                    });
                },2000);
            }
        }
    });
    return false;

}

function arrayContains(arr,v){
    for (i=0; i<arr.length; i++){
        if (arr[i]==v) {
            return true;
        }
    }
    return false;
}

function shuffle(o){
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function openWhatsThisPopup(url) {
    window.open (url, "WhatIsThisPopup",
        "width=450," +
        "height=625," +
        "channelmode=0," +
        "directories=0," +
        "location=0," +
        "menubar=0," +
        "resizable=0," +
        "scrollbars=1," +
        "status=0," +
        "titlebar=0," +
        "toolbar=0"
        );
}

function bidAmountValidate(obj,minimum_bid){
    var val = $("input[type='text'][name='bidAmount']").val();
    val = val.replace(/,/,"");
    if(isNaN(val)){
        pushValue(null);
        $("div.bidAmountCheck").show();
        $(obj).prev().focus();
        return false;
    } else  {

	
        if(val<minimum_bid)
        {
            pushValue(null);
            $("div.bidAmountCheck").show();
            $(obj).prev().focus();
            return false;
        }
        else
        {
            pushValue(val);
            $("div.bidAmountCheck").hide();
            return true;
        }
    }
}

function pushValue(v){
    v?$("input[name='bidAmount']").val(v):$("input[name='bidAmount']").val("");
}

function getLoggedInUserId(){
    return getCookie("WWW_SESSION_ID_USER") ? getCookie("WWW_SESSION_ID_USER") : getCookie("WWW_SESSION_ID_DEV_USER");
}

function formatTo12(closeTime){
    var close_time = new Date(closeTime*1000);
    var hours = close_time.getHours();
    var minutes = close_time.getMinutes();
    minutes = minutes + "";
    if (minutes.length == 1){
        minutes = "0" + minutes;
    }
    if(hours>=12){
        if((hours-12)==0){
            return hours+":"+minutes+" PM";
        }else if((hours-12)==12){
            return "12:"+minutes+" AM";
        }else{
            return (hours-12)+":"+minutes+" PM" ;
        }
    }else{
        return hours+":"+minutes+" AM"  ;
    }
}

/* introduce word break \S Match any character NOT whitespace */
function wbr(str, num) { 
    return str.replace(RegExp("(\\S{" + num + "})(\\S)", "g"), function(all,text,char){
        return text + "<wbr>" + char;
    });
}


function states_basedon_country(country_code,state_code){
    if(country_code == "US" || country_code == "CA")
    {
        Invoker.statesBasedOnCountry(country_code,function(msg){
            var insert_text;
            insert_text='<select class="inp-se-Style" name="state" id="dynamic_states" >';
            for(var i=0;i<msg.result.length;i++){
                for(var obj in msg.result[i]){
                    if(obj==state_code)
                    {
                        insert_text+='<option value="'+obj+'"  selected >'+msg.result[i][obj] +'</option>';
                    }
                    else
                    {
                        insert_text+='<option value="'+obj+'" >'+msg.result[i][obj] +'</option>';
                    }
                }
            }
            insert_text+='</select>';
            $('#dynamic_states').after(insert_text).remove();
				
        });
    }//end of CA and US
    else {
        //for non CA and US states
        if(state_code){
            $('#dynamic_states').after('<input class="inp-se-Style" id="dynamic_states" type="text" name="state" value="'+state_code +'"/>').remove();
        } else {
            $('#dynamic_states').after('<input class="inp-se-Style" id="dynamic_states"  type="text" name="state" />').remove();
        }
    }
}




function currentESTTime(t) {
    // create Date object for current location
    d = t?new Date(t):new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    //offset of EST
    var offset = '-4';

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*offset));

    // return time as a Date
    return new Date(nd);
}



function viewManifest(user_id,auction_id){
    var id=user_id ? user_id : global_user_id;
    if(id == ""||id == undefined){
        window.location = "/login?page=/";
    }else{
        window.open('/auction/view_csv?id='+auction_id+'','proxy','menubar=yes,width=800,height=600,scrollbars=yes,location=no');
    }
}

function escape2HTML(text) {
	var replacements = ["&amp;", "&#xa;","&quot;"];
	var replaceWith = ["&", "",'"'];
	for(i=0;i<replacements.length;i++){
		 text = text.replace(replacements[i], replaceWith[i]);
	}
	return text;
}

function formatTypeAheadData(options,cat,cnt){
	options = options.slice(0,cnt);
	var x = $.map( options, function( item ) {
		return {
			label: escape2HTML(item.name),
			value: escape2HTML(item.name),
			category : cat
		}
	});
	return x;
}
function validate_search (form) {
    var url = form.action+"?cmd=keyword";
    var value = '';
    if(form.words.value=="-- Search --")
        form.words.value="";
    if(isBlank(form.words.value) && isBlank(getSelectField(form.location)) && isBlank(getSelectField(form.item_condition)) && isBlank(getSelectField(form.category1)))
        return false;

    url += (!isBlank(form.words.value)) ? '&words=' + form.words.value : '';
    url += (!isBlank(value = getSelectField(form.location))) ? '&location=' + value : '';
    url += (!isBlank(value = getSelectField(form.item_condition))) ? '&condition_' + value + '=1' : '';
    url += (!isBlank(value = getSelectField(form.category1))) ? '&category1=' + value : '';
    window.location = url;
    return false;
}

function getSelectField (field) {
  return field.value;
}

function isBlank (s) {
 for (var i = 0; i < s.length; i++) {
	 var c = s.charAt(i);
	 if ((c != ' ') && (c != '\n') && (c != '\t')) return false;
 }

 return true;
}

function validate_advSearch(form){
	if(form.words && form.words.value == "-- Search --")
		form.words.value= "";
    if(form.seller && form.seller.value == "-- Name --")
        form.seller.value = "";
    if(form.auction_id && form.auction_id.value == "-- Number --")
        form.auction_id.value = "";
    if(form.words.value!=""){
    	form.words.value = (form.words.value).replace('&','%26');
    }
    $("#advSearch").trigger('submit');
    return true;
}

function saveSearch(isloggedIn,search_param,search_query){
    isloggedIn = isloggedIn?isloggedIn:global_user_id;
   // if(search_param.search_name=="")
    if(!isSTR()){
	search_param.flag="new";
	}
    search_param.search_name=$('#saveSearchText').val();
	if(search_param.email_flag=="")
	search_param.email_flag=$('#runSearchCheck').attr("checked")?1:0;
	if(search_param.search_name == "")
		return;
    $('input.saveBtn').css({"background":"transparent url(/shared/bu/images/add_watchlist_loading.gif) no-repeat scroll 0 0"});
    if(isloggedIn){
        if(search_param.user_id == "")search_param.user_id = global_user_id;
        Invoker.createSavedSearch(search_param,function(data){
			if(!data.error){
				$('div.saveSearchContainer').hide();
				$('ul#saveSearch').find('li').html("<div class='saveSearchSuccess'>Search saved successfully to <a href='/account/main?tab=SearchAgent'>My Account</a></div>");
			}else{
				$('div.saveSearchContainer').hide();
				var el=$('ul#saveSearch').find('li').html();
				$('ul#saveSearch').find('li').html("<div class='saveSearchFailure'>Save was unsuccessful,Please try again</div>");
				setTimeout(function(){
                   $('.saveSearchFailure').fadeOut("slow",function(){
                      $('.saveSearchFailure').before(el).remove();
					  $("a#saveSearchLink").click(function(){
						if($("div.saveSearchContainer").css('display') == "none"){
							$("div.saveSearchContainer").show();
						}else{
							$("div.saveSearchContainer").hide();
						}

	});
                   });
                },2000);
			}
		});
	}else{
        var url = location.pathname+"?query="+search_query+"&search_name="+search_param.search_name+"&email_flag="+search_param.email_flag+"&_page="+search_param.page+"&sort="+search_param.sort+"&ascending="+search_param.ascending+"&isSaveSearch=true";
		if(!isSTR()) url+="&flag=new"
        window.location.href = "/login?page="+escape(url);
    }
}

function toggleView(){
	if($("#imgMorelink").attr("src")=="/shared/bu/images/sorting-icon_up.png"){
		$("#imgMorelink").attr("src","/shared/bu/images/sorting-icon_down.png");
		$("#aMorelink").html("More Sellers");
	}else {
		$("#imgMorelink").attr("src","/shared/bu/images/sorting-icon_up.png");
		$("#aMorelink").html("Less Sellers");
	}
	$('.hidden').toggle();
	return false;
}
var Invoker;
if (!Invoker) { 
    Invoker = {};
}

var _LSIURL = window.location.protocol + '//' + window.location.host;
var _LSISECUREURL =  "https://" + window.location.host;
var _LSI_NORMAL_URL = _LSIURL;
Invoker.callCollectionAPI = function(url,collName,callback){
    url = _LSIURL + url+"?&_select=auctions";
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data,collName);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error :[ {
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }]
            },collName );
        }
    });
};

Invoker.addItemToWatchList = function(param,callback){
    var url = "/api/v1/watchlist//create";
    url = _LSIURL + url;
    $.ajax({
        type:"POST",
        data:param,
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.createSavedSearch = function(param,callback){
    var url = "/api/v1/auction_saved_searches//create";
    url = _LSIURL + url;
    $.ajax({
        type:"POST",
        data:param,
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.removeShipping = function(transaction_id,callback){
    var url = "/api/v1/transaction/"+transaction_id+"/removeshipping";
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.statesBasedOnCountry = function(country_code,callback){
    var url = "/api/v1/address//statecodes?_select="+country_code;
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.accessCheck = function(rule, callback){
    var url = "/api/v1/access//check?rule="+rule;
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.GetRecomendedAuctions = function(callback){
    var url = "/api/v1/collection_master//recommended"; //"/api/v1/collection_master/7?_select=title&_select=auctions"
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback( {
                error : [ {
                    status_code:XMLHttpRequest.status,
                    errorText:
                    textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }
                ]
            } );
        }
    });
};

Invoker.GetRecomendedLeftNav = function(params,callback){
    var url = "/api/v1/auction_search//narrow"; //"/api/v1/collection_master/7?_select=title&_select=auctions"
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        data:params,
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback( {
                error : [ {
                    status_code:XMLHttpRequest.status,
                    errorText:
                    textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }
                ]
            } );
        }
    });
};

Invoker.GetAllAuctions = function(paramString, callback){
    var url = "/api/v1/auction_search//search?"+paramString;
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback( {
                error : [ {
                    status_code:XMLHttpRequest.status,
                    errorText:
                    textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }
                ]
            } );
        }
    });
};

Invoker.RemoveFromWatchlist = function(auction_id, obj, callback){
    var url = "/api/v1/watchlist/"+auction_id+"/delete"; //"/api/v1/collection_master/7?_select=title&_select=auctions"
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data, obj);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback( {
                error : [ {
                    status_code:XMLHttpRequest.status,
                    errorText:
                    textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }
                ]
            } );
        }
    });
};


Invoker.setDefaultShippingFlag = function(address_id,callback){
    var url = "/api/v1/user_address/"+address_id+"/update";
    url = _LSIURL + url;
    $.ajax({
        type:"POST",
        data:"default_shipping_location_flag=1",
        url: url,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    error_text: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.allAddressOfUser = function(user_id,callback){
    var url = "/api/v1/user_address//search?"+"user_id="+user_id+"&record_status=A";
    var params1="&_select=name&_select=address_type&_select=shipping_address_type_code&_select=state&_select=city&_select=postal_code";
    var params2="&_select=address1&_select=address2&_select=country_code&_select=default_billing_location_flag&_select=default_shipping_location_flag";
    url+=params1+params2;
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    error_text: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.createCreditCard = function(user_id,str,callback){
    var url = "/api/v1/creditcard//create";
    url =_LSIURL + url;
    $.ajax({
        type:"POST",
        data:str+'&user_id='+user_id,
        url: url,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    error_text: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.bidBoxDataCall = function(auction_id,record_status,callback){
    var url ="/api/v1/auction_view/"+auction_id;
    url = _LSIURL + url;
	
    var params1="_select=current_bid&_select=next_bid&_select=close_time&_select=close_flag&_select=record_status&_select=auction_type_code&_select=offer_rate&_select=quantity";
    if(record_status!="SV"){ 
        params1=params1+ "&_select=price_per_unit";
    }
    var params2="&_select=retail_price&_select=buy_now_price&_select=number_of_bids&_select=number_of_lots_remaining&_select=auction_type_code&_select=open_time&_select=winner&_select=time_until_close&_select=close_time";
	
    $.ajax({
        type:"GET",
        data:params1+params2,
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
             callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error :[ {
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }]
            } );
        }
    });
};

Invoker.loginAuth = function(params,callback){
    var url =  "/api/v1/user//login";
    url = _LSIURL + url;
    $.ajax({
        type:"POST",
        data:'username='+params.id+'&password='+params.pwd+'&page='+params.page,
        url: url,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error : [{
                    status_code:XMLHttpRequest.status,
                    error_text: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                } ]
            } );
        }
    });
};

Invoker.userIdentify = function(callback){
	if ( Invoker.userIdentifyCalled != undefined )
		return;
	Invoker.userIdentifyCalled = true;

    var url ="/api/v1/user//identify?_select=premium_group";
    url = _LSIURL + url;
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error :[ {
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }]
            } );
        }
    });
};

Invoker.carouselApi = function(url,callback){
    // var url ="/api/v1/user//identify";
    url = _LSIURL + url+"?_select=auctions";
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error :[ {
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }]
            } );
        }
    });
}

Invoker.tabbedCarouselApi = function(category,callback){
    var url =_LSIURL + "/api/v1/collection_master//search?";
    for(var i=0;i<category.length;i++){
        url = url+"label="+category[i]+"&";
    }
    url=url+"record_status=A&_select=auctions&_select=title&_select=label";
    $.ajax({
        type:"GET",
        url: url,
        cache: false,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            callback({
                error :[ {
                    status_code:XMLHttpRequest.status,
                    errorText: textStatus,
                    xmlrequest:XMLHttpRequest,
                    status:"false"
                }]
            } );
        }
    });
}

Invoker.updateSignIn = function(identifyResponse) {
    var str_d;
    var premium_group = $.cookie('premium_group');
    if(identifyResponse.result && identifyResponse.result.username && !identifyResponse.result.error)
    {
        username=identifyResponse.result.username;
		premium_group = identifyResponse.result.premium_group;
        global_user_id=identifyResponse.result.user.id;
        str_d = Invoker.getSignDetails(username,global_user_id);
        if(str){
			var url_logged = "http://www.surveymonkey.com/s.aspx?sm=AS0aWR_2bPJoBQbTGp3RP2Bw_3d_3d&c="+identifyResponse.result.user.id;

			if(identifyResponse.result.username)
			{   $('#myAccountFooterLink').show();
				$("a.takesurvey").attr("href",url_logged);
			}        
        }
    }
    else if ( username = $.cookie('username') )
    {
    	str_d = Invoker.getSignDetails(username);
    }
    else
    {
    	str_d = Invoker.getSignDetails();
        if ( str ) {
			$('#loginFooterLink').show();
			var url_not_logged = "http://www.surveymonkey.com/s.aspx?sm=AS0aWR_2bPJoBQbTGp3RP2Bw_3d_3d&c=nli";
			$("a.takesurvey").attr("href",url_not_logged);        
        }
    }
    $('#signDetails').html(str_d);
    
    /* display tabbed navigation for STR users */
	var lcomURI = document.location.href.replace(/\/direct\./,"/").replace(/\.com.*$/,".com");
	$("div#tabLCOM a").attr("href",lcomURI);	
	if( premium_group && premium_group.match(/STR/) )
	{
		$('.auctionTabbedContainerHeader').show();
		var directURI = document.location.href.replace(/http:\/\/(www.)?/,"http://direct.").replace(/\.com.*$/,".com");
		$("div#tabSTR a").attr("href",directURI);
	}
	
};

Invoker.getSignDetails = function(username, user_id) {
	var details;
	var recommended_url = str ? "/recommended.html" : "/bu/recommended.html";
	if ( username ) {
		details="Hello, "+username+"! We have <a href='"+recommended_url+"'>recommendations</a> for you. (<a href='/logout'>logout</a>)";
	} else {
		details = "Hello!&nbsp;<a href='/login'>Sign in</a> for today's best deals, or <a href='/register'>register now</a>.";
	}
	return details;
};