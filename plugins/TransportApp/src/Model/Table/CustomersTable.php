<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \TransportApp\Model\Table\SalesTable&\Cake\ORM\Association\HasMany $Sales
 *
 * @method \TransportApp\Model\Entity\Customer newEmptyEntity()
 * @method \TransportApp\Model\Entity\Customer newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Customer findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Customer[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Customer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Customer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CustomersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Sales', [
            'foreignKey' => 'customer_id',
            'className' => 'TransportApp.Sales',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 180)
            ->allowEmptyString('name');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 45)
            ->allowEmptyString('phone');

        $validator
            ->scalar('address')
            ->allowEmptyString('address');

        $validator
            ->scalar('area')
            ->maxLength('area', 45)
            ->allowEmptyString('area');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->scalar('vehicle')
            ->maxLength('vehicle', 45)
            ->requirePresence('vehicle', 'create')
            ->notEmptyString('vehicle');

        return $validator;
    }
}
