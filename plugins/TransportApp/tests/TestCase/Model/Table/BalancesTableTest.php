<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\BalancesTable;

/**
 * TransportApp\Model\Table\BalancesTable Test Case
 */
class BalancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\BalancesTable
     */
    protected $Balances;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Balances',
        'plugin.TransportApp.Bills',
        'plugin.TransportApp.Salesitems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Balances') ? [] : ['className' => BalancesTable::class];
        $this->Balances = $this->getTableLocator()->get('Balances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Balances);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
