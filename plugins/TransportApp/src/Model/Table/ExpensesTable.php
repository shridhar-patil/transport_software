<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Expenses Model
 *
 * @method \TransportApp\Model\Entity\Expense newEmptyEntity()
 * @method \TransportApp\Model\Entity\Expense newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Expense[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Expense get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Expense findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Expense patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Expense[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Expense|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Expense saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Expense[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Expense[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Expense[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Expense[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ExpensesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('expenses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('expense')
            ->maxLength('expense', 90)
            ->allowEmptyString('expense');

        $validator
            ->nonNegativeInteger('cost')
            ->requirePresence('cost', 'create')
            ->notEmptyString('cost');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmptyDate('date');

        return $validator;
    }
}
