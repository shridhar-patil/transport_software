
<style>
    .container {  
      display:flex;
       flex-direction:row;
       flex-wrap:wrap;
      
      }
      .form {  
      display:flex;
       flex-direction:row;
       flex-wrap:wrap;
      
      }
      .input{
        
       display:flex;
       flex-direction:row;
      }
      #date{
           width: 300px;
          
      }
      button{
           width: 200px;
          
      }
</style>
<h1>Create Customer नवीन खातेदार फॉर्म  </h1>
<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $customer
 */
?>
<div> <h4 class="heading"><?= __('Actions') ?></h4></div>
   

<div class="container">
     <div>  <?= $this->Html->link(__('List Customers'), ['action' => 'index'], ['class' => 'side-nav-item']) ?> </div>  
    
        <div class="customers form content">
            <div class="form"> <?= $this->Form->create($customer) ?></div>
            
          
                <div> <?php echo $this->Form->control('name');?></div>
                   <div> <?php echo $this->Form->control('phone');?></div>
                    <div> <?php echo $this->Form->control('address');?></div>
                    <div> <?php echo $this->Form->control('area');?></div><br>
                   <div> <?php echo $this->Form->control('date', ['empty' => true]);?></div>
                   <div> <?php echo $this->Form->control('vehicle');?></div>
                    <div>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                    <div>
        </div>
    
</div>
