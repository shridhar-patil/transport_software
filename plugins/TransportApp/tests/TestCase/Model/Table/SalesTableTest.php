<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\SalesTable;

/**
 * TransportApp\Model\Table\SalesTable Test Case
 */
class SalesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\SalesTable
     */
    protected $Sales;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Sales',
        'plugin.TransportApp.Customers',
        'plugin.TransportApp.Salesitems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Sales') ? [] : ['className' => SalesTable::class];
        $this->Sales = $this->getTableLocator()->get('Sales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Sales);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
