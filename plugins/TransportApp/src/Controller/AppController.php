<?php
declare(strict_types=1);

namespace TransportApp\Controller;

use App\Controller\AppController as BaseController;
use Cake\Utility\Security;
use Cake\Event\EventInterface;

class AppController extends BaseController
{
   public function beforeRender(\Cake\Event\EventInterface $event)
    {
        $this->viewBuilder()->setTheme('TransportApp');
    }
    public function beforeFilter(EventInterface $event)
{
    parent::beforeFilter($event);
$this->loadComponent('Security');
//    $this->Security->setConfig('blackHoleCallback', 'blackhole');
            $this->Security->setConfig('unlockedActions', ['insert2']);
                        $this->Security->setConfig('validatePost', false);
                        
                    
}

public function blackhole($type, SecurityException $exception)
{
    if ($exception->getMessage() === 'Request is not SSL and the action is required to be secure') {
        // Reword the exception message with a translatable string.
        $exception->setMessage(__('Please access the requested page through HTTPS'));
    }

    // Re-throw the conditionally reworded exception.
    throw $exception;

    // Alternatively, handle the error, e.g. set a flash message &
    // redirect to HTTPS version of the requested page.
}
}
