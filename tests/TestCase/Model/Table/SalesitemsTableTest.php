<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesitemsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesitemsTable Test Case
 */
class SalesitemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesitemsTable
     */
    protected $Salesitems;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Salesitems',
        'app.Sales',
        'app.Balances',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Salesitems') ? [] : ['className' => SalesitemsTable::class];
        $this->Salesitems = $this->getTableLocator()->get('Salesitems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Salesitems);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
