<?php

declare(strict_types=1);

namespace TransportApp\Controller;

use TransportApp\Controller\AppController;

/**
 * SalesPayments Controller
 *
 * @property \TransportApp\Model\Table\SalesPaymentsTable $SalesPayments
 * @method \TransportApp\Model\Entity\SalesPayment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalesPaymentsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Sales'],
        ];


        $salesPayments = $this->paginate($this->SalesPayments);
        
        

        $this->set(compact('salesPayments'));
    }

    /**
     * View method
     *
     * @param string|null $id Sales Payment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $salesPayment = $this->SalesPayments->get($id, [
            'contain' => ['Sales'],
        ]);

        $this->set(compact('salesPayment'));
    }

    public function pendingpayments() {
        $salesPayment = $this->SalesPayments->newEmptyEntity();
        if ($this->request->is('post')) {
            
            
            $payment_amount = $this->request->getData('payment_amount');
            $salesid = $this->request->getData('sales_id');
            
       //     $sql ="select s.id ,s.total from sales s join salespayments sp on s.id =sp.sales_id where sp.sales_id=".$salesid;
            
            
            $salespayments  = $this->SalesPayments->findBySalesId($salesid);
            $sales  = $this->SalesPayments->Sales->findById($salesid)->first()->toArray();
        
            $sumOfpayment_amount =  $salespayments->sumOf('payment_amount');
           
            $salesPayment = $this->SalesPayments->patchEntity($salesPayment, $this->request->getData());
            
            if ($this->SalesPayments->save($salesPayment)  &&
                    ($sales['total'] > $sumOfpayment_amount ) &&
                    ($sales['total'] >= ($sumOfpayment_amount+$payment_amount )) ) {
                $this->Flash->success(__('The sales payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sales payment could not be saved. Please, try again.'));
        }
        $sales = $this->SalesPayments->Sales->find('list',
                        ['keyField' => 'id',
                            'valueField' => function ($sale) {
                                return $sale->get('label');
                            }])
                ->select(['id', 'name', 'date', 'total', 'itemname']);
                            
                            

        $this->set(compact('salesPayment', 'sales'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Payment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $salesPayment = $this->SalesPayments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesPayment = $this->SalesPayments->patchEntity($salesPayment, $this->request->getData());
            if ($this->SalesPayments->save($salesPayment)) {
                $this->Flash->success(__('The sales payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sales payment could not be saved. Please, try again.'));
        }
        $sales = $this->SalesPayments->Sales->find('list', ['limit' => 200]);
        $this->set(compact('salesPayment', 'sales'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Payment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $salesPayment = $this->SalesPayments->get($id);
        if ($this->SalesPayments->delete($salesPayment)) {
            $this->Flash->success(__('The sales payment has been deleted.'));
        } else {
            $this->Flash->error(__('The sales payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
