<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gst Model
 *
 * @method \TransportApp\Model\Entity\Sm newEmptyEntity()
 * @method \TransportApp\Model\Entity\Sm newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Sm findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Sm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sm|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sm saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sm[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class PurchaseTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('purchase');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');
       
        $validator
            ->scalar('purchase_ac')
            ->maxLength('purchase_ac', 30)
            ->allowEmptyString('purchase_ac');

        $validator
            ->scalar('invoice_no')
            ->maxLength('invoice_no', 30)
            ->allowEmptyString('invoice_no');

        $validator
            ->date('invoice_no_date')
            ->allowEmptyDate('invoice_no_date');

        $validator
            ->scalar('party_name')
            ->maxLength('party_name', 30)
            ->allowEmptyString('party_name');

                    
        $validator
            ->scalar('order_no')
            ->maxLength('order_no', 30)
            ->allowEmptyString('order_no');

        $validator
            ->date('order_no_date')
            ->allowEmptyDate('order_no_date');
        
        $validator
            ->scalar('pay_to')
            ->maxLength('pay_to', 30)
            ->allowEmptyString('pay_to');

        $validator
            ->scalar('from_ps')
            ->maxLength('from_ps', 30)
            ->allowEmptyString('from_ps');
            
        $validator
            ->scalar('o_no')
            ->maxLength('o_no', 30)
            ->allowEmptyString('o_no');
        
        $validator
            ->scalar('quality_description')
            ->maxLength('quality_description', 30)
            ->allowEmptyString('quality_description');

        $validator
            ->scalar('hsn_code1')
            ->maxLength('hsn_code1', 30)
            ->allowEmptyString('hsn_code1');

        $validator
            ->scalar('hsn_code2')
            ->maxLength('hsn_code2', 30)
            ->allowEmptyString('hsn_code2');
            
        $validator
            ->integer('quantity')
            ->allowEmptyString('quantity');
        
        $validator
            ->integer('rate')
            ->allowEmptyString('rate');

        $validator
            ->integer('amount')
            ->allowEmptyString('amount');
          
        $validator
            ->scalar('add1')
            ->maxLength('add1', 30)
            ->allowEmptyString('add1');

        $validator
            ->scalar('add_value1')
            ->maxLength('add_value1', 30)
            ->allowEmptyString('add_value1');

        $validator
            ->scalar('add2')
            ->maxLength('add2', 30)
            ->allowEmptyString('add2');

        $validator
            ->scalar('add_value2')
            ->maxLength('add_value2', 30)
            ->allowEmptyString('add_value2');

        $validator
            ->scalar('add3')
            ->maxLength('add3', 30)
            ->allowEmptyString('add3');
        
        $validator
            ->scalar('add_value3')
            ->maxLength('add_value3', 30)
            ->allowEmptyString('add_value3');
        
        $validator
            ->scalar('deduction1')
            ->maxLength('deduction1', 30)
            ->allowEmptyString('deduction1');

        $validator
            ->scalar('deduction_value1')
            ->maxLength('deduction_value1', 30)
            ->allowEmptyString('deduction_value1');

        $validator
            ->scalar('deduction2')
            ->maxLength('deduction2', 30)
            ->allowEmptyString('deduction2');
            
        $validator
            ->scalar('deduction_value2')
            ->maxLength('deduction_value2', 30)
            ->allowEmptyString('deduction_value2');
        
        $validator
            ->scalar('deduction3')
            ->maxLength('deduction3', 30)
            ->allowEmptyString('deduction3');

        $validator
            ->scalar('deduction_value3')
            ->maxLength('deduction_value3', 30)
            ->allowEmptyString('deduction_value3');

        $validator
            ->scalar('tax_type')
            ->maxLength('tax_type', 30)
            ->allowEmptyString('tax_type');

        $validator
            ->scalar('total_weight')
            ->maxLength('total_weight', 30)
            ->allowEmptyString('total_weight');

        $validator
            ->date('due_day')
            ->allowEmptyDate('due_day');
        
        return $validator;
    }
}