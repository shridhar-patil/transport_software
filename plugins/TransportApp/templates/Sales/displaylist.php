<style>
    @media print {
        td,tr,select{
            line-height: 2px;
            border-style: 1px solid rgb(233, 225, 225);
            color: rgb(253, 249, 249);
            font-size: 10px;
            font-weight: lighter;
        }
        .top_nav,h2,p{
            position: absolute;
            top: -9999px;
            left: -9999px;
        }
    }
</style>
<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<?php
echo"<h2>Delete List</h2><p>Once deleted can not be recovered!";
?><br>
डिलीट प्रेस करताच डिलीट होईल -प्रोम्पट येणार नाही!
<h3><?= __('Sales सगळे तपशील विक्री') ?></h3>
<div>
    <table class= "table table-bordered table-hover" id="testTabl">
        <thead>
            <tr>

                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('itemname') ?></th>
                <th><?= $this->Paginator->sort('itemrate') ?></th>
                <th><?= $this->Paginator->sort('itemquantity') ?></th>
                <th><?= $this->Paginator->sort('total') ?></th>   
                <th class="actions"><?= __('Actions') ?></th>

            </tr>
        </thead>
        <tbody>
<?php foreach ($sales_rows as $sales): ?>
                <tr>

                    <td><?= h($sales['name']) ?></td>
                    <td><?= h($sales['itemname']) ?></td>
                    <td><?= h($sales['itemrate']) ?></td>
                    <td><?= h($sales['itemquantity']) ?></td>
                    <td><?= h($sales['total']) ?></td>
                    <td>
    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sales->id)]) ?>
                    </td>

                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    
    
      <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
    
    
    
</div>

<script>

    $(document).ready(function () {
        $('.table').dataTable(
                {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                }
        );
    });



</script>



