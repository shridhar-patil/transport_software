<style>
    .container {

        display:flex;
        flex-direction:row;
        flex-wrap:wrap;

    }
</style>
<h1> Daiky Entry दैनिक एन्ट्री १ </h1>
<?php echo $this->Form->create($sale, array('id'=>'SaleInsert2Form')); ?>
<div class="container">
    
    <label>Customer No1</label>
    <!-- <input type="text" id="customerid" name="customer_id" onchange="customer();"/> -->
    
    <div class="item2">
        <?php 
            echo $this->Form->select(
            'customer_id',
            $customers ,
            ['empty' => '(choose one customer)', 'id'=>'customer_id']
        );
        ?>
    </div>
  
    <div id="stocks"></div>

    <div class="item3"><?php echo $this->Form->control('itemrate'); ?></div>

    <div class="item4"><?php echo $this->Form->control('itemquantity'); ?></div>

    <div class="item5"><?php echo $this->Form->control('total'); ?></div>

    <div class="item6"><?php echo $this->Form->control('date', array("value"=>date('Y-m-d'))); ?></div>

    <div>
        <input type="button" value="Save" onclick="submitsalesform();" />
        <input type="reset" value="Reset" />
        <input type="button" value="Listitems" onclick="listitems();"/>

    </div>
</div>
        <?php echo $this->Form->end(); ?>
<hr>
<div id="listitems" class="first span-10"></div>



<script>
// $("#stocks").load("/serve1/spaccount/web/stocks/loadstockname");
    $("#stocks").load("<?php
    echo $this->Url->build(array('controller' => 'Stocks',
        'action' => 'loadstockname'));
    ?>");
</script>
<script>
//DISPLAYING LIST OF ITEMS ADDED NOW FOR PRESENT DATE
// $("#listitems").load("/serve1/spaccount/web/sales/listitems2");
    $("#listitems").load("<?php echo $this->Url->build(array('controller' => 'Sales', 'action' => 'listitems2')); ?>");


    function listitems() {
        // $("#listitems").load("/serve1/spaccount/web/sales/listitems2");
        $("#listitems").load("<?php echo $this->Url->build(array('controller' => 'Sales', 'action' => 'listitems2')); ?>");


    }
</script>
<script>

    function customer() {
//CONVERT DATA TO JSON FORMAT	
        var custom =<?php echo json_encode($customers); ?>;
//        console.log(custom);

//GET VALUE OF FORM INPUT TO FIND DATA IN JSON VAR
        var k = $("#customerid").val();

//var k=k-1;

// USE SAPH.JS FOR TRAVERSING THE JSON ARRAY
//var db=SpahQL.db(custom);
//console.log(custom[k]);
        var i = 0;
        while (i != 1000) {
            //alert("adsfafds");
            var db = SpahQL.db(custom[i]);
            //console.log(db);
            var cid = db.select("/id");
            var id = parseInt(cid.value());
            if (k == id) {
                //alert("success"+ id );
                var customers = db.select("/name");

                $("#name").val(customers.value());
            }
            i++;
        }
//FIND THE CUSTOMER NAME AND SET THE FORM INPUT CUSTOMER NAME
//var customers=db.select("/customers/name");
//console.log(customers.value());
//$("#SaleName").val(customers.value());

//alert(customers.value());
    }//function customers ends here
</script>
<script>
//CALCULATE TOTAL USING THE ITEMQUANTITY
    $("#itemquantity").change(function () {

        var rate = $("#itemrate").val();
        var qty = $("#itemquantity").val();
        var rate = parseFloat(rate);
        var qty = parseFloat(qty);
        var total = rate * qty;
        $("#total").val(total);
    });
</script>
<script>
//AJAX SUBMISSION OF FORM
    function submitsalesform() {
        var info1 = $("#SaleInsert2Form").serialize();
        console.log(info1);
        //$("#SalesitemAddForm").post("/serve1/spaccount/web/salesitems/add");
        $.ajax({
            url: '<?php echo $this->Url->build(array('controller' => 'Sales', 'action' => 'insert2')); ?>',
            type: 'POST',
            data: info1,
            success: function (data) {
                //called when successful
                $('#ajaxphp-results').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
        alert("Saved");
listitems() ;
    }
</script>
<script>
//DELETE ITEMS ADDED USING AJAX
    function deletesales(id) {
        $.ajax({
            url: '<?php echo $this->Url->build(array('controller' => 'Sales', 'action' => 'delete2')); ?>/' + id,
            type: 'POST',
            //data: info1 ,
            success: function (data) {
                //called when successful
                $('#ajaxphp-results').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
        alert("Deleted !");
       listitems();
    }
</script>

<script>
    $(".chosen-select").chosen({disable_search_threshold: 10});

     $("#customer_id").chosen({disable_search_threshold: 10});
    </script>