<?php
declare(strict_types=1);
namespace TransportApp\Controller;

use TransportApp\Controller\AppController;
use TransportApp\Model\Entity\Purchase;
use TransportApp\Model\Table\PurchaseTable;
/**
 * Gst Controller
 *
 * @property \TransportApp\Model\Table\SMSTable $Gst
 * @method \TransportApp\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GstController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() {
        $gst = $this->paginate($this->Gst);

        $this->set(compact('gst'));
    }

    /**
     * View method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $gst = $this->Gst->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gst'));
    }
   
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $gst = $this->Gst->newEmptyEntity();
        if ($this->request->is('post')) {
            $gst = $this->Gst->patchEntity($gst, $this->request->getData());
            if ($this->Gst->save($gst)) {
                $this->Flash->success(__('The gst has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gst could not be saved. Please, try again.'));
        }
        $this->set(compact('gst'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $gst = $this->Gst->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gst = $this->Gst->patchEntity($gst, $this->request->getData());
            if ($this->Gst->save($gst)) {
                $this->Flash->success(__('The gst has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gst could not be saved. Please, try again.'));
        }
        $this->set(compact('gst'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $gst = $this->Gst->get($id);
        if ($this->Gst->delete($gst)) {
            $this->Flash->success(__('The gst has been deleted.'));
        } else {
            $this->Flash->error(__('The gst could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function gstinvoice(){

     $this->loadModel('TransportApp.Sets');          

     $sets = $this->Sets->find('list')->select(['name'=>'Sets.name'])->where(['Sets.type' => 'sale_ac'])->limit(10)->toArray();
     $this->set(compact('sets'));

     $sets1 = $this->Sets->find('list')->select(['name'=>'Sets.name'])->where(['Sets.type' => 'from_ps'])->limit(10)->toArray();
     $this->set(compact('sets1'));

     $sets2 = $this->Sets->find('list')->select(['name'=>'Sets.name'])->where(['Sets.type' => 'hs_code'])->limit(10)->toArray();
     $this->set(compact('sets2'));

     $sets3 = $this->Sets->find('list')->select(['name'=>'Sets.name'])->where(['Sets.type' => 'transport'])->limit(10)->toArray();
     $this->set(compact('sets3'));

     $sets4 = $this->Sets->find('list')->select(['name'=>'Sets.name'])->where(['Sets.type' => 'station'])->limit(10)->toArray();
     $this->set(compact('sets4'));
    
        $gst = $this->Gst->newEmptyEntity();
        if ($this->request->is('post')) {
            $gst = $this->Gst->patchEntity($gst, $this->request->getData());
            if ($this->Gst->save($gst)) {
                $this->Flash->success(__('The gst has been saved.'));

                return $this->redirect(['action' => 'gstinvoice']);
            }
            $this->Flash->error(__('The gst could not be saved. Please, try again.'));
        }

        $this->set(compact('gst'));
    }
    public function gstlist() {
        $this->loadModel('TransportApp.Gst');
        $this->loadModel('TransportApp.Customers');   
        $customers = $this->Customers->find('list')->select(['id'=>'Customers.id','party_name'=> 'Customers.name'])->limit(10)->toArray();
        $gst = $this->paginate($this->Gst);
        $this->set(compact('gst'));
    }
  
    public function purchaseinvoice(){
        $this->loadModel('TransportApp.Purchase');   
        $this->loadModel('TransportApp.Customers');        
        $customers = $this->Customers->find('list')->select(['id'=>'Customers.id','party_name'=> 'Customers.name'])->limit(10)->toArray();
        $this->set(compact('customers'));

        $purchase = $this->Purchase->newEmptyEntity();
            if ($this->request->is('post')) {
                $purchase = $this->Purchase->patchEntity($purchase, $this->request->getData());
                if ($this->Purchase->save($purchase)) {
                    $this->Flash->success(__('The purchase has been saved.'));
    
                    return $this->redirect(['action' => 'purchaseinvoice']);
                }
                $this->Flash->error(__('The purchase could not be saved. Please, try again.'));
            }
            $this->set(compact('purchase'));
    }   
    public function purchaselist() {
        $this->loadModel('TransportApp.Purchase');
        $this->loadModel('TransportApp.Customers');   
        $customers = $this->Customers->find('list')->select(['id'=>'Customers.id','party_name'=> 'Customers.name'])->limit(10)->toArray();     
        $purchase = $this->paginate($this->Purchase);
        $this->set(compact('purchase'));
    }
}