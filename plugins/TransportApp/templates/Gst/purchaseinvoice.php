<style>
    .cotnanier{
        padding:5px;
    }
    .item1,.item3,.item6{
        border:2px;
        display:flex;
        flex-direction:row;
        flex-wrap:wrap;
    }
    .item2 > label > input{
        display:inline;
        width:100%;
    }
    .item > label ,
    .item  > .input > label ,
    .item > .input > label{
        width:150px;
        margin-left:20px;      
    } 
    table{
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        border: 1px solid #ddd;
    }
    #o-no,#quality-description{
        width:120px;      
    }
    #quantity,#rate,#amount{
        width:70px;
    }  
</style>

<h1> Purchase Invoice जि  एस टी पर्चस </h1>
<?php echo $this->Form->create($purchase , array('id'=>'PurchaseForm')); ?>
<div class="cotnanier">
    <div class="item1">
        <div class="item1.1">
                <div class="item"><?php echo $this->Form->label('purchase_ac');echo $this->Form->select('purchase_ac', $customers ,["class"=>'chosenselect']);?></div>
                <div class="item"><?php echo $this->Form->label('party_Name'); echo $this->Form->select('party_name', $customers ,["class"=>'chosenselect']);?></div>
                <div class="item"><?php echo $this->Form->label('pay_to'); echo $this->Form->select('pay_to',$customers ,["class"=>'chosenselect']);?></div>
        </div>
        <div class="item1.2">
                <div class="item"><?php echo $this->Form->control('invoice_no'); ?></div>
                <div class="item"><?php echo $this->Form->control('invoice_no_date',["label"=>'Invoice Date']); ?></div>
        </div>
        <div class="item1.3">
                <div class="item"><?php echo $this->Form->control('order_no'); ?></div>
                <div class="item"><?php echo $this->Form->control('order_no_date',["label"=>'Order Date']); ?></div>
        </div>    
    </div>
<br>
<div style="overflow-x:auto;">
        <table>
            <tr>
                <td><?php echo $this->Form->control('o_no',["label"=>'Order No']); ?> </td>

                <td><?php echo $this->Form->control('quality_description'); ?></td>

                <td><?php echo $this->Form->label('hsn_code1'); echo $this->Form->select('hsn_code1', $customers ,["class"=>'chosenselect']); ?></td>

                <td><?php echo $this->Form->label('hsn_code2'); echo $this->Form->select('hsn_code2', $customers ,["class"=>'chosenselect']); ?></td>

                <td><?php echo $this->Form->control('quantity'); ?></td>

                <td><?php echo $this->Form->control('rate'); ?></td>

                <td><?php echo $this->Form->control('amount'); ?></td>

                <td><button id="addRow" onclick="additem(event);">Add</button></td>
            </tr>
        </table>         
    </div>  
    <div class="item5">
        <table id="itemstable">
            <thead>
                <th>Order No</th>
                <th>Quality Description</th>
                <th>HSN code1</th>
                <th>HSN code2</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </thead>
        </table>
    </div>

    <div class="item3">
            <div class="item3.1">
                <div class="item"><?php echo $this->Form->control('add1'); ?></div>               
                
                <div class="item"><?php echo $this->Form->control('add2'); ?></div>

                <div class="item"><?php echo $this->Form->control('add3'); ?></div>
            </div>
            <div class="item3.2">
                <div class="item"><?php echo $this->Form->control('add_value1',["label"=>'Add Amount 1']); ?></div>

                <div class="item"><?php echo $this->Form->control('add_value2',["label"=>'Add Amount 2']); ?></div>

                <div class="item"><?php echo $this->Form->control('add_value3',["label"=>'Add Amount 3']); ?></div>
            </div>       
        </div>
        <div class="item6">    
            <div class="item3.3">
                <div class="item"><?php echo $this->Form->control('deduction1'); ?></div>

                <div class="item"><?php echo $this->Form->control('deduction2'); ?></div>

                <div class="item"><?php echo $this->Form->control('deduction3'); ?></div>
            </div>
            <div class="item3.4">
                <div class="item"><?php echo $this->Form->control('deduction_value1',["label"=>'Deduction Amount 1']); ?></div>
             
                <div class="item"><?php echo $this->Form->control('deduction_value2',["label"=>'Deduction Amount 2']); ?></div>
                
                <div class="item"><?php echo $this->Form->control('deduction_value3',["label"=>'Deduction Amount 3']); ?></div>
            </div>   
        </div>
    </div>

    <div class="item4">
            <div class="item"><?php echo $this->Form->control('tax_type'); ?></div>

            <div class="item"><?php echo $this->Form->control('total_weight'); ?></div>

            <div class="item"><?php echo $this->Form->control('due_day'); ?></div>

            <div class="item"><?php echo $this->Form->submit(); ?> </div> 
    </div> 
</div>
        <?php echo $this->Form->end();?>
<hr>
<div id="listitems" class="first span-10"></div>

<script>
 $(".chosenselect").chosen({disable_search_threshold: 10});
 
 function additem(e){
     e.preventDefault();
     
 }

 $(document).ready(function() {

    var f = $('#itemstable2').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching":false
    }); 

    var t = $('#itemstable').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching":false
    });     
        $('#addRow').on( 'click', function () {
        var o_no= $("#o-no").val();
        if(o_no != '') {     
        var quality_description= $("#quality-description").val();
        var hsn_code1= $("td:nth-child(3) > select:nth-child(2)").val();
        var hsn_code2= $("td:nth-child(4) > select:nth-child(2)").val();
        var quantity= $("#quantity").val();
        var rate= $("#rate").val();
        var amount= $("#amount").val();     
            t.row.add( [        
                o_no ,quality_description,hsn_code1,hsn_code2,quantity,rate,amount        
            ] ).draw( false );       
        }
    } );
    // Automatically add a first row of data
    // $('#addRow').click();
} );
</script>