<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesPaymentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesPaymentsTable Test Case
 */
class SalesPaymentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesPaymentsTable
     */
    protected $SalesPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SalesPayments',
        'app.Sales',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SalesPayments') ? [] : ['className' => SalesPaymentsTable::class];
        $this->SalesPayments = $this->getTableLocator()->get('SalesPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SalesPayments);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
