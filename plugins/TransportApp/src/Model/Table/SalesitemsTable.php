<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Salesitems Model
 *
 * @property \TransportApp\Model\Table\SalesTable&\Cake\ORM\Association\BelongsTo $Sales
 * @property \TransportApp\Model\Table\BalancesTable&\Cake\ORM\Association\BelongsTo $Balances
 *
 * @method \TransportApp\Model\Entity\Salesitem newEmptyEntity()
 * @method \TransportApp\Model\Entity\Salesitem newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Salesitem get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Salesitem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Salesitem[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SalesitemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('salesitems');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'className' => 'TransportApp.Sales',
        ]);
        $this->belongsTo('Balances', [
            'foreignKey' => 'balance_id',
            'className' => 'TransportApp.Balances',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('itemname')
            ->maxLength('itemname', 45)
            ->allowEmptyString('itemname');

        $validator
            ->nonNegativeInteger('itemquantity')
            ->allowEmptyString('itemquantity');

        $validator
            ->numeric('itemrate')
            ->allowEmptyString('itemrate');

        $validator
            ->scalar('total')
            ->maxLength('total', 45)
            ->allowEmptyString('total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['sale_id'], 'Sales'), ['errorField' => 'sale_id']);
        $rules->add($rules->existsIn(['balance_id'], 'Balances'), ['errorField' => 'balance_id']);

        return $rules;
    }
}
