<?php
declare(strict_types=1);

namespace TransportApp\Test\TestCase\Model\Table;

use Cake\TestSuite\TestCase;
use TransportApp\Model\Table\SmsTable;

/**
 * TransportApp\Model\Table\SmsTable Test Case
 */
class SmsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransportApp\Model\Table\SmsTable
     */
    protected $Sms;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.TransportApp.Sms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Sms') ? [] : ['className' => SmsTable::class];
        $this->Sms = $this->getTableLocator()->get('Sms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Sms);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
