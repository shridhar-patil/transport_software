<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $purchase
 */
?>
<div class="purchase index content">
    <?= $this->Html->link(__('New Purchase'), ['action' => 'purchaseinvoice'], ['class' => 'button float-right']) ?>
    <h3><?= __('Purchase') ?></h3> 
   
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('purchase_ac') ?></th>
                    <th><?= $this->Paginator->sort('invoice_no') ?></th>
                    <th><?= $this->Paginator->sort('order_no') ?></th>
                    <th><?= $this->Paginator->sort('quality_description') ?></th>
                    <th><?= $this->Paginator->sort('quantity') ?></th>
                    <th><?= $this->Paginator->sort('rate') ?></th>
                    <th><?= $this->Paginator->sort('amount') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($purchase as $items): ?>
                <tr>
                <td> </td>
                    <td><?= $this->Number->format($items->id) ?></td>
                    <td><?= h($items->purchase_ac) ?></td>
                    <td><?= h($items->invoice_no) ?></td>
                    <td><?= h($items->order_no) ?></td>
                    <td><?= h($items->quality_description) ?></td>
                    <td><?= h($items->quantity) ?></td>
                    <td><?= h($items->rate) ?></td>
                    <td><?= h($items->amount) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $items->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $items->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $items->id], ['confirm' => __('Are you sure you want to delete # {0}?', $items->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
