<?php
declare(strict_types=1);

namespace TransportApp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sales Model
 *
 * @property \TransportApp\Model\Table\CustomersTable&\Cake\ORM\Association\BelongsTo $Customers
 * @property \TransportApp\Model\Table\SalesitemsTable&\Cake\ORM\Association\HasMany $Salesitems
 *
 * @method \TransportApp\Model\Entity\Sale newEmptyEntity()
 * @method \TransportApp\Model\Entity\Sale newEntity(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sale[] newEntities(array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sale get($primaryKey, $options = [])
 * @method \TransportApp\Model\Entity\Sale findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \TransportApp\Model\Entity\Sale patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sale[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \TransportApp\Model\Entity\Sale|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sale saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransportApp\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \TransportApp\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SalesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sales');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'className' => 'TransportApp.Customers',
        ]);
        $this->hasMany('Salesitems', [
            'foreignKey' => 'sale_id',
            'className' => 'TransportApp.Salesitems',
        ]);
        
        $this->hasMany('SalesPayments', [
            'foreignKey' => 'sales_id',
            'className' => 'TransportApp.SalesPayments',
        ]);
        
//        $this->belongsTo('SalesPayments', [
////            'foreignKey' => 'id',
//            'className' => 'TransportApp.SalesPayments',
//        ])->setForeignKey('sales_id')
//            ->setProperty('SalesPayments');
                ;

        
//         $this->belongsToMany('TransportApp.SalesPayments', [
//            'joinTable' => 'sales_payments',
//        ]);
         
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 90)
            ->allowEmptyString('name');

        $validator
            ->scalar('salesman')
            ->maxLength('salesman', 45)
            ->allowEmptyString('salesman');

        $validator
            ->scalar('vehicleno')
            ->maxLength('vehicleno', 45)
            ->allowEmptyString('vehicleno');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->numeric('payed')
            ->allowEmptyString('payed');

        $validator
            ->numeric('pending')
            ->allowEmptyString('pending');

        $validator
            ->scalar('itemname')
            ->maxLength('itemname', 45)
            ->allowEmptyString('itemname');

        $validator
            ->scalar('location')
            ->maxLength('location', 45)
            ->allowEmptyString('location');

        $validator
            ->numeric('itemrate')
            ->allowEmptyString('itemrate');

        $validator
            ->numeric('itemquantity')
            ->allowEmptyString('itemquantity');

        $validator
            ->numeric('total')
            ->allowEmptyString('total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'), ['errorField' => 'customer_id']);

        return $rules;
    }
}
