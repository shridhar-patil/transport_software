<?php
declare(strict_types=1);

namespace TransportApp\Model\Entity;

use Cake\ORM\Entity;

/**
 * Purchase Entity
 *
 * @property int $id
 * @property string|null $from
 * @property string|null $to
 * @property string|null $message
 * @property \Cake\I18n\FrozenTime|null $created_at
 */
class Purchase extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_ac' => true,
        'invoice_no' => true,
        'invoice_no_date' => true,
        'party_name' => true,
        'order_no' => true,
        'order_no_date' => true,
        'pay_to' => true,
        'o_no' => true,
        'quality_description' => true,
        'hsn_code1' => true,
        'hsn_code2' => true,
        'quantity' => true,
        'rate' => true,
        'amount' => true,
        'add1' => true,
        'add_value1' => true,
        'add2' => true,
        'add_value2' => true,
        'add3' => true,
        'add_value3' => true,
        'deduction1' => true,
        'deduction_value1' => true,
        'deduction2' => true,
        'deduction_value2' => true,
        'deduction3' => true,
        'deduction_value3' => true,
        'total_weight' => true,
        'due_day' => true,

    ];
}